"""
Basic example with merging droplets
===================================

This example shows the most basic use of the code framework: 

#. Use predefined parameters to set-up the NSCH model with adaptive refinement.
#. Use a predefined domain definition and a predefined initial condition.
#. More involved problem than the `fluid front` example


Typical example runs would be:

* "`python3 drop_merge.py`" to run the 2D simulation with two equally sized droplets.
* "`python3 drop_merge.py R0=0.2 R1=0.4 Dcenter=0.6`" to run the 2D simulation with a large and a small droplet.

The code is structured as follows:
"""


# %%
# First, we import the NSCH model by adding its parent directory to the os.path.
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), "../"))
from NSCH import *
from nutils import cli
import treelog

# %%
# To adopt the Nutils style, we define a main function that is later called by ``nutils.cli``. 
# Each variable is type-set, and default values are later given in the docstring. 
# This enables passing of arguments when calling the script, e.g. "`python3 drop_merge.py Pu=2 P=1`".
def main(R0:float,R1:float,Dcenter:float,width:float,height:float,epsilon:float, \
         nelemsx:int,nelemsy:int,steps:int,dt:float,refinements:int,Pu:int,P:int,k:int):
    """
    .. arguments::
        R0 [0.3]
            Radius of left droplet
        R1 [0.3]
            Radius of right droplet
        Dcenter [0.5]
            Distance of center of the two droplets
        width [1.6]
            Width of the rectangular domain
        height [1]
            height of the rectangular domain
        nelemsx [16]
            Elements in x-direction
        nelemsy [10]
            Elements in y-direction
        epsilon [0.1]
            Interface width parameter of coarsest level
        steps [10]
            Number of time steps run
        dt [0.1]
            Timestep size
        refinements [2]
            Number of (residual-based) spatial refinement levels
        Pu [3]
            Polynomial degree of u
        P [2]
            Polynomial degree of all other fields
        k [1]
            Basis continuity
    """

# %%
# We then set up the NSCH problem as a modification of the default problem settings.
# All parameters (physical, finite element, timestepping, postprocessing, etc.) are
# specified to calling the appropriate setters. Note for this example the use of 
# the `zero_mean_pressure` model parameter, as Dirichlet velocity data will be 
# specified all around the domain boundary. The ``rlist`` class exists to specify
# refinemenet-level dependent parameters.
    my_problem = ProblemDefinition(default=True)
    my_problem.set_finite_element_parameters(k=k,Pp=P,Pu=Pu,Pφ=P,Pμ=P)
    my_problem.set_time_stepping_parameters(dt=dt,T=dt*steps,method="CN")
    my_problem.set_post_processing_parameters(plot_fields=['u','φ','p','p - 0.5 ζmix_ij δ_ij'],
                                              export_folder="output/drop_merge")
    my_problem.set_physical_parameters(ε=epsilon)
    my_problem.set_model_parameters(extended_density_function=True)
    my_problem.set_model_parameters(zero_mean_pressure=True, \
                                    extended_density_function=True)
    if Pu == P+1:
        my_problem.set_stabilization_parameters(τLSIC=1E5)
    else:
        my_problem.set_stabilization_parameters(τpskel=10**(-P-2))
    if refinements > 0:
        my_problem.set_finite_element_parameters(refinement_method=1,refinement_levels=refinements)
        my_problem.set_time_stepping_parameters(method=rlist({-1:"CN"},default="IE"))
        my_problem.set_physical_parameters(ε=rlist([epsilon/2**r for r in range(refinements+1)]))

# %%
# A variety of domain definitions and initial conditions is predefined. 
# The ``DomainDefinition`` class houses Nutils ``domain`` and ``geometry`` information,
# as well as additional boundary condition information. The ``intial_condition_getter``
# is a function that takes a ``SolutionSet`` and produces a projection integral. That 
# approach enables residual based adaptive refinement already on the initial condition.
# Together with the ``problem_definition``, all this information is passed along to a
# ``TimeStepper`` instance, which involves most of the actual implemenetation.
    rectangular_domain = create_rectangular_domain(width,height, [nelemsx,nelemsy], origin='center')
    rectangular_domain.add_Dirichlet_condition('u_0', 'bottom,left,right,top', 0, \
                                               'u_1', 'bottom,left,right,top', 0, \
                                               'φ', 'bottom,left,right,top', -1) 
    initial_condition_getter = ic_generator_two_droplets(R0,R1,Dcenter)
    time_stepper = TimeStepper(my_problem,rectangular_domain,initial_condition_getter)

# %%
# The full simulation is then run simply in a while loop, by successively calling ``perform_time_step``.
    while not time_stepper.stop_criterion_reached():
        time_stepper.perform_time_step() # <-- Most implementation is in here

# %%
# The ``main`` function is ran by passing it to the Nutils command-line interface. This 
# interfaces all logging and plotting in the ``NSCH`` module with the ``treelog`` module.
if __name__ == "__main__":
    cli.run( main )

# %%
# Below follows the complete example file:
#
# .. literalinclude:: drop_merge.py
#    :lines: 21-116