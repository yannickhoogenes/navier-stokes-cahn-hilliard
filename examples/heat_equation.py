"""
Manual example of heat equation
===============================

This example shows the extensibility of the framework by adopting manual weak formulations, 
a manual intitial condition and user actions (to plot the error) in between time steps. 
Most likely, extensions to the weak form would be made directly as a seperate module.

The support for storing and loading solutions as ``dill`` files is also illustrated.
Typical example runs would be:

* "`python3 heat_equation.py`" to run the first 4 time steps
* "`python3 heat_equation.py dill_name=heat_solution/heat_solution_t5.dill`" to run the subsequent 4 time steps.

The code is structured as follows:
"""


# %%
# First, we import the NSCH model by adding its parent directory to the os.path.
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), "../"))
from NSCH import *
from nutils import cli
import treelog


# %%
# The manual residual and inertial forms are provided as getter functions that take as
# an argument a ``SolutionSet`` instance and return the integral of the forms.
# The relevant fields are provided as a list: ``[name:str,type:str,P:int,k:int]``
def my_residual(solution_set, *args,**kwargs):
    # Heat equation.
    domain = solution_set.domain; ns = solution_set.ns; get_degree=solution_set.get_degree
    return domain.integral( '∇_j(φ)  ∇_j(φtest) dV' @ ns , degree=get_degree('φφ'))
def my_mass(solution_set, *args,**kwargs):
    # Heat equation.
    domain = solution_set.domain; ns = solution_set.ns; get_degree=solution_set.get_degree
    return domain.integral( 'φ  φtest dV' @ ns , degree=get_degree('φφ'))
def my_fields_list(P,k):
    return [('φ','scalar',P,k),('φtest','scalar',P,k)]
    
# %%
# Similarly, the initial condition getter function takes a ``SolutionSet`` and returns
# the integral form of the projection.
def my_ic_generator(solution_set, t=0):
    domain = solution_set.domain; ns = solution_set.ns.copy_(); get_degree=solution_set.get_degree
    ns.pi = pi; ns.t = t
    return domain.integral('''(φ - 100 sin(pi x_0) sin(pi x_1) exp(-2 t pi^2)) φtest''' @ ns,degree=get_degree('φφ'))

# %%
# And a user operation can be defined as a function that is ran after (each 
# refinement of) each time step. It takes the ``time_stepper`` object and the
# current ``solution_set`` as arguments, and al kwargs that are also passed to 
# all actions in the action list of the ``time_stepper``.
def plot_error(time_stepper,solution_set,**kwargs):
        true_sol_sqr = my_ic_generator(solution_set, time_stepper.t+time_stepper.dt)
        true_sol = solver.solve_linear(['φdofs'], [true_sol_sqr.derivative("φtestdofs")], constrain=time_stepper.last_constraint_vector)
        error = {'φdofs' : solution_set.solution_dict['φdofs']-true_sol['φdofs']}
        export_plot(solution_set,['φ'], solution_dict = error,filename='φ_error.png')

# %%
# To adopt the Nutils style, we define a main function that is later called by ``nutils.cli``. 
# Each variable is type-set, and default values are later given in the docstring. 
# This enables passing of arguments when calling the script, e.g. "`python3 heat_equation.py P=2 k=1`".
def main(dill_name:str,dt_method:str,dt:float,steps:int,nelems:int,P:int,k:int):
    '''
    .. arguments::
        dill_name []
            Path relative to the output directory to the dill file
            to load as initialization.
        dt_method [CN]
            Time-stepping method
        dt [0.0005]
            Timestep size
        steps [5]
            Number of time steps run
        nelems [20]
            Elements in x- and y-direction
        P [1]
            Polynomial degree of all other fields
        k [0]
            Basis continuity
    '''

# %%
# As this is not an NSCH simulation, we take an empty problem defintion and only
# supply relevant parameters; generic time-stepping and post-processing parameters.
# The manual problem is then set as a 3-tuple with the residual and mass getters 
# and the list of variable types. The action that is performed at (each refinement of)
# each timestep is passed as a function to the ``user_operation`` model parameter key.
    my_problem = ProblemDefinition() # Empty problem definition
    my_problem.set_time_stepping_parameters(method=dt_method,dt=dt,T=dt*steps)
    my_problem.set_post_processing_parameters(export_folder="output/heat_solution", 
                                              plot_fields=['φ'], plot_interval=1,
                                              plot_sample_degree=3, dill_interval=-1,
                                              dill_filename="heat_solution")
    my_problem.set_model_parameters(manual=(my_residual,my_mass,my_fields_list(P,k))) # Specify custom PDE
    my_problem.set_model_parameters(user_operation=plot_error) # Add custom operation to the action stack

# %%
# We make use of a unit square domain, and create the ``TimeStepper`` instance.
    square_domain = create_square_domain(1, nelems)
    square_domain.add_Dirichlet_condition('φ','bottom,left,right,top','0')
    time_stepper = TimeStepper(my_problem,square_domain)

# %%
# The initial condition is then supplied to the ``time_stepper`` either as the just-defined 
# ``ic_getter`` function, or as a pathname to a ``dill`` file.
    if dill_name:
        time_stepper.set_initial_condition("./output/"+dill_name)
        my_problem.set_time_stepping_parameters(T=time_stepper.t+dt*steps)
        treelog.user("Using fourth solution from previous run to compute the next 4 time steps.")
    else:
        time_stepper.set_initial_condition(my_ic_generator)
        treelog.user("Using the prescribed initial condition to compute the first 4 time steps.")

# %%
# The full simulation is then run simply in a while loop, by successively calling ``perform_time_step``.
    while not time_stepper.stop_criterion_reached():
        time_stepper.perform_time_step() # <-- Most implementation is in here

# %%
# The ``main`` function is ran by passing it to the Nutils command-line interface. This 
# interfaces all logging and plotting in the ``NSCH`` module with the ``treelog`` module.
if __name__ == "__main__":
    cli.run( main )

# %%
# Below follows the complete example file:
#
# .. literalinclude:: heat_equation.py
#    :lines: 19-127