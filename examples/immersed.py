"""
Example of the immersed module
==============================

This example shows the most basic use of the code framework."""


# %%
# Import all modules
from nutils import *
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), "../"))
from NSCH import *
import treelog

# %%
# Define the problem as a modification of default settings
def main(inflow=5,width=4E-5,nelemsx=40,dt=0.25*7.8125E-8,steps=800, eps_frac=1):
    eps_baseline = 7.8125e-07
    m_baseline = 2.4389632e-10 * 2**-3
    eps_target = eps_baseline/eps_frac
    m_target = m_baseline*eps_frac**(-1.7) # Based on Demont et al 2023 scaling
    refinement_levels = int(numpy.log2(eps_frac))

    my_problem = ProblemDefinitionImmersed(default=True)
    my_problem.set_finite_element_parameters(k=0,Pp=2,Pu=3,Pφ=3,Pμ=3,tol_newton_solver=1E-7)
    my_problem.set_time_stepping_parameters(T=dt*steps)
    my_problem.set_post_processing_parameters(plot_fields=['u_0','u_1','p','μ','φ'],plot_mesh_overlay=False,\
                                              export_folder="output/immersed",dill_interval=5,dill_filename="immersed_solution")
    my_problem.set_model_parameters(extended_density_function=True)
    my_problem.set_physical_parameters(μ2=1.E-3,ρ2=1E3,Λ=1) # Change both to water
    # my_problem.set_physical_parameters(μ2=1.813E-5,Λ=1)

    # Refinement related
    my_problem.set_finite_element_parameters(refinement_levels=refinement_levels,refinement_method=2,refinement_measure=1)
    #my_problem.set_time_stepping_parameters(method=rlist({-1:"CN"},default="IE"),adaptive_method=1)
    my_problem.set_time_stepping_parameters(method="IE",adaptive_method=1)
    my_problem.set_physical_parameters(ε=rlist([eps_target*2**r for r in range(refinement_levels-1,0,-1) ] + 2*[eps_target]) , \
                                       m=rlist([m_target*8**r   for r in range(refinement_levels-1,0,-1) ] + 2*[m_target]  ))

# %%
# Specify immersed parameters
    my_problem.set_stabilization_parameters(βu=10,γu=10,γp=1,γφ=100,γμ=100)

# %%
# Define the immersed-boundary domain from a level-set function
    h = width/nelemsx
    trimfunc =  "0.1 + 0.25 sin( 2E5 x_1) sin( -1E5 x_0 ) " \
              + "+ 0.7 ( 1E5 x_1 + 0.14 ) cos( -4E10 x_1 x_0) cos( -3E5 x_0 ) " \
              + "+ 0.7 ( ( 1 / ( 1E10 x_1^2 + 0.5 + 0.8E10 x_0^2 ) ) - 0.2 ) sin( -6E10 x_1 x_0 - 2) sin( -4E5 x_0 ) " \
              + "+ 0.2 sin( 10E5 x_1 - 2 ) sin( -9E5 x_0 ) " \
              + "+ ( 1 / ( 3 ( 1E5 x_0 + 2 )^2 +  3 ( 1E5 x_1 - 1 )^2 + 0.25 ) ) " \
              + "- ( 1 / (20 ( 1E5 x_0 + 2 )^6 + 20 ( 1E5 x_1 - 1 )^6 + 0.01 ) ) "
    trimfunc =  f"{width/4+0.1*h} - abs( x_1 - {h} )"
    domain = create_immersed_domain_2D(trimfunc, width,width/2, [nelemsx,nelemsx//2], maxrefine=max(3,refinement_levels), origin="center")

    Ymin = width/4-0.9*h
    my_problem.define_expression('ux', Ux ,['x',inflow,Ymin])

    my_problem.set_time_stepping_parameters(dt=1000)
    initial_condition_getter = ic_generator_straight_interface(center=-1)
    domain.add_Dirichlet_condition('u', 'immersed', 0, \
                                   'u_1', 'top', 0, \
                                   'u_0', 'left', 'ux', \
                                   'u_1', 'left', 0, \
                                   'φ','left,right,top,bottom',1)
    time_stepper_ic = TimeStepper(my_problem,domain,initial_condition_getter)
    time_stepper_ic.perform_time_step()

    my_problem.set_time_stepping_parameters(dt=dt)
    domain.Dirichlet_bcs = []
    domain.add_Dirichlet_condition('u', 'immersed', 0, \
                                   'u_1', 'top', 0, \
                                   'u_0', 'left', 'ux', \
                                   'u_1', 'left', 0, \
                                   'φ','left',-1)
    initial_condition_getter = ic_generator_straight_interface(center=0)
    time_stepper = TimeStepper(my_problem,domain,initial_condition_getter)
    time_stepper.solution_set.solution_dict['udofs'] = time_stepper_ic.solution_set.solution_dict['udofs']

# %%
# Perform the time-stepping
    while not time_stepper.stop_criterion_reached():
        time_stepper.perform_time_step() # <-- Most implementation is in here

class Ux(function.Custom):
    def __init__(self,x,inflow,Ymin):
        super().__init__(args=[x],shape=(),dtype=float)
        self.inflow = inflow
        self.Ymin = Ymin
    def evalf(self,x):
        return self.inflow * (self.Ymin + x[:,1] ) * (self.Ymin*3 - x[:,1] )/(4*self.Ymin**2)

# %%
# Run the Nutils code.
if __name__ == "__main__":    
    cli.run( main )

# %%
# Below follows the complete example file:
#
# .. literalinclude:: immersed.py
#    :lines: 8-65
