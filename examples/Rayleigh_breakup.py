"""
Filament Rayleigh breakup
=========================

A full-fletched implementation of an axisymmetric infinite fillament with Rayleigh 
instability. Depending on the magnitude of the perturbation, pinch-off will occur.
Documented in van Sluijs' MSc thesis. As these simulations involve a rather large 
number of refinement levels, they are quite expensive computations. 

Runnable as:

* "`python3 Rayleigh_breakup.py dill_name=Rayleigh_breakup_ic.dill`" if this has been stored.
* "`python3 Rayleigh_breakup.py refinements=3`"
* "`python3 Rayleigh_breakup.py A_frac=0.05 refinements=3`"

The code is structured as follows:
"""


# %%
# First, we import the NSCH model by adding its parent directory to the os.path.
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), "../"))
from NSCH import *
from nutils import cli
import treelog

# %%
# Define the main function that is called by ``nutils.cli`` according to the Nutils standard, 
# i.e., with docstrings that define default arguments.
def main(R0:float, k_max:float, A_fac:float,eps_target:float,m_target:float,
         refinements:int,nelemsr:int,dt:float,steps:int,Pu:int,P:int,k:int):
    """
    .. arguments::
        R0 [2E-5]
            Radius of the filament. Domain-radius is taken as triple this value.
        k_max [0.688]
            Height parameter as a fraction of the half-filament circumference, per 'pi*R0/k_max'
        A_fac [1E-1]
            Rayleigh perturbation of the filament, as a factor of the radius, per 'A_fac*R0'
        eps_target [0.5E-6]
            Epsilon at deepest refinement level
        m_target [1E-12]
            Mobility at deepest refinement level
        refinements [2]
            Number of (residual-based) spatial refinement levels
        nelemsr [12]
            Number of elements in radial direction of the cylinder at initial refinement, 
            the number of elements in the height directly is scaled proportionately.
        dt [2.5E-6]
            Timestep size
        steps [200]
            Number of time steps run
        Pu [3]
            Polynomial degree of u
        P [2]
            Polynomial degree of all other fields
        k [1]
            Basis continuity
    """
    
# %%
# We set up the NSCH problem as a modification of the default problem settings.
    my_problem = ProblemDefinition(default=True)
    my_problem.set_finite_element_parameters(k=k,Pp=P,Pu=Pu,Pφ=Pu,Pμ=Pu,tol_newton_solver=1E-10)
    my_problem.set_time_stepping_parameters(dt=dt,T=dt*steps,repeat_halved_dt=4)
    my_problem.set_post_processing_parameters(plot_fields=['u','p','φ','ρmix'],\
                                              export_folder="output/Rayleigh",\
                                              dill_interval=5,dill_filename="Rayleigh_breakup")
    my_problem.set_model_parameters(zero_mean_pressure=True,extended_density_function=True)
    my_problem.set_stabilization_parameters(τLSIC=1E5)
    my_problem.set_finite_element_parameters(refinement_levels=refinements,refinement_method=3,refinement_threshold=0)
    my_problem.set_time_stepping_parameters(method=rlist({-1:"CN"},default="IE"),adaptive_method=1)
    my_problem.set_physical_parameters(ε=rlist([eps_target*2**r for r in range(refinements-2,0,-1) ] + 3*[eps_target]) , \
                                       m=rlist([m_target*8**r   for r in range(refinements-2,0,-1) ] + 3*[m_target]  ))

# %%
# Define the domain, boundary conditions and initial condition.
# Obtain the wrapper object for performing a time-step based on the problem definition
    L = H = pi*R0/k_max
    cylindrical_domain = create_axisymmetric_cylindrical_domain(R=3*R0, H=H, nelems=[nelemsr,round(nelemsr*H/(3*R0))])
    cylindrical_domain.add_Dirichlet_condition('u_0', 'right', 0 )
    cylindrical_domain.add_Dirichlet_condition('u_1', 'bottom,top', 0 )
    initial_condition_getter = ic_generator_infinite_filament(R=R0,A=A_fac*R0,L=L)
    time_stepper = TimeStepper(my_problem,cylindrical_domain,initial_condition_getter)

# %%
# The full simulation is then run simply in a while loop, by successively calling ``perform_time_step``.
# The empty lists are filled during the user defined action.
    while not time_stepper.stop_criterion_reached():
        time_stepper.perform_time_step() # <-- Most implementation is in here

# %%
# The ``main`` function is ran by passing it to the Nutils command-line interface. This 
# interfaces all logging and plotting in the ``NSCH`` module with the ``treelog`` module.
if __name__ == "__main__":
    cli.run( main )

# %%
# Below follows the complete example file:
#
# .. literalinclude:: Rayleigh_breakup.py
#    :lines: 20-97