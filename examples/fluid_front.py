"""
Basic example of a fluid front
==============================

This example shows the most basic use of the code framework: 

#. Use predefined parameters to set-up the NSCH model with adaptive refinement.
#. Use predefined domain definitions to run a 2D, 3D or axisymmetric simulation.
#. Use a predefined initial condition.

Typical example runs would be:

* "`python3 fluid_front.py`" to run the 2D standing capillary wave case (see Magaletti 2013).
* "`python3 fluid_front.py amplitude=0 inflow=0.1`" to run a 2D moving fluid front.
* "`python3 fluid_front.py CASE=2`" to run the case of a wavy fillament (i.e., axisymmetric).
* "`python3 fluid_front.py CASE=2 amplitude=0 refinements=0 steps=1`" to run a steady axisymmetric case, confirming the Young-Laplace law: ΔP = σ/R = 0.0728/0.5 = 0.1456 Pa.
* "`python3 fluid_front.py CASE=3 amplitude=0 inflow=0.1`" to run a 3D moving fluid front.

The code is structured as follows:
"""


# %%
# First, we import the NSCH model by adding its parent directory to the os.path.
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), "../"))
from NSCH import *
from nutils import cli
import treelog

# %%
# To adopt the Nutils style, we define a main function that is later called by ``nutils.cli``. 
# Each variable is type-set, and default values are later given in the docstring. 
# This enables passing of arguments when calling the script, e.g. "`python3 fluid_front.py Pu=2 P=1`".
def main(CASE:int,width:float,epsilon:float,amplitude:float,inflow:float,steps:int,\
        dt:float,refinements:int,nelemsx:int,nelemsy:int,nelemsz:int,Pu:int,P:int,k:int):
    """
    .. arguments::
        CASE [1]
            1: 2D Square domain with moving, curved fluid front
            2: Axisymmetric domain with curved fluid front
            3: 3D cubic domain with moving, curved fluid front
        width [1]
            Width of the square/cubic domain
        epsilon [0.1]
            Interface width parameter of coarsest level
        amplitude [0.025]
            Amplitude of the interface oscillation
        inflow [0]
            Inflow speed (reset to zero for axisymmetric case)
        steps [10]
            Number of time steps run
        dt [0.1]
            Timestep size
        refinements [2]
            Number of (residual-based) spatial refinement levels
        nelemsx [10]
            Elements in x-direction
        nelemsy [5]
            Elements in y-direction
        nelemsz [1]
            Elements in z-direction
        Pu [3]
            Polynomial degree of u
        P [2]
            Polynomial degree of all other fields
        k [0]
            Basis continuity
    """

# %%
# We then set up the NSCH problem as a modification of the default problem settings.
# All parameters (physical, finite element, timestepping, postprocessing, etc.) are
# specified to calling the appropriate setters. The ``rlist`` class exists to specify
# refinemenet-level dependent parameters.
    my_problem = ProblemDefinition(default=True)
    my_problem.set_finite_element_parameters(k=k,Pp=P,Pu=Pu,Pφ=P,Pμ=P)
    my_problem.set_time_stepping_parameters(dt=dt,T=dt*steps,method="CN")
    my_problem.set_post_processing_parameters(plot_fields=['u','φ','p','p - 0.5 ζmix_ij δ_ij'],
                                              export_folder="output/fluid_front")
    my_problem.set_physical_parameters(ε=epsilon)
    my_problem.set_model_parameters(extended_density_function=True)
    if Pu == P+1:
        my_problem.set_stabilization_parameters(τLSIC=1E5)
    else:
        my_problem.set_stabilization_parameters(τpskel=10**(-P-2))
    if refinements > 0:
        my_problem.set_finite_element_parameters(refinement_method=1,refinement_levels=refinements)
        my_problem.set_time_stepping_parameters(method=rlist({-1:"CN"},default="IE"))
        my_problem.set_physical_parameters(ε=rlist([epsilon/2**r for r in range(refinements+1)]))

# %%
# A variety of domain definitions and initial conditions is predefined. 
# The ``DomainDefinition`` class houses Nutils ``domain`` and ``geometry`` information,
# as well as additional boundary condition information. The ``intial_condition_getter``
# is a function that takes a ``SolutionSet`` and produces a projection integral. That 
# approach enables residual based adaptive refinement already on the initial condition.
    if CASE == 1:
        domain_definition = create_square_domain(width, [nelemsx,nelemsy])
        domain_definition.add_Dirichlet_condition('u_0','left',inflow, \
                                                  'u_1','bottom,top',0, \
                                                  'φ','left',-1)
    if CASE == 2:
        domain_definition = create_axisymmetric_cylindrical_domain(width,width, [nelemsx,nelemsy])
        domain_definition.add_Dirichlet_condition('u_1','bottom,top',0, \
                                                  'φ','right',1 )
    if CASE == 3:
        domain_definition = create_cube_domain(width, [nelemsx,nelemsy,nelemsz])
        domain_definition.add_Dirichlet_condition('u_0','left',inflow, \
                                                  'u_2','front,back',0, \
                                                  'u_1','bottom,top',0, \
                                                  'φ','left',-1 )
    initial_condition_getter = ic_generator_curved_interface(amplitude,center=0.5*width,u_0=inflow)

# %%
# Obtain the ``TimeStepper`` object for performing a time-step. This is the class with most
# implemenetation. It requires the problem definition, domain definition and initial condition
# getter to get started. These may also directly be passed in the constructor.
    time_stepper = TimeStepper()
    time_stepper.set_problem_definition(my_problem)
    time_stepper.set_domain_definition(domain_definition)
    time_stepper.set_initial_condition(initial_condition_getter)

# %%
# The full simulation is then run simply in a while loop, by successively calling ``perform_time_step``.
    while not time_stepper.stop_criterion_reached():
        time_stepper.perform_time_step() # <-- Most implementation is in here

# %%
# The ``main`` function is ran by passing it to the Nutils command-line interface. This 
# interfaces all logging and plotting in the ``NSCH`` module with the ``treelog`` module.
if __name__ == "__main__":    
    cli.run( main )

# %%
# Below follows the complete example file:
#
# .. literalinclude:: fluid_front.py
#    :lines: 23-133