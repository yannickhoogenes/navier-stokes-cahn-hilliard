"""
Taylor-Couette flow
===================

Investigation of wetting conditions.
"""


# %%
# First, we import the NSCH model by adding its parent directory to the os.path.
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), "../"))
from NSCH import *
from nutils import cli
from numpy import cos, pi
import treelog

# %%
# To adopt the Nutils style, we define a main function that is later called by ``nutils.cli``. 
# Each variable is type-set, and default values are later given in the docstring. 
# This enables passing of arguments when calling the script, e.g. "`python3 fluid_front.py Pu=2 P=1`".
def main(dill_name:str,width:float,radius:float,umax:float,αNS:float,αCH:float,\
         steps:int,dt:float,refinement_levels:int,nelemsx:int,Pu:int,P:int,k:int,\
         γskel:float,γu:float,γp:float,γφ:float,γμ:float,βu:float,Nitsche:int):
    """
    .. arguments::
        dill_name []
            Path relative to the output directory to the dill file
            to load as initialization.
        width [0.1]
            Width of the domain
        radius [0.025]
            Height of the domain
        umax [4E-3]
            Wall speed (positive means top moves to left bottom moves to right)
        αNS [100]
            Navier-slip parameter (0 means free slip, -1 means no-slip)
        αCH [0]
            Dynamic wetting parameter (TODO)
        steps [20]
            Number of time steps run
        dt [0.1]
            Timestep size
        refinement_levels [0]
            Number of (residual-based) spatial refinement levels
        nelemsx [40]
            Elements in x-direction
        Pu [3]
            Polynomial degree of u
        P [3]
            Polynomial degree of all other fields
        k [2]
            Basis continuity
        γskel [0.01]
            Skeleton stabilization coefficient
        γu [0.01]
            Velocity ghost stabilization coefficient
        γp [0.01]
            Pressure ghost stabilization coefficient
        γφ [0.01]
            Velocity ghost stabilization coefficient
        γμ [0.01]
            Pressure ghost stabilization coefficient
        βu [100]
            Velocity Nitsche parameter
        Nitsche [1]
            -1: Non-symmetric Nitsche (penalty + consistency terms)
            0: No Nitsche (only penalty terms)
            1: Partial symmetric Nitsche (penalty + consistency + viscous symmetry term)
            2: Symmetric Nitsche (penalty + consistency + all symmetry terms) TODO
    """

    # Jacqmin's coarsest epsilon if refinement_levels <= 2.
    ε_target = ((4**(-1))*numpy.sqrt(2)*((numpy.arctanh(0.9))**(-1))*6.475E-3)*2**(2-max(2,refinement_levels))
    # Jacqmin's coarsest mobility if refinement_levels <= 2.
    m_target = (1.06E-7)*2**(2-max(2,refinement_levels))
    μ1 = μ2 = 0.1
    ρ1 = ρ2 = 1E3

# %%
# We then set up the NSCH problem as a modification of the default problem settings.
# All parameters (physical, finite element, timestepping, postprocessing, etc.) are
# specified to calling the appropriate setters. The ``rlist`` class exists to specify
# refinemenet-level dependent parameters.
    my_problem = ProblemDefinitionImmersed(default=True)
    my_problem.set_finite_element_parameters(k=k,Pp=P,Pu=Pu,Pφ=Pu,Pμ=Pu)
    if Pu == P:
        my_problem.set_stabilization_parameters(τpskel=γskel)
    my_problem.set_time_stepping_parameters(dt=dt,T=dt*steps,method="IE")
    my_problem.set_post_processing_parameters(plot_fields=['u','u_0','u_1','∇_i(u_i)','φ','p','p - 0.5 ζmix_ij δ_ij'],
                                              plot_mesh_overlay=False,
                                              export_folder="output/immersed_bubbles")
    my_problem.set_physical_parameters(ε=ε_target, m=m_target)
    my_problem.set_physical_parameters(ρ1=ρ1, ρ2=ρ2, μ1=μ1, μ2=μ2)
    my_problem.set_physical_parameters(σ01=numpy.pi/2,σ02=numpy.pi/2,σ12=3E-2)
    my_problem.set_model_parameters(extended_density_function=True)
    if refinement_levels > 0:
        my_problem.set_finite_element_parameters(refinement_method=1,refinement_levels=refinement_levels)
        my_problem.set_physical_parameters(ε=rlist([ε_target*2**r for r in range(refinement_levels-2,0,-1) ] + min(3,refinement_levels+1)*[ε_target]) , \
                                           m=rlist([m_target*8**r for r in range(refinement_levels-2,0,-1) ] + min(3,refinement_levels+1)*[m_target]  ))

# %%
# A variety of domain definitions and initial conditions is predefined. 
# The ``DomainDefinition`` class houses Nutils ``domain`` and ``geometry`` information,
# as well as additional boundary condition information. The ``intial_condition_getter``
# is a function that takes a ``SolutionSet`` and produces a projection integral. That 
# approach enables residual based adaptive refinement already on the initial condition.

# Specify immersed parameters
    my_problem.set_stabilization_parameters(βu=βu,γu=γu,γp=γp,γφ=γφ,γμ=γμ)
    my_problem.set_model_parameters(Nitsche=Nitsche)

# Set Navier-Slip parameters
    my_problem.define_expression('uinflow',u_inflow,['x','t',width,radius,umax,αNS,μ1])
    my_problem.set_physical_parameters(αNS=αNS)
    if αNS > 0:
        my_problem.set_model_parameters(Navier_slip_boundaries="immersed")

    trimfuncs = [f"x_i x_i - {radius**2}", \
                 f" (x_0 - {width/2})^2 + (x_1 - {width/2})^2 - {radius**2}", \
                 f" (x_0 + {width/2})^2 + (x_1 - {width/2})^2 - {radius**2}", \
                 f" (x_0 - {width/2})^2 + (x_1 + {width/2})^2 - {radius**2}", \
                 f" (x_0 + {width/2})^2 + (x_1 + {width/2})^2 - {radius**2}" ]
    domain_definition = create_immersed_domain_2D(trimfuncs, width,width, [nelemsx,nelemsx], maxrefine=max(3,refinement_levels), origin="center")
    initial_condition_getter = "./output/" + dill_name if dill_name else ic_generator_straight_interface(center=0, flip=True)
    time_stepper = TimeStepper(my_problem,domain_definition,initial_condition_getter)
    domain_definition.add_Dirichlet_condition('u_1','top,bottom','0', \
                                              'u_i','left','uinflow_i', \
                                              'φ','left',1, \
                                              time_dependent = True)
    if αNS >= 0:
        domain_definition.add_Dirichlet_condition('u_i n_i', 'immersed', 0)
    else:
        domain_definition.add_Dirichlet_condition('u_i','immersed','uTC_i')
        
# %%
# The full simulation is then run simply in a while loop, by successively calling ``perform_time_step``.
    while not time_stepper.stop_criterion_reached():
        time_stepper.perform_time_step() # <-- Most implementation is in here
        ns = time_stepper.solution_set.ns
        solargs = time_stepper.solution_set.solution_dict
        domain = time_stepper.domain_definition.domain
        treelog.user("div u:", domain.integrate('∇_i(u_i) dV'@ns,degree=8,arguments=solargs) )
        treelog.user("u*n :", domain.boundary["immersed"].integrate('u_i n_i dS'@ns,degree=8,arguments=solargs) )


class u_inflow(function.Custom):
    def __init__(self,x,t, domheight,radius,umax,αNS,μ):
        super().__init__(args=[x,t],shape=(2,),dtype=float)
        self.ramp_t = 1
        self.height = domheight-2*radius
        self.radius = radius
        self.uslip = umax/(1+αNS*self.height/4/μ) if αNS >= 0 else 0
        self.umax = umax
    
    def evalf(self,x,t):
        t = float(t[0])
        u_fact = 1/2*(1-cos(pi*t/self.ramp_t)) if t < self.ramp_t else 1
        u = numpy.zeros_like(x)
        u[ : , 0] = ( self.uslip + (self.umax-self.uslip) * (self.height/2-x[:,1])*(self.height/2+x[:,1])/self.height**2*4 )* u_fact
        return u


# %%
# The ``main`` function is ran by passing it to the Nutils command-line interface. This 
# interfaces all logging and plotting in the ``NSCH`` module with the ``treelog`` module.
if __name__ == "__main__":    
    cli.run( main )

# %%
# Below follows the complete example file:
#
# .. literalinclude:: fluid_front.py
#    :lines: 23-133
