"""
A 2D oscillating droplet
========================

A full-fletched implementation of the 2D oscillating droplet from Demont et al. 2023.
As they involve a rather large number of refinement levels, these are quite expensive
computations. They output the vtk files of a quarter of the oscillating droplet, and
Bochner L2-norm error progressesion w.r.t. the linearized solution.

Runnable as:

* "`python3 oscillating_droplet.py dill_name=oscillating_droplet_ic_n2.dill`" if this has been stored.
* "`python3 oscillating_droplet.py n=2 eps_target_frac=8 m_target_frac=256`" optimal mobility for most refined case.
* "`python3 oscillating_droplet.py n=4 eps_target_frac=8 m_target_frac=128`" optimal mobility for most refined case for higher order mode.

The code is structured as follows:
"""


# %%
# First, we import the NSCH model by adding its parent directory to the os.path.
import sys, os
sys.path.append(os.path.join(os.path.dirname(__file__), "../"))
from NSCH import *
from scipy.special import jv as J1, hankel2 as H2
from numpy import cos, sin, exp, real, newaxis, sqrt, tanh, arctan2 as atan
from nutils import cli
import treelog

# %%
# Define the main function that is called by ``nutils.cli`` according to the Nutils standard, 
# i.e., with docstrings that define default arguments.
def main(dill_name:str,width:float,n:int,eps_target_frac:int,m_target_frac:int,\
         nelems:int,dt:float,steps:int,Pu:int,P:int,k:int):
    """
    .. arguments::
        dill_name []
            Path relative to the output directory to the dill file
            to load as initialization.
        width [5E-5]
            Domain width
        n [2]
            Mode of oscillation (2 and 4 are implemented)
        eps_target_frac [2]
            Epsilon at deepest refinement level as 7.8125e-07/eps_target_frac. This also 
            implicitly defines the number of refinement levels as 4+log2(eps_target_frac).
            Values in paper include 1, 2, 4 and 8.
        m_target_frac [32]
            Mobility at deepest refinement level as 2.4389632e-10/m_target_frac
            Values in paper include 1, 2, 2^2, 2^3 until 2^11.
        nelems [10]
            Number of elements in x and y at initial refinement
        dt [7.8125E-8]
            Timestep size
        steps [200]
            Number of time steps run
        Pu [3]
            Polynomial degree of u
        P [2]
            Polynomial degree of all other fields
        k [0]
            Basis continuity
    """

# %%
# We set up the NSCH problem as a modification of the default problem settings.
    eps_target = 7.8125e-07/eps_target_frac
    m_target = 2.4389632e-10/m_target_frac
    refinement_levels = 4+int(numpy.log2(eps_target_frac))
    my_problem = ProblemDefinition(default=True)
    my_problem.set_finite_element_parameters(k=k,Pp=P,Pu=Pu,Pφ=Pu,Pμ=Pu,tol_newton_solver=1E-10)
    my_problem.set_time_stepping_parameters(dt=dt,T=dt*steps)
    my_problem.set_post_processing_parameters(plot_fields=['u','p','φ','ρmix'],\
                                              export_folder="output/droplet",\
                                              dill_interval=5,dill_filename="oscillating_droplet",
                                              plot_mesh_overlay=rlist({0:True,1:True,2:True},default=False),
                                              plot_refinement_details=True) 
    my_problem.set_model_parameters(zero_mean_pressure=True,extended_density_function=True)
    my_problem.set_physical_parameters(μ2=1.813E-5,Λ=1)
    my_problem.set_finite_element_parameters(refinement_levels=refinement_levels,refinement_method=3,refinement_measure=1)
    my_problem.set_time_stepping_parameters(method=rlist({-1:"CN"},default="IE"),adaptive_method=1)
    my_problem.set_physical_parameters(ε=rlist([eps_target*2**r for r in range(refinement_levels-2,0,-1) ] + 3*[eps_target]) , \
                                       m=rlist([m_target*8**r   for r in range(refinement_levels-2,0,-1) ] + 3*[m_target]  ))
    my_problem.set_model_parameters(user_operation=plot_Bochner_error)

# %%
# The exact solution is passed to all ``Namespace``'s in all ``SolutionSet``'s by
# defining the expressions in the ``ProblemDefinition``.
    my_problem.define_expression('R',get_parameters(n)['R'])
    my_problem.define_expression('modeN',n)
    my_problem.define_expression('φinit',CirclePhasefield, ['x','R','ε'])
    my_problem.define_expression('utrue',TrueSolution, ['x','t','modeN'])

# %%
# Create the ``TimeStepper`` instance and pass the ``problem_definition``, the domain definition 
# and the initial condition getter.
    square_domain = create_square_domain(width, nelems)
    initial_condition_getter = "./output/" + dill_name if dill_name else ic_generator(['utrue','φinit'],['u','φ'])
    time_stepper = TimeStepper(my_problem,square_domain,initial_condition_getter)
    # Add boundary conditions after IC to avoid boundary refinement
    square_domain.add_Dirichlet_condition('u_i', 'bottom,left,right,top', 'utrue_i', time_dependent=True)

# %%
# The full simulation is then run simply in a while loop, by successively calling ``perform_time_step``.
# The empty lists are filled during the user defined action.
    time_stepper.L2truelist = []
    time_stepper.L2errorlist = []
    time_stepper.tlist = []
    while not time_stepper.stop_criterion_reached():
        time_stepper.perform_time_step() # <-- Most implementation is in here

# %%
# The user operation: computing the L2 error, storing it, and plotting the progression
# of the Bochner L2-error norms.
def plot_Bochner_error(time_stepper,solution_set,refinement_level=None,**kwargs):
    if refinement_level.get() != time_stepper.problem_definition.get('FE','refinement_levels'):
        return
    domain = solution_set.domain; ns = solution_set.ns; get_degree=solution_set.get_degree
    l2true = domain.integrate("utrue_i utrue_i dV" @ ns, degree=get_degree('uu++'))**0.5
    l2error = domain.integrate("(utrue_i - u_i) (utrue_i - u_i) dV" @ ns, degree=get_degree('uu++'), arguments=solution_set.solution_dict)**0.5
    time_stepper.L2truelist.append(l2true)    
    time_stepper.L2errorlist.append(l2error)
    time_stepper.tlist.append(time_stepper.t)
    sumtrue = numpy.cumsum( [ l2 for l2 in time_stepper.L2truelist] )
    sumerrors = numpy.cumsum( [ e for e in time_stepper.L2errorlist] )
    bochnertrue = [ t/(i+1) for i,t in enumerate(sumtrue)]
    bochnererrors = [ e/(i+1) for i,e in enumerate(sumerrors)]
    relbochnererrors = [ e/t for e,t in zip(bochnererrors,bochnertrue)]
    treelog.debug(time_stepper.tlist,time_stepper.L2errorlist,time_stepper.L2truelist)
    treelog.user("Relative L2 Bochner error:",relbochnererrors)
    with export.mplfigure("L2-Bochner-error.png") as fig:
        ax = fig.subplots(1)
        ax.plot(time_stepper.tlist,bochnererrors, 'x-')
    with export.mplfigure("Rel-L2-Bochner-error.png") as fig:
        ax = fig.subplots(1)
        ax.plot(time_stepper.tlist,relbochnererrors, 'x-')

# %%
# Evaluable function for the initial condition for phi: a simple circle
class CirclePhasefield(function.Custom):
    def __init__(self,x,R,ε):
        super().__init__(args=[x],shape=(),dtype=float)
        self.R = R.eval()
        self.ε = ε.eval()
    
    def evalf(self,x):
        r = numpy.linalg.norm(x,axis=1)
        return -tanh( (r-self.R) / (self.ε * sqrt(2)) )

# %%
# Evaluable function for the true solution (and thus initial condition) for u
class TrueSolution(function.Custom):
    def __init__(self,x,t,n):
        super().__init__(args=[x,t],shape=(2,),dtype=float)
        self.parameters = get_parameters(n.eval()) # Dictionary with all relevant parameters
    
    def evalf(self,x,t):
        t = float(t[0])
        r = numpy.linalg.norm(x,axis=1)
        theta = atan(x[:,1],x[:,0])
        indx_in  = (r <= self.parameters['R']).nonzero()
        indx_out = (r > self.parameters['R'] ).nonzero()
        uexact = numpy.zeros_like(x)
        uexact[indx_in,:] = self.inside(  t, r[indx_in], theta[indx_in], x[indx_in,0].flatten(), x[indx_in,1].flatten() , **self.parameters) 
        uexact[indx_out,:] = self.outside(t, r[indx_out],theta[indx_out],x[indx_out,0].flatten(),x[indx_out,1].flatten(), **self.parameters)
        return uexact

    def inside(self,t,r,theta,x,y, n=0,δ=0,H=0,P=0,R=0,A=0,E=0,G=0,sqrtG=0,Dt=0,**kwargs):
        J1n = J1(n,sqrtG/R*r)
        J1J1 = J1(n-1,sqrtG/R*r)-J1(n+1,sqrtG/R*r)
        return real( δ*H/(P*R)*exp(-G*H/(P*R*R)*(t+Dt))*( 
            ( cos(n*theta)*(E*n/R**(n-1)*r**(n-2)*x+A*n*J1n*R*x/r**2) + sin(n*theta)*(E*n/R**(n-1)*r**(n-2)*y + 0.5*A*sqrtG*y/r*J1J1) )[:,newaxis]*numpy.array([1,0]) 
          + ( cos(n*theta)*(E*n/R**(n-1)*r**(n-2)*y+A*n*J1n*R*y/r**2) - sin(n*theta)*(E*n/R**(n-1)*r**(n-2)*x + 0.5*A*sqrtG*x/r*J1J1) )[:,newaxis]*numpy.array([0,1]) ))

    def outside(self,t,r,theta,x,y, n=0,δ=0,H=0,P=0,R=0,B=0,F=0,G=0,sqrtRHG=0,Dt=0,**kwargs):
        H2n = H2(n,sqrtRHG/R*r)
        H2H2 = H2(n-1,sqrtRHG/R*r)-H2(n+1,sqrtRHG/R*r)
        return real( δ*H/(P*R)*exp(-G*H/(P*R*R)*(t+Dt))*( 
            ( cos(n*theta)*(F*n*R**(n+1)*r**(-n-2)*x+B*n*H2n*R*x/r**2) - sin(n*theta)*(F*n*R**(n+1)*r**(-n-2)*y-0.5*B*y*sqrtRHG/r*H2H2) )[:,newaxis]*numpy.array([1,0]) 
          + ( cos(n*theta)*(F*n*R**(n+1)*r**(-n-2)*y+B*n*H2n*R*y/r**2) + sin(n*theta)*(F*n*R**(n+1)*r**(-n-2)*x-0.5*B*x*sqrtRHG/r*H2H2) )[:,newaxis]*numpy.array([0,1]) ) )

# %%
# Parameters for the water droplet in air considered in the reference paper.
def get_parameters(n):
    parameters = { 'n':n, 'δ':0.01, 'H':0.001, 'P':1000, 'R':sqrt(2)*0.00001}
    if n == 2:
        parameters_n = { 'A':0.046641609353224462025+0.001375420286576458121j, 'B':114.21017372659606970+448.53965183564686980j, \
                         'E':0.523673362169117968+38.817298987688434145j, 'F':95.230022152994510011+94.181631814087089545j, \
                         'G':3.757636785475576745-78.079225410898901324j, 'sqrtG':6.4002824085615392207-6.0996703291134284802j, \
                         'sqrtRHG':1.5031427806225857992-1.4325423214325472073j, 'Dt':0.0000040235960808484986225 }
    elif n == 4:
        parameters_n = { 'A':-0.0012390097662801594185 - 0.0019507410192801644306j, 'B':1490.1368919237731741 + 104.0690442846047767j, \
                         'E':1.419356030429828210 + 60.838910369190932702j, 'F':198.55261334556942733 + 155.23743051236099404j, \
                         'G':20.86670010834165572 - 244.65239549935738890j, 'sqrtG':11.541389782132784938 - 10.598914000725611947j, \
                         'sqrtRHG':2.7105611318271829102 - 2.4892153260798050193j, 'Dt':0.0000012841045954925240195 }
    else:
        raise NotImplementedError
    return {**parameters,**parameters_n}

# %%
# The ``main`` function is ran by passing it to the Nutils command-line interface. This 
# interfaces all logging and plotting in the ``NSCH`` module with the ``treelog`` module.
if __name__ == "__main__":    
    cli.run( main )

# %%
# Below follows the complete example file:
#
# .. literalinclude:: oscillating_droplet.py
#    :lines: 20-204
