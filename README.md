# Navier-Stokes Cahn-Hilliard

A Nutils powered Navier-Stokes Cahn-Hilliard (NSCH) code framework.

Provides a single framework for adressing a wide range of research questions, such as those pertaining to 
different NSCH formulations, adaptive refinement strategies, time-integration, immersed methods, surfactants, etc. 

To facilitate this, the main design philosophies of this code framework are:
- Modularity: providing functionality for mixing and matching different model/code components.
- Extensibility: supporting multiple approaches for introducing new model/code components.

Have a look at the docs by opening `./docs/index.html`.