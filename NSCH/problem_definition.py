import __main__
import os,treelog
from .weak_formulations import get_residual, get_residual_bdy, get_inertia
from .model_initialization import initialize_namespace_model_expressions, get_solution_fields_list


class rlist:
    """
    A simple wrapper around the python list, that lets the ProblemDefinition
    identify refinement-dependent parameters. Users can set the parameter per
    refinement level, or specify a default and exceptions (first/last/...).

    Parameters
    ----------
    vals : list or dict
        List of parameters for succesive refinement levels, or a dictionary 
        mapping specific refinement levels to parameters. Dictionary may 
        also include negative integers for (before-)last refinement levels.
    default : optional
        Parameter used if refinement level does not occur in provided dict,
        or past max index of provided list.

    Examples
    --------
    >>> ε_cont = rlist([ 0.1, 0.05, 0.025, 0.025 ])
    >>> tstep_meths = rlist({-1:"CN"},default="IE")
    >>> my_problem = ProblemDefinition(default=True)
    >>> my_problem.set_time_stepping_parameters(method=tstep_meths)
    >>> my_problem.set_physical_parameters(ε=ε_cont)
    """

    def __init__(self,vals, default=None):
        self.default = default
        self.vals_r = {r:val for r,val in enumerate(vals)} if type(vals) == list else vals
        self.refinement_levels = 0

    def __getitem__(self,item):
        if item in self.vals_r.keys():
            return self.vals_r[item]
        if item-self.refinement_levels-1 in self.vals_r.keys():
            return self.vals_r[item-self.refinement_levels-1]
        return self.default
    
    def __str__(self):
        return ", ".join(["r%i"%r+": "+str(self[r]) for r in range(0,self.refinement_levels+1)] )
            
        
class ProblemDefinition:
    """
    This class holds all the parameters required to set up a NSCH simulation. 
    They are separated into six sets:
    - Model ``model`` (the type of NSCH, user specified additions, etc)
    - Physical ``phys`` (densities, surface tensions, epsilons, etc)
    - Finite element ``FE`` (polynomial and continuity orders, refinement strategies, etc)
    - Stabilization ``stab`` (LSIC or skeleton stabilization parameter)
    - Time stepping ``time`` (stepsize, endtime, method, refinement approach, etc)
    - Post processing ``pp`` (vtk/png output, whether to store final state as dill, etc)
    Take a look at the source code for the full range of possible parameters.

    Writing your own module may involve enheriting from this class. In particular, the

    Parameters
    ----------
    default : bool, optional
        Whether to use a default set of parameters.
        Default: False
    
    Attributes
    ----------
    all_rlists : list of rlist
        All refinement dependent parameters are collected here, for easy access and
        setting/resetting.
    refinement_levels : int
        The total amount of refinement levels. Automatically taken from the ``FE``
        parameter set when this is altered. At alteration, the corresponding attributes
        for all ``rlist`` parameters is also updated.
    refinement : int
        The current refinement level for which the parameters are decided
        (for ``rlist`` parameters) if the ``get`` method is used.
    user_defined_expressions : list
        Filled with either strings or nutils.function objects. These are added to each
        newly created ``Namespace`` of each new ``SolutionSet``.
    """
    def __init__(self, default=False):
        self.all_rlists:list[rlist] = [] # Easy access for resetting their refinement_levels
        self.refinement_levels = 0
        self.refinement = 0
        self.user_defined_expressions = [] # List of functions appended to namespace from SolutionSets

        self.__model_parameters = {
            "Stokes_linearization" : False, # Whether to use Stokes (True) or Navier-Stokes (False)
            "chemical_potential" : True, # Mixed or primal form of the Cahn-Hilliard equations
            "potential_split" : False, # Split or conventional double well potential 
            "directional_do_nothing_boundaries" : None, # The user-defined function operated after every time-step computation
            "zero_mean_pressure" : False, # Lagrange multiplier to enforce int p = 0 (if no traction BCs)
            "Conservative_CH" : False, # Using additional weak terms to stabilize immersed problems
            "Navier_slip_boundaries" : None, # The boundaries on which the Navier slip is added to the momentum residual.
            "extended_density_function" : False, # Whether to use positivity preserving in place of mixing density
            "user_operation" : lambda *a,**b: None, # The user-defined function operated after every time-step computation
            "manual" : None, # If a weak form is manually specified. Then as ( res(SolutionSet),mass(SolutionSet),field_list ) tuple.
        }
        self.__physical_parameters = {
            "ρ1" : None, # Density of phase 1 [kg/m3]
            "ρ2" : None, # Density of phase 2 [kg/m3]
            "μ1" : None, # Dynamic viscosity of phase 1 [kg/m/s]
            "μ2" : None, # Dynamic viscosity of phase 2 [kg/m/s]
            "σ01" : None, # Surface tension solid - phase 1 [kg/s2]
            "σ02" : None, # Surface tension solid - phase 2 [kg/s2]
            "σ12" : None, # Surface tension phase 1 - phase 2 [kg/s2]
            "Ff" : None, # Resultant force vector (e.g, pressure gradient)
            "Fg" : None, # Body force acceleration vector (used as F = Ff + Fg*rho_mix)
            "Λ" : None, # Volume ratio: ρ1 m2 / ρ2 m1. If not set, a linear approximation is used. # NOTE CHECK THIS!!!
            "dwa" : None, # Factor for scaling the double well potential, if not set a standard double well is used.
            "dwm" : None, # Factor for scaling the double well potential, if not set a standard double well is used.
            "m" : None, # Mobility [m3 s/kg]
            "ε" : None, # ~1/4th of phase-field thickness parameter [m]
            "αNS" : None, # Slip resistance in the Navier slip condition.
        }
        self.__finite_element_parameters = {
            "k" : 0, # Order of continuity of all fields, unless specified differently
            "Pu" : None, # Polynomial order of the basis functions for velocity
            "ku" : None, # Continuity order of the basis functions for velocity
            "Pp" : None, # Polynomial order of the basis functions for pressure
            "kp" : None, # Continuity order of the basis functions for pressure
            "Pφ" : None, # Polynomial order of the basis functions for order-parameter
            "kφ" : None, # Continuity order of the basis functions for order-parameter
            "Pμ" : None, # Polynomial order of the basis functions for chemical-potential
            "kμ" : None, # Continuity order of the basis functions for chemical-potential
            "Plmp" : None, # Lagrange multiplier for pressure field, must remain None
            "klmp" : None, # Filler
            "degree" : None, # Integration degree, None means auto-generated per integrand
            "tol_newton_solver" : 1E-12, # Absolute tolerance of the residual
            "fail_relaxation" : 0.05, # Minimum value for relaxation of newton solver
            "refinement_levels" : 0, # Number of iterations of the refinement method
            "refinement_method" : 0, # 0: None, 1: residual-based, 2: ...
            "refinement_measure" : 0, # 0: absolute value, 1: volume scaling, 2: L2-metric
            "refinement_fraction" : 0, # Elements are refined that are resposible for this percentile of the estimated error
            "refinement_threshold" : 0, # Residual value below which no-longer to refine.
        }
        self.__stabilization_parameter = {
            'τLSIC' : None, # Least-squares on incompressibility constraint
            'τpskel': None, # Skeleton stabilization on pressure
        }
        self.__time_stepping_parameters = {
            "t" : 0, # Initial time [s]
            "dt" : None, # (Initial) time-step size [s]
            "T" : None, # Final time [s]
            "method" : "", # "Crank-Nicolson", "CN", "Explicit-Euler", "EE", "Implicit-Euler", "IE","Runge-Kutta-4", "RK4"
            "CN_post_process" : None, # Whether to multiply the uninitialized, non-inertial fields by two after first Crank-Nicolson timestep.
            "adaptive_method" : 0, # 0: None, 1: succesive halving, 2: ...
            "minimal_dt" : 0, # Error is thrown after adaptive method results in a dt below this dt
            "repeat_halved_dt": 0 # The number of times that a halved-timestep is repeated before increasing again
        }
        self.__post_processing_parameters = {
            "export_folder" : "", # Folder where pvd, dill, and VTU output is exported
            "plot_interval" : 0,  # Number of time-steps between plots, 0 means never, -1 means last time step.
            "plot_fields"   : [], # Names of the fields that should be outputted ('u', 'p', etc). Empty means all.
            "plot_mesh_overlay" : True, # Whether to include the mesh on plots
            "plot_sample_degree" : 0, # Number of evaluation points per axis in element for plotting.
            "pvd_filename"  : "", # File name of VTU output, empty means no output
            "pvd_interval"  : 0,  # Number of time-steps between VTU exports, 0 means never, -1 means last time step.
            "pvd_fields"    : [], # Names of the fields that should be outputted ('u', 'p', etc). Empty means all.
            "pvd_sample_degree" : 0, # Number of evaluation points per axis in element for pvd export.
            "dill_filename" : "", # File name of dill output, empty means no output
            "dill_interval" : 0,  # Number of time-steps between dill saves, 0 means never, -1 means last time step.
            "plot_refinement_details" : False, # Whether additional information during the refinement is graphed (mostly for debugging)
        }
        if default:
            self.set_physical_parameters( **_get_default_physical_parameters() )
            self.set_finite_element_parameters( **_get_default_finite_element_parameters() )
            self.set_time_stepping_parameters( **_get_default_time_stepping_parameters() )
            self.set_post_processing_parameters( **_get_default_post_processing_parameters() )
    
    def __str__(self):
        full_str = "Model parameters:\n"
        for key,item in self.__model_parameters.items():
            full_str += str(key)+":\t\t"+str(item)+"\n"
        full_str += "\nPhysical parameters:\n"
        for key,item in self.__physical_parameters.items():
            full_str += str(key)+":\t\t"+str(item)+"\n"
        full_str += "\nFinite element parameters:\n"
        for key,item in self.__finite_element_parameters.items():
            full_str += str(key)+":\t\t"+str(item)+"\n"
        full_str += "\nStabilization parameters:\n"
        for key,item in self.__stabilization_parameter.items():
            full_str += str(key)+":\t\t"+str(item)+"\n"
        full_str += "\nTime stepping parameters:\n"
        for key,item in self.__time_stepping_parameters.items():
            full_str += str(key)+":\t\t"+str(item)+"\n"
        full_str += "\nPost processing parameters:\n"
        for key,item in self.__post_processing_parameters.items():
            full_str += str(key)+":\t\t"+str(item)+"\n"
        return full_str

    
    @property
    def __parameterset_map(self):
        """
        Relates parameter-set tags to the actual dicts. Is a property to do this 
        dynamically.
        """
        return {"phys": self.__physical_parameters , \
                "FE": self.__finite_element_parameters , \
                "time": self.__time_stepping_parameters , \
                "pp": self.__post_processing_parameters , \
                "model": self.__model_parameters, \
                "stab": self.__stabilization_parameter }
    
    def _convert_rlist(self,val):
        """
        Return the value for the current refinement level for an ``rlist`` parameter.
        """
        if type(val) == rlist:
            return val[self.refinement]
        return val

    def _convert_rlist_set(self,set):
        """
        Return the full parameter dictionary where all ``rlist`` parameters are
        converted to their values at the current refinement level.
        """
        newset = {}
        for key,val in set.items():
            newset = {key:self._convert_rlist(val), **newset}
        return newset

    def get(self,set,key=None):
        """
        Get the value of ``key`` of the parameter set ``set``.

        Parameters
        ----------
        set : str
            Name of the set: "model", "phys", "FE", "stab", "time" or "pp"
        key : str, optional
            The particular parameter of the set. If ``None`` it returns the
            whole set.
            
        Returns
        -------
        dict or parameter
            Either the specific parameter or the full set. The ``rlist`` parameters
            are chosen for the current refinement level.
        """
        if key is None:
            return self._convert_rlist_set(self.__parameterset_map[set])
        return self._convert_rlist(self.__parameterset_map[set][key])

    def get_keys(self,set):
        """
        Get all keys of the parameter set ``set``.

        Parameters
        ----------
        set : str
            Name of the set: "model", "phys", "FE", "stab", "time" or "pp"
            
        Returns
        -------
        dict_keys
            Keys of parameter dict.
        """
        return self.__parameterset_map[set].keys()

    def set_physical_parameters(self,**kwargs):
        """
        Set a (or multiple) parameters of the parameter set ``phys``.

        Parameters
        ----------
        kwargs : dict
            key-to-value pairs that are set

        Examples
        --------
        >>> my_problem.set_physical_parameters(μ2=1.813E-5)
        """
        self.__set_parameters(self.__physical_parameters,**kwargs)
    
    def set_finite_element_parameters(self,**kwargs):
        """
        Set a (or multiple) parameters of the parameter set ``FE``.

        Parameters
        ----------
        kwargs : dict
            key-to-value pairs that are set

        Examples
        --------
        >>> my_problem.set_finite_element_parameters(k=0,Pp=2,Pu=3,Pφ=3,Pμ=3,tol_newton_solver=1E-8)
        """
        self.__set_parameters(self.__finite_element_parameters,**kwargs)
        if "refinement_levels" in kwargs:
            self.refinement_levels = kwargs["refinement_levels"]
            for rl in self.all_rlists:
                rl.refinement_levels = kwargs["refinement_levels"]
    
    def set_time_stepping_parameters(self,**kwargs):
        """
        Set a (or multiple) parameters of the parameter set ``time``.

        Parameters
        ----------
        kwargs : dict
            key-to-value pairs that are set

        Examples
        --------
        >>> my_problem.set_time_stepping_parameters(method=rlist({-1:"CN"},default="IE"))
        """
        self.__set_parameters(self.__time_stepping_parameters,**kwargs)
    
    def set_post_processing_parameters(self,**kwargs):
        """
        Set a (or multiple) parameters of the parameter set ``pp``.

        Parameters
        ----------
        kwargs : dict
            key-to-value pairs that are set

        Examples
        --------
        >>> my_problem.set_post_processing_parameters(plot_filename="%s.jpg",plot_fields=['φ','p - 0.5 ζmix_ij δ_ij']
        """
        self.__set_parameters(self.__post_processing_parameters,**kwargs)
    
    def set_model_parameters(self,**kwargs):
        """
        Set a (or multiple) parameters of the parameter set ``model``.

        Parameters
        ----------
        kwargs : dict
            key-to-value pairs that are set

        Examples
        --------
        >>> my_problem.set_model_parameters(zero_mean_pressure=True)
        """
        self.__set_parameters(self.__model_parameters,**kwargs)
    
    def set_stabilization_parameters(self,**kwargs):
        """
        Set a (or multiple) parameters of the parameter set ``stab``.

        Parameters
        ----------
        kwargs : dict
            key-to-value pairs that are set

        Examples
        --------
        >>> my_problem.set_stabilization_parameters(τLSIC=1E5)
        """
        self.__set_parameters(self.__stabilization_parameter,**kwargs)

    def __set_parameters(self,parameter_set,**kwargs):
        """
        Actual implementation of the other set_..._parameters methods.
        Also checks and stores whether passed parameters are of type ``rlist``,
        such that all their ``refinement_levels`` can be updated when needed.
        """
        for kwarg in kwargs:
            val = kwargs[kwarg]
            if type(val) == rlist:
                val.refinement_levels = self.refinement_levels
                self.all_rlists.append(val)
            if kwarg in parameter_set:
                parameter_set[kwarg] = val
            else:
                treelog.warning("Warning! Parameter %s not a valid parameter"%kwarg)

    def define_expression(self,name,function, args=None):
        """
        Adds this function to the namespace for each newly created solutionset.
        Useful for defining exact solutions, error evaluation, boundary conditions, etc.

        Parameters
        ----------
        name : str
            The name added to the NameSpace
        function : str or function.Array
            The expression
        args : list of str, optional
            The ordered names of arguments to function, as defined in the NameSpace.
            Example: ['x','t']
        """
        self.user_defined_expressions.append( (name,function,args) )

    def get_residual(self,*args,**kwargs):
        return get_residual(self,*args,**kwargs)

    def get_residual_bdy(self,*args,**kwargs):
        return get_residual_bdy(self,*args,**kwargs)

    def get_inertia(self,*args,**kwargs):
        return get_inertia(self,*args,**kwargs)
    
    def initialize_namespace_model_expressions(self,*args,**kwargs):
        return initialize_namespace_model_expressions(self,*args,**kwargs)
    
    def get_solution_fields_list(self,*args,**kwargs):
        return get_solution_fields_list(self,*args,**kwargs)

        

def _get_default_physical_parameters():
    """
    Returns a dictionary with the default physical parameters for the case of water/air. 
    """
    default_parameters = {
            "ρ1" : 1e3, # Density of water [kg/m3]
            "ρ2" : 1, # Density of air [kg/m3]
            "μ1" : 1e-3, # Dynamic viscosity of water [kg/m/s]
            "μ2" : 1E-4, # Dynamic viscosity of air [kg/m/s]
            "σ01" : 1e-1, # Surface tension solid - water [kg/s2]
            "σ02" : 1e-1, # Surface tension solid - air [kg/s2]
            "σ12" : 7.28e-2, # Surface tension water - air [kg/s2]
            "m" : 64e-12, # Mobility [m2/s]
            "ε" : 5E-2, # ~1/4th of phase-field thickness parameter [m]
        }
    return default_parameters

def _get_default_finite_element_parameters():
    """
    Returns a dictionary with the default finite element parameters for a 
    unit square with 10 elements along each axis. 
    """ 
    default_parameters = {
            "Pu" : 1, # Polynomial order of the basis functions for velocity
            "Pp" : 1, # Polynomial order of the basis functions for pressure
            "Pφ" : 1, # Polynomial order of the basis functions for order-parameter
            "Pμ" : 1, # Polynomial order of the basis functions for chemical-potential
            "tol_newton_solver" : 1E-10, # Absolute tolerance of the residual
            "degree" : 8, # Integration degree of all terms
            "refinement_fraction" : 0.25, # Elements are refined that are resposible for this percentile of the estimated error
        }
    return default_parameters

def _get_default_time_stepping_parameters():
    """
    Returns a dictionary with the default time-stepping parameters for a 
    10 microsecond simulation with time-steps of 2.5 microseconds. 
    """ 
    default_parameters = {
            "dt" : 2.5e-6, # Maximum time-step size [s]
            "T" : 1E-5, # Final time [s]
            "method" : "IE",
            "CN_post_process" : True, # Whether to multiply the uninitialized, non-inertial fields by two after first Crank-Nicolson timestep.
            "repeat_halved_dt": 4 # The number of times that a halved-timestep is repeated before increasing again
        }
    return default_parameters
    
def _get_default_post_processing_parameters():
    """
    Plot φ at every timestep, dill save at last timestep.
    """ 
    default_parameters = {
            "export_folder" : "output", # Folder where pvd, dill, and VTU output is exported.
            "plot_interval" : 1, # Number of time-steps between plots, 0 means never, -1 means last time step.
            "plot_fields"   : [], # Names of the fields that should be outputted ('u', 'p', etc). Empty means none.
            "plot_sample_degree" : 5, # Number of evaluation points per axis in element for plotting.
            "pvd_filename"  : os.path.basename(__main__.__file__)[:-3], # File name of pvd file and VTU folder, empty means no output.
            "pvd_interval"  : 1,  # Number of time-steps between VTU exports, 0 means never, -1 means last time step.
            "pvd_fields"    : ['u','p','φ'], # Names of the fields that should be outputted ('u', 'p', etc). Empty means none.
            "pvd_sample_degree" : 5, # Number of evaluation points per axis in element for pvd export.
            "dill_filename" : os.path.basename(__main__.__file__)[:-3], # File name of dill output, empty means no output.
            "dill_interval" : -1, # Number of time-steps between dill saves, 0 means never, -1 means last time step.
        }
    return default_parameters
    
