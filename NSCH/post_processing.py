from __future__ import annotations
from nutils import export
from matplotlib import collections
import os, numpy, meshio, dill
import treelog
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from .solution_set import SolutionSet


def export_plot(solution_set:SolutionSet,plot_fields,plotmesh=True,solution_dict=None,filename="%s.png"):
    """
    Generates a plot with the given name of the described fields, based 
    on the data in the passed ``SolutionSet``. With the option of manually
    passing the solution dictionary.

    Parameters
    ----------
    solution_set : SolutionSet
        The solution set that holds the basis on which the solution is 
        defined, as well as the solution vectors.
    plot_fields : list of str
        List of expressions to be plotted. Can be simply the solution fields, 
        or manipulations thereof.
    plotmesh : bool, optional
        Whether to overlay the plot with the mesh (typically deactivated for
        higher refinement levels).
    solution_dict : dict of array, optional
        If passed this takes precedence over the solution vectors from the
        given solution_set.
    filename : str, optional
        Name of the plot file. This may include the `%s`-format string, which 
        is replaced by the fieldname/expression that is being plotted.
    """
    sample = solution_set.domain_definition.plot_sample
    xy,*field_vals = _get_sampled_values(solution_set,sample,plot_fields,solution_dict) 
    for field_name,vals in zip(plot_fields,field_vals):
        plotname = filename%field_name if "%s" in filename else filename
        plottitle = field_name + ' time=%.6gs' % solution_set.t
        vecaxes = solution_set.domain_definition.plot_vec_axes
        _produce_plot(plotname,sample,xy[:,vecaxes],vals,plotmesh,plottitle,vecaxes)
        

def _produce_plot(plotname,sample,xy,vals,plotmesh,plottitle,vecaxes=(0,1)):
    """
    Actual matplotlib plot implementation. Front-facing implementation: ``export_plot``
    """
    vals[numpy.isnan(vals)] = 0 # matplotlib hates nans
    plot_vals = vals if type(vals[0]) != numpy.ndarray else numpy.linalg.norm(vals[:,vecaxes], ord=2, axis=1) # scalar or vector
    with export.mplfigure(plotname) as fig:
        ax = fig.add_subplot(111, aspect='equal', title=plottitle)
        im = ax.tripcolor(*xy.T, sample.tri, plot_vals, shading='gouraud', cmap='coolwarm')
        if type(vals[0]) == numpy.ndarray: # vector
            ax.quiver(*xy.T, vals[:,vecaxes[0]], vals[:,vecaxes[1]], angles='xy', scale_units='xy')
        if plotmesh:
            ax.add_collection( collections.LineCollection( xy[sample.hull], colors='k', linewidth=1, alpha=0.1) )
        fig.colorbar(im)


def export_pvd(export_folder, pvd_filename, time_step_nr, time_step):
    """ 
    Exports or updates a xml-type pvd file which combines timesteps and vtu files 
    in one data set that can be used in Paraview.

    Parameters
    ----------
    export_folder : str
        Name of the folder where the pvd file is output.
    pvd_filename : str
        Name of the pvd file.
    time_step_nr : int
        Time-step number in simulation which is used to number vtu files.
    time_step : float
        Simulation time linked to the vtu file that is added to the pvd file.
    """
    filenamepath = f"{export_folder}/{pvd_filename}.pvd"
    if not os.path.exists(filenamepath):
        with open(filenamepath,"w") as pvdfile:
            pvdfile.write('<?xml version="1.0"?>\n<VTKFile type="Collection" version="0.1">\n  <Collection>\n')
            pvdfile.write('  </Collection>\n</VTKFile>')
    with open(filenamepath,"r+") as pvdfile:
        lines = pvdfile.readlines()
        for index, line in enumerate(lines):
            if '</VTKFile>' in line:
                lines.insert(index-1, f'    <DataSet timestep="{time_step}" part="0" file="{pvd_filename}_VTU/VTU_{time_step_nr}.vtu"/>\n')
                break
        pvdfile.seek(0)
        pvdfile.writelines(lines)


def export_vtu(filename,solution_set:SolutionSet):
    """
    Exports the different fields in the ``solution_set`` to a vtu file with the 
    filename ``filename``.

    Parameters
    ----------
    filename : str
        Name of the vtu file.
    solution_set : SolutionSet
        The solution set whose fields are exported.
    """
    solution_dict = solution_set.solution_dict
    field_names = [name.strip("dofs") for name in solution_dict.keys()]
    sample = solution_set.domain_definition.pvd_sample
    xy,*field_vals = _get_sampled_values(solution_set,sample,field_names,solution_dict) 
    vtu_fields = dict(zip(field_names,field_vals))
    if solution_set.domain.ndims == 3:
        meshio.write_points_cells("{}.vtu".format(filename), xy, {"tetra": sample.tri}, point_data=vtu_fields)
    elif solution_set.domain.ndims == 2:
        meshio.write_points_cells("{}.vtu".format(filename), xy, {"triangle": sample.tri}, point_data=vtu_fields)
    elif solution_set.domain.ndims == 1:
        meshio.write_points_cells("{}.vtu".format(filename), xy, {"line": sample.tri}, point_data=vtu_fields)
    else:
        treelog.warning("Unexpected dimension of domain in VTU export.")
    treelog.user(f"Exported to {filename}")


def export_dill(filename,solution_sets:SolutionSet):
    """
    Exports the complete list of ``SolutionSet`` objects. This is used to enable 
    restarting from that point, by loading with ``load_dill``.

    Parameters
    ----------
    filename : str
        Name of the dill file.
    solution_sets : list of SolutionSet
        The solution sets that are exported.
    """
    with open(filename,'wb') as dill_object:
        dill.dump(solution_sets, dill_object)
    treelog.user(f"Exported to {filename}")


def load_dill(filename):
    """
    Imports a complete list of ``SolutionSet`` objects stored as a
    dill file, exported with ``export_dill``.

    Parameters
    ----------
    filename : str
        Name of the dill file.
        
    Returns
    -------
    tuple
        ``t``, ``solution_sets`` for the time and list
        of ``SolutionSet`` objects.
    """
    with open(filename,'rb') as dill_object:
        solution_sets = dill.load(dill_object)
        t = solution_sets[-1].t
    return t,solution_sets


def _get_sampled_values(solution_set:SolutionSet,sample,plot_fields,solution_dict):
    """
    Returns the list of coefficient vectors for the evaluated fields on the sample.
    """
    solution_dict = solution_set.solution_dict if solution_dict is None else solution_dict
    fieldlist = _get_fields_from_namespace(solution_set,plot_fields)
    intθ = solution_set.domain_definition.intθ
    return sample.eval([intθ(solution_set.ns.x), *[intθ(field) for field in fieldlist]], **solution_dict)


def _get_fields_from_namespace(solution_set:SolutionSet,plot_fields):
    """
    Returns a list of namespace-attributes corresponding to the field-strings in the 
    ``plot_fields`` list.
    """
    fieldlist = []
    ns = solution_set.ns.copy_()
    for field_name in plot_fields:
        if field_name in solution_set.get_fieldnames():
            field = getattr(ns, field_name)
        else:
            ns.fieldname = field_name
            field = ns.fieldname
        fieldlist.append(field)
    return fieldlist


def get_output_directory(base_dir_name, i=0):
    """
    Returns the name of the directory in which ``dill`` and pvd files are exported to. 
    If the directory already exists, it appends an ``_%i`` behind it, for the lowest 
    integer for which the directory does not yet exists

    Parameters
    ----------
    base_dir_name : str
        Name of the output directory without suffix.
    i : int, optional
        The suffix. If the output directory with suffix exists, this function is
        recursively called with a higher ``i``.
        
    Returns
    -------
    str
        Name of the output directory with lowest integer suffix that does not yet exist.
    """
    appendix = "_"+str(i) if i > 0 else ""
    if not os.path.isdir(base_dir_name+appendix):
        return base_dir_name+appendix
    return get_output_directory(base_dir_name, i=i+1)