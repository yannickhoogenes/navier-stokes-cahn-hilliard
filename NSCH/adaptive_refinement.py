from __future__ import annotations
import numpy as np
from nutils import solver, sparse, function
from .problem_definition import ProblemDefinition
from .weak_formulations import *
import treelog
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from .solution_set import SolutionSet
    


def get_refinement_level_previous(refinement_level:int,problem_definition:ProblemDefinition):
    """
    Returns the refinement level that should act as the solution at the
    previous time step. This is a trade-off: always choosing the highest
    refinement level requires costly integrations and potentially more 
    Newton iterations, while at equal level disconnects the different 
    refinement levels. Current default is simply "refinement_level+1".

    Parameters
    ----------
    refinement_level : int
        Current refinement level.
    problem_definition : ProblemDefinition
        Contains refinement and model settings.
    
    Returns
    -------
    int
        Refinement level referenced as previous solution.
    """
    max_depth = problem_definition.get("FE","refinement_levels")
    return min(max_depth, refinement_level+1)

        
def get_refinement_indices(problem_definition:ProblemDefinition,solution_set:SolutionSet,solution_set_prev:SolutionSet):
    """
    Checks if the refinement parameters are set correctly and selects the
    correct inputs for the mark function to return a set of elements of 
    the unrefined mesh that should be refined.

    Parameters
    ----------
    problem_definition : ProblemDefinition
        Contains refinement and model settings.
    solution_set : SolutionSet
        SolutionSet for one refinement level of the current time-step.
    solution_set_prev : SolutionSet 
        SolutionSet for one refinement level of the previous time-step.
    
    Returns
    -------
    set
        A set of elements of the unrefined mesh that are selected for 
        refinement based on refinement parameters and settings
    """
    method = problem_definition.get("FE","refinement_method")
    measure = problem_definition.get("FE","refinement_measure")
    fraction = problem_definition.get("FE","refinement_fraction")
    threshold = problem_definition.get("FE","refinement_threshold")
    plot_details = problem_definition.get("pp","plot_refinement_details")

    if method == 0:
        treelog.error("Specified more than zero refinement levels but no refinement method")
        exit()

    if method in [1,2,3]: # Residual based refinement
        merged_ref_set,vectors_bases_type = get_refined_residual_vector(problem_definition,solution_set,solution_set_prev)
        vectors_bases_type = get_error_bounds_vector(measure,merged_ref_set,vectors_bases_type)
        return mark(merged_ref_set,vectors_bases_type,fraction,threshold, plot_details=plot_details)

    treelog.error("Specified refinement method not implemented")
    exit()


def get_refined_residual_vector(problem_definition:ProblemDefinition,solution_set:SolutionSet,solution_set_prev:SolutionSet):
    """
    Returns the correct residual integral on a super-refinement for 
    residual-based refinement.

    Parameters
    ----------
    problem_definition : ProblemDefinition
        Contains refinement and model settings
    solution_set : SolutionSet
        SolutionSet for the current refinement level and time-step
    solution_set_prev : SolutionSet
        SolutionSet of the previous time-step

    Returns
    -------
    SolutionSet
       Merged unrefined and refined `SolutionSet`
    list of lists
        That contain the nodal residual contribution, the field basis 
        corresponding to these nodes, and the field type.
    """
    method = problem_definition.get("FE","refinement_method")
    if method == 1:
        return get_refined_residual_vector_steady_Cahn_Hilliard(problem_definition,solution_set)
    if method == 2:
        return get_refined_residual_vector_dynamic_Cahn_Hilliard(problem_definition,solution_set,solution_set_prev)
    if method == 3:
        return get_refined_residual_vector_full_weak_form(problem_definition,solution_set,solution_set_prev)


def get_error_bounds_vector(measure,merged_ref_set:SolutionSet,vectors_bases_types):
    """
    Returns the manipulation of the residual integral vector to yield
    contributions to a normed error. The current naive implementation
    simply weighs in the element volume.

    Parameters
    ----------
    measure : int
        0: Simply take absolute values
        1: Element-weighting
        2: TODO L2-norm
    merged_ref_set : SolutionSet
       Merged unrefined and refined `SolutionSet`
    vectors_bases_type : list of lists
        That contain the nodal residual contribution, the field basis 
        corresponding to these nodes, and the field type.

    Returns
    -------
    list of lists
        That contain the updated nodal residual contribution, the field 
        basis corresponding to these nodes, and the field type.
    """
    vbt = []
    for vector,basis,field_type in vectors_bases_types:
        if measure==0:
            vbt.append((numpy.abs(vector),basis,field_type))
        elif measure==1:
            weight = merged_ref_set.domain.integrate( basis * merged_ref_set.ns.dV , degree=0)
            if field_type=='vector':
                weight = weight.export('dense').sum(1) # sum over axis 1
            vbt.append( (numpy.abs(vector)*weight,basis,field_type) )
        else:
            treelog.error("This refinement measure has not been implemented")
            exit()
    return vbt
        

def mark(merged_ref_set:SolutionSet,vectors_bases_type,fraction,threshold=0,plot_details=False):
    """
    Determines which element of the unrefined mesh needs to be refined
    based on the nodal residuals and refinement settings.

    Parameters
    ----------
    merged_ref_set : SolutionSet
       Merged unrefined and refined `SolutionSet`
    vectors_bases_type : list of lists
        Contains the nodal residual contribution, the field basis 
        corresponding to these nodes, and the field type.
    fraction : float
        Approximately the fraction of basis function that are refined.
    threshold : float (optional)
        Minimum value of the residual before an element is marked for
        refinement. 
    plot_details : bool (optional)
        Whether to draw a line-plit of the ordered list of all residual 
        values with refinement fraction cut-off.

    Returns
    -------
    set
        A set of elements on the unrefined domain that are marked for 
        refinement.
    """
    refine = set()
    domain = merged_ref_set.domain
    for vector,basis,field_type in vectors_bases_type:
        mean = numpy.mean(vector)
        d = min(mean-numpy.min(vector), numpy.max(vector)-mean)
        mask = ( vector > mean+d*(0.5-fraction) + threshold ) # whereby fraction is an actual fraction if vector is a linear distribution
        if plot_details:
            _plot_residual_distribution_and_cutoff(vector,mask)
        if field_type == "scalar":
            supp_ind = get_support_scalar_basis(domain, basis, mask)
        elif field_type == "vector":
            supp_ind = get_support_vector_basis(domain, basis, mask)
        refine.update(supp_ind)
    treelog.info(f"Refining {len(refine)}/{len(domain)} elements.")
    return refine

def _plot_residual_distribution_and_cutoff(vector,mask):
    """
    Matplotlib plotting implementation of the residual distribution
    and the cut-off line for refinement.
    """
    from nutils import export
    with export.mplfigure("marked_residual.png") as fig:
        ax = fig.add_subplot(111)
        ax.plot(range(len(vector)),numpy.sort(vector))
        ax.plot([len(vector)-sum(mask),len(vector)-sum(mask)],[0,max(vector)])


def get_refined_residual_vector_steady_Cahn_Hilliard(problem_definition:ProblemDefinition,solution_set:SolutionSet):
    """
    Determines the function to obtain the nodal residual vector based
    on primal or mixed Cahn-Hilliard model for a time independent problem.

    Parameters
    ----------
    problem_definition : ProblemDefinitions
        Contains model settings and parameters.
    solution_set : SolutionSet
        The unrefined SolutionSet. 
        
    Returns
    -------
    SolutionSet
       Merged unrefined and refined `SolutionSet`
    list of lists
        Contains the nodal residual contribution, the field basis 
        corresponding to these nodes, and the field type.
    """
    # Refines wherever the solution least resembles a steady Cahn-Hilliard profile
    get_residual_CH_vol_potential = get_residual_CH_mixed_potential if problem_definition.get("model","chemical_potential") \
                        else get_residual_CH_primal
    get_residual_CH_vol_transport = get_residual_CH_mixed_transport_conservative if problem_definition.get("model","Conservative_CH") \
                        else get_residual_CH_mixed_transport
    get_residual_CH_bdy = get_residual_CH_mixed_bdy if problem_definition.get("model","chemical_potential") \
                        else get_residual_CH_primal_bdy
    get_residual_CH = lambda solution_set: get_residual_CH_vol_potential(solution_set)+get_residual_CH_vol_transport(solution_set)+get_residual_CH_bdy(solution_set)
    fields = ["φ","μ"] if problem_definition.get("model","chemical_potential") else ["φ"]
    return get_refined_residual_vector_steady(solution_set,get_residual_CH,fields)


def get_refined_residual_vector_dynamic_Cahn_Hilliard(problem_definition:ProblemDefinition,solution_set:SolutionSet,solution_set_prev:SolutionSet):
    """
    Determines the function to obtain the nodal residual
    vector based on primal or mixed Cahn-Hilliard model
    for a time dependent problem.

    Parameters
    ----------
    problem_definition : ProblemDefinitions
        Contains model settings and parameters.
    solution_set : SolutionSet
        The unrefined SolutionSet of the current time-setp.
    solution_set_prev : SolutionSet
        The unrefined SolutionSet of the previous time-step.
    Returns
    -------
    SolutionSet
       Merged unrefined and refined `SolutionSet`
    list of lists
        List of lists that contain the nodal residual 
        contribution, the field basis corresponding to 
        these nodes, and the field type.
    """
    # Refines wherever the solution least resembles an unsteady Cahn-Hilliard profile
    get_residual_CH_vol_potential = get_residual_CH_mixed_potential if problem_definition.get("model","chemical_potential") \
                        else get_residual_CH_primal
    get_residual_CH_vol_transport = get_residual_CH_mixed_transport_conservative if problem_definition.get("model","Conservative_CH") \
                        else get_residual_CH_mixed_transport
    get_residual_CH_bdy = get_residual_CH_mixed_bdy if problem_definition.get("model","chemical_potential") \
                        else get_residual_CH_primal_bdy
    get_residual_CH = lambda solution_set: get_residual_CH_vol_potential(solution_set)+get_residual_CH_vol_transport(solution_set)+get_residual_CH_bdy(solution_set)
    fields = ["φ","μ"] if problem_definition.get("model","chemical_potential") else ["φ"]
    return get_refined_residual_vector_dynamic(solution_set,solution_set_prev,get_inertia_CH,get_residual_CH,fields)


def get_refined_residual_vector_full_weak_form(problem_definition:ProblemDefinition,solution_set:SolutionSet,solution_set_prev:SolutionSet):
    """
    Determines the function to obtain the nodal residual
    vector based on primal or mixed Cahn-Hilliard-Navier-Stokes
    model for a time dependent problem.

    Parameters
    ----------
    problem_definition : ProblemDefinitions
        Contains model settings and parameters.
    solution_set : SolutionSet
        The unrefined SolutionSet of the current time-setp.
    solution_set_prev : SolutionSet
        The unrefined SolutionSet of the previous time-step.
    Returns
    -------
    SolutionSet
       Merged unrefined and refined `SolutionSet`
    list of lists
        List of lists that contain the nodal residual 
        contribution, the field basis corresponding to 
        these nodes, and the field type.
    """
    get_inertia_full = lambda solution_set: problem_definition.get_inertia(solution_set)
    get_residual_full = lambda solution_set: problem_definition.get_residual(solution_set) \
                                            + problem_definition.get_residual_bdy(solution_set)
    fields = solution_set.get_fieldnames()
    return get_refined_residual_vector_dynamic(solution_set,solution_set_prev,get_inertia_full,get_residual_full,fields)


def get_refined_residual_vector_steady(solution_set:SolutionSet,residual,fields):
    """
    Obtains the nodal residual vector and the combined
    domain of the unrefined and refined
    SolutionSet for a time indepedent problem. 

    Parameters
    ----------
    solution_set : SolutionSet
        The unrefined SolutionSet.
    residual : function
        A function that returns the residual integral of a 
        specific solution.
    fields : list of str
        A list with the fields of interest that need to be
        evaluated for the residual.

    Returns
    -------
    SolutionSet
       Merged unrefined and refined `SolutionSet`
    list of lists
        List of lists that contain the nodal residual 
        contribution, the field basis corresponding to 
        these nodes, and the field type. 
    """
    ref_set = solution_set.refine()
    merged_ref_set = ref_set.merge(solution_set)
    indicator_integral = residual(merged_ref_set)
    vectors_bases_type = compute_vectors_bases_types(merged_ref_set,indicator_integral,fields,solution_set.solution_dict)    
    return merged_ref_set,vectors_bases_type


def get_refined_residual_vector_dynamic(solution_set:SolutionSet,solution_set_prev:SolutionSet,inertia,residual,fields):
    """
    Obtains the nodal residual vector and the combined domain of 
    the unrefined and refined SolutionSet for a time depedent problem. Based on
    an implicit euler approximation of the time derivative.

    Parameters
    ----------
    solution_set : SolutionSet
        The unrefined SolutionSet of the current time-setp.
    solution_set_prev : SolutionSet
        The unrefined SolutionSet of the previous time-step.
    inertia : function
        A function that returns the Cahn-Hilliard mass 
        integral of a specific solution.
    residual : function
        A function that returns the residual integral of a 
        specific solution.
    fields : list of str
        A list with the fields of interest that need to be
        evaluated for the residual.

    Returns
    -------
    SolutionSet
       Merged unrefined and refined `SolutionSet`
    list of lists
        List of lists that contain the nodal residual 
        contribution, the field basis corresponding to 
        these nodes, and the field type. 
    """
    ref_set = solution_set.refine()
    merged_ref_set = ref_set.merge(solution_set)
    merged_ref_set_prev = ref_set.merge(solution_set_prev)

    solution_dict_old = {key+'old': solution_set_prev.solution_dict[key] for key in solution_set_prev.solution_dict.keys() }
    replace_new_old = { key:function.Argument(key+'old',solution_set_prev.solution_dict[key].shape) for key in solution_set_prev.solution_dict.keys() }
    replace_new_old['t'] = function.Argument( 'told',() )
    inertia_prev = inertia(merged_ref_set_prev).replace(replace_new_old)

    indicator_integral = 1/(solution_set.t-solution_set_prev.t)*(inertia(merged_ref_set)-inertia_prev)+residual(merged_ref_set)
    vectors_bases_type = compute_vectors_bases_types(merged_ref_set,indicator_integral,fields,{**solution_set.solution_dict,**solution_dict_old})
    return merged_ref_set,vectors_bases_type


def compute_vectors_bases_types(solution_set:SolutionSet,indicator_integral,fields,arguments):
    """
    Computes the nodal residual vector for each field of
    interest.

    Parameters
    ----------
    solution_set : SolutionSet
        Merge of the unrefined and refined SolutionSet
    indicator_integral : 
        Unevaluated residual integral of merged SolutionSet
    fields : list of str
        A list with the fields that need to be evaluated for 
        the residual
    arguments : dict
        A dictionary of the unrefined solution

    Returns
    -------
    list of lists
        List of lists that contain the nodal residual 
        contribution, the field basis corresponding to 
        these nodes, and the field type.
    """
    vectors_bases_types = []
    for field in fields:
        basis = solution_set.bases[ tuple(solution_set.field_data[field][key] for key in ['P','k','type']) ]
        if basis is None: # LMP-basis
            continue
        indicator_integral_vector = indicator_integral.derivative(field+"testdofs")
        vector = indicator_integral_vector.eval(**arguments)
        field_type = solution_set.field_data[field]['type']
        vectors_bases_types.append([vector,basis,field_type])
    return vectors_bases_types


def inter_mesh_projection(solution_set_new:SolutionSet,solution_set_old:SolutionSet,solution_dict):
    """
    Projects the solution with coefficients in `solution_dict` on the basis
    of `solution_set_old` onto the basis of `solution_set_new`.
    
    Parameters
    ----------
    solution_set_new : SolutionSet
       SolutionSet of the new time-step to which the solution
        is projected
    solution_set_old : SolutionSet
        SolutionSet of the old time-step from which the solution
        is projected
    solution_dict : dict
        Dictionary containing the solution of solution_set_old
    Returns
    -------
    dict
        Dictionary containing the solution of solution_set_old
        projected on the domain of solution_set_new
    """

    common_domain = solution_set_new.merge_domain(solution_set_old)
    intθ = solution_set_new.domain_definition.intθ
    lhs = {}

    for fielddofs, value in solution_dict.items():
        field = fielddofs.replace("dofs",'')
        field_data = solution_set_old.field_data[field]
        basis = solution_set_old.bases[ tuple(field_data[key] for key in ['P','k','type']) ]
        if field_data["type"] == "vector":
            delta = getattr(solution_set_new.ns, field) - value @ basis @ solution_set_old.domain_definition.rot
            degree = solution_set_new.get_degree(field) * 2
            sqr = common_domain.integral(intθ(delta @ delta * solution_set_new.ns.dV), degree=degree)
            value = solver.optimize(fielddofs, sqr)
        elif field_data["type"] == "scalar":
            delta = getattr(solution_set_new.ns, field) - value @ basis
            degree = solution_set_new.get_degree(field) * 2
            sqr = common_domain.integral(intθ(delta**2 * solution_set_new.ns.dV), degree=degree)
            value = solver.optimize(fielddofs, sqr)
        lhs[fielddofs] = value

    return lhs


def get_support_scalar_basis(domain,basis,mask):
    """
    Obtains the element support on the unrefined domain of
    the elements with a sufficiently large residual on the
    refined mesh, which is based on the nodal residual contributions.

    Parameters
    ----------
    domain : domain
       domain of the DomainDefinition of the
       merge of the unrefined and refined SolutionSet
    basis : basis
        Basis function of the refined field on the merge of the
        unrefined and refined SolutionSet
    mask : list of bool
        True if the residual of the node is sufficiently large
        to be marked for refinement
    Returns
    -------
    array
        Array of element indices of the unrefined domain
        which are marked for refinement
    """
    return domain.transforms[basis.get_support(mask)]


def get_support_vector_basis(domain, basis, mask):
    """
    Obtains the element support on the unrefined domain of
    the elements with a sufficiently large residual on the
    refined mesh, which is based on the nodal residual contributions.

    Parameters
    ----------
    domain : domain
       domain of the DomainDefinition of the
       merge of the unrefined and refined SolutionSet
    basis : basis
        Basis function of the refined field on the merge of the
        unrefined and refined SolutionSet
    mask : list of bool
        True if the residual of the node is sufficiently large
        to be marked for refinement
    Returns
    -------
    set
        Set of element indices of the unrefined domain
        which are marked for refinement
    """
    '''determine local support in the form of a dof -> transform chains mapping.'''
    # NOTE: the following algorithm (kronecker + integrate) is sub-optimal but
    # currently the most future proof. Later we should change this to a routine
    # that directly evaluates the locally supported dofs.
    f = function.kronecker(basis.sum(range(1, basis.ndim)), pos=domain.f_index, length=len(domain), axis=0)
    supp = [[] for i in range(len(basis))]
    for ielem, idof in zip(*sparse.indices(domain.sample('gauss', 1).integrate_sparse(f))):
        supp[idof].append(domain.transforms[ielem])

    refine = set()
    for idof in mask.nonzero()[0]:
        transforms = supp[idof]
        minlen = min(len(trans) for trans in transforms)
        refine.update(trans for trans in transforms if len(trans) == minlen)

    return refine
