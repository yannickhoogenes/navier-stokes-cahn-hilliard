from __future__ import annotations
from nutils import solver,function
import numpy
import treelog
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from .solution_set import SolutionSet

def get_implemented_RK_methods():
    """
    Get a list of all implemented Runge-Kutta methods, including their abbreviations

    Returns
    -------
    list of str
        Names and abbreviations
    """
    return ["Runge-Kutta4", "RK4", "Runge-Kutta38", "RK38", "Implicit-midpoint", "IMP", "Runge-Kutta-implicit-midpoint", "RKIMP",
            "Runge-Kutta-Crouzeix", "RKC", "Runge-Kutta-Norsett", "RKN", "RKEE", "RKIE", "RKCN"]

def get_implemented_theta_methods():
    """
    Get a list of all implemented Runge-Kutta methods, including their abbreviations

    Returns
    -------
    list of str
        Names and abbreviations
    """
    return ["Implicit-Euler", "IE", "Explicit-Euler", "EE", "Crank-Nicolson", "CN"]

def take_time_step(method,**kwargs):
    """
    Forwards the call to the appropriate method.

    Parameters
    ----------
    method : string
        The name or abbreviation for the time-stepping method
    kwargs : dict
        These arguments are forwarded. See the documentation of either ``theta_method`` 
        or ``explicit_runge_kutta_method``, or ``diagonal_implicit_runge_kutta_method`` 
        for arguments specific to that method.
        
    Returns
    -------
    dict of array
        The solution vectors for all solution fields
    """
    if method in ["Implicit-Euler", "IE"]:
        return implicit_euler(**kwargs)
    elif method in ["Explicit-Euler", "EE"]:
        return explicit_euler(**kwargs)
    elif method in ["Crank-Nicolson", "CN"]:
        return crank_nicolson(**kwargs)
    elif method in ["Explicit-midpoint", "EMP", "Runge-Kutta-explicit-midpoint", "RKEMP"]:
        return runge_kutta_explicit_midpoint(**kwargs)
    elif method in ["Runge-Kutta4", "RK4"]:
        return runge_kutta4(**kwargs)
    elif method in ["Runge-Kutta38", "RK38"]:
        return runge_kutta38(**kwargs)
    elif method in ["Implicit-midpoint", "IMP", "Runge-Kutta-implicit-midpoint", "RKIMP"]:
        return runge_kutta_implicit_midpoint(**kwargs)
    elif method in ["Runge-Kutta-Crouzeix", "RKC"]:
        return runge_kutta_crouzeix(**kwargs)
    elif method in ["Runge-Kutta-Norsett", "RKN"]:
        return runge_kutta_norsett(**kwargs)
    elif method in ["RKEE"]:
        return runge_kutta_explicit_midpoint(**kwargs) # Equivalent to regular EE
    elif method in ["RKIE"]:
        return runge_kutta_implicit_midpoint(**kwargs) # Equivalent to regular IE
    elif method in ["RKCN"]:
        return runge_kutta_crank_nicolson(**kwargs) # Equivalent to regular CN
    else:
        treelog.error(f"Unknown time-stepping method '{method}', aborting.")
        exit()


def implicit_euler(**kwargs):
    """
    Forwards the call to ``theta_method`` with theta = 1.
    """
    return theta_method(1,**kwargs)


def explicit_euler(**kwargs):
    """
    Forwards the call to ``theta_method`` with theta = 0.
    """
    return theta_method(0,**kwargs)


def crank_nicolson(**kwargs):
    """
    Forwards the call to ``theta_method`` with theta = 0.5.
    """
    return theta_method(0.5,**kwargs)


def theta_method(theta,t=0,dt=0,inertia=None,residual=None,solution_set_prev:SolutionSet=None,\
                 constraint_vector=None,inertia_prev=None,residual_prev=None,initial_guess={},newtontol=1E-12,failrelax=0,**kwargs):
    """
    Implementation of 1/dt (M_prev u_prev - M u) = (1-theta) K_prev u_prev + theta K u
    Various cases: ``EI``,  ``EE`` and ``CN``.

    Parameters
    ----------
    theta : float
        0: explicit Euler, 1: implicit Euler, 0.5: Crank-Nicolson
    t : float
        Time at start of timestep
    dt : float
        Timestep
    inertia : integral
        The mass integral on the basis of the new solution
    residual : integral
        The stiffness integral on the basis of the new solution
    solution_set_prev : SolutionSet
        The ``SolutionSet`` of the last time step
    constraint_vector : dict of array
        The masks of Dirichlet constraints, as held by the ``DomainDefinition``.
    inertia_prev : integral
        The mass integral on the basis of the last time step
    residual_prev : integral
        The stiffness integral on the basis of the last time step
    initial_guess : dict of array, optional
        Solution vectors of initial guess in case of implicit solves
    newtontol : float, optional
        Newton tolerance in case of implicit solves
        
    Returns
    -------
    dict of array
        The solution vectors for all solution fields
    """
    solution_dict_prev = solution_set_prev.solution_dict
    targets = tuple(solution_dict_prev.keys())
    
    replace_new_old = { key:function.Argument(key+'old',solution_dict_prev[key].shape) for key in targets }
    replace_new_old['t'] = function.Argument( 'told',() )
    inertia_prev = inertia_prev.replace(replace_new_old)
    if theta != 1: residual_prev = residual_prev.replace(replace_new_old)

    if theta == 1:
        transient_residual = (inertia-inertia_prev)/dt + residual
    elif theta == 0:
        transient_residual = (inertia-inertia_prev)/dt + residual_prev
    else:
        transient_residual = (inertia-inertia_prev)/dt + residual*theta + residual_prev*(1-theta)
    residual_list = [ transient_residual.derivative(key[:-4]+"testdofs") for key in targets ]

    arguments_old = {**{key+"old": solution_dict_prev[key] for key in targets},**{'told':t,'t':t+dt}}
    sol_new_and_old = solver.newton(targets, residual=residual_list, constrain=constraint_vector,failrelax=failrelax, \
        arguments={**initial_guess,**arguments_old}).solve(tol=newtontol)#linsolver="direct",
    sol_new = { key:sol_new_and_old[key] for key in targets  }
    if t==0 and theta < 1 and solution_set_prev.problem_definition.get('time','CN_post_process'):
        sol_new = _postprocess_time_independent_fields(theta,sol_new,solution_dict_prev,inertia)
    return sol_new


def _postprocess_time_independent_fields(theta,sol_new:SolutionSet,solution_dict_prev,inertia):
    """
    Theta methods with theta != 1 require initialization of all fields. Usually, we don't 
    initialize the non-timedependent fields (pressure & chemical potential). In that case,
    the theta methods effectively computes '1/theta*field' in place of 'field'. In subsequent
    timesteps, this leads to back and forth oscillations. This function compensates for that.
    """
    sol_new_ = {}
    for key,vals in solution_dict_prev.items():
        if key not in inertia.arguments.keys() and numpy.linalg.norm(vals) < 1E-13: # Time independent field that is seemingly uninitialized
            treelog.warning(f"Assuming field {key} is uninitialized, postprocessing as initial implicit Euler step")
            sol_new_[key] = sol_new[key]*theta
        else:
            sol_new_[key] = sol_new[key]
    return sol_new_


def runge_kutta_explicit_euler(**kwargs):
    """
    1-stage, 1st order, explicit.
    Equivalent to explicit Euler.
    
    Notes
    -----
    Tableau:\n
    +-----+-----+
    | 0   |     |
    +-----+-----+
    | 1   | 1   |
    +-----+-----+
    """
    tableau =  [[0  , []],
                [ 1 , [ 1 ]]]
    return explicit_runge_kutta_method(tableau,**kwargs)


def runge_kutta_explicit_midpoint(**kwargs):
    """
    2-stage, 2nd order, explicit.
    
    Notes
    -----
    Tableau:\n
    +-----+-----+-----+
    | 0   |           |
    +-----+-----+-----+
    | 1/2 | 1/2 |     |
    +-----+-----+-----+
    | 1   |  0  |  1  |
    +-----+-----+-----+
    """
    tableau =  [[0  , []],
                [1/2, [1/2]],
                [ 1 , [ 0 , 1 ]]]
    return explicit_runge_kutta_method(tableau,**kwargs)


def runge_kutta4(**kwargs):
    """
    4-stage, 4th order, explicit.
    
    Notes
    -----
    Tableau:\n
    +-----+-----+-----+-----+-----+
    | 0   |                       |
    +-----+-----+-----+-----+-----+
    | 1/2 | 1/2 |                 |
    +-----+-----+-----+-----+-----+
    | 1/2 |  0  | 1/2 |           |
    +-----+-----+-----+-----+-----+
    | 1   |  0  |  0  |  1  |     |
    +-----+-----+-----+-----+-----+
    | 1   | 1/6 | 1/3 | 1/3 | 1/6 |
    +-----+-----+-----+-----+-----+
    """
    tableau =  [[0  , []],
                [1/2, [1/2]],
                [1/2, [ 0 ,1/2]],
                [1  , [ 0 , 0 , 1 ]],
                [1  , [1/6,1/3,1/3,1/6]]]
    return explicit_runge_kutta_method(tableau,**kwargs)


def runge_kutta38(**kwargs):
    """
    4-stage, 4th order, explicit.
    
    Notes
    -----
    Tableau:\n
    +-----+-----+-----+-----+-----+
    | 0   |                       |
    +-----+-----+-----+-----+-----+
    | 1/2 | 1/2 |                 |
    +-----+-----+-----+-----+-----+
    | 2/3 |-1/3 |  1  |           |
    +-----+-----+-----+-----+-----+
    | 1   |  1  | -1  |  1  |     |
    +-----+-----+-----+-----+-----+
    | 1   | 1/8 | 3/8 | 3/8 | 1/8 |
    +-----+-----+-----+-----+-----+
    """
    tableau =  [[0  , []],
                [1/3, [ 1/3]],
                [2/3, [-1/3,1 ]],
                [1  , [  1 ,-1.,1.0]],
                [1  , [ 1/8,3/8,3/8,1/8]]]
    return explicit_runge_kutta_method(tableau,**kwargs)


def explicit_runge_kutta_method(tableau,t=0,dt=0,inertia=None,residual=None,solution_dict_prev_projected={},constraint_vector=None,newtontol=1E-12,**kwargs):
    """
    Implementation of an explicit Runge Kutta method for an arbitrary explicit tableau.
    Various tableau's: ``RKEE``, ``RKEMP``, ``RK4`` and ``RK38``.

    Parameters
    ----------
    t : float
        Time at start of timestep
    dt : float
        Timestep
    inertia : integral
        The mass integral on the basis of the new solution
    residual : integral
        The stiffness integral on the basis of the new solution
    solution_dict_prev_projected : dict of array
        The solution vectors of the solutions of the last time step
        projected on the basis of the new solution.
    constraint_vector : dict of array
        The masks of Dirichlet constraints, as held by the ``DomainDefinition``.
    newtontol : float, optional
        Newton tolerance in case of implicit solves
        
    Returns
    -------
    dict of array
        The solution vectors for all solution fields
    """
    targets = tuple(solution_dict_prev_projected.keys())
    target_ddt = tuple([key+"ddt" for key in targets])
    constraint_vector_ddt = _get_cons_ddt(constraint_vector)
    
    replace_new_ddt = { key:function.Argument(key+'ddt',solution_dict_prev_projected[key].shape) for key in targets }
    inertia_ddt = inertia.replace(replace_new_ddt)
    transient_residual = inertia_ddt + residual # This now solves for dudt in M dudt + K u = 0
    residual_list = [ transient_residual.derivative(key[:-4]+"testdofs") for key in targets ]

    arguments_full = [  ]
    for dtfact,weights in tableau[:-1]:
        arguments = {'t':t+dtfact*dt}
        for key in targets:
            arguments[key] = solution_dict_prev_projected[key] + \
                dt*sum( [weight*arguments_full[i][key+"ddt"] for i,weight in enumerate(weights)] )
        sol_new_and_old = solver.newton(target_ddt, residual=residual_list, constrain=constraint_vector_ddt, arguments=arguments).solve(tol=newtontol)
        arguments_full.append( { keyddt:sol_new_and_old[keyddt] for keyddt in target_ddt } )
    
    sol_new = {}
    final_weights = tableau[-1][1]
    for key in targets:
        sol_new[key] = solution_dict_prev_projected[key] + dt*sum( [weight*arguments_full[i][key+"ddt"] for i,weight in enumerate(final_weights)] )
    return sol_new


def runge_kutta_implicit_euler(**kwargs):
    """
    1-stage, 1st order, implicit.
    Equivalent to implicit Euler.
    
    Notes
    -----
    Tableau:\n
    +-----+-----+
    | 1   |  1  |
    +-----+-----+
    | 1   |  1  |
    +-----+-----+
    """
    tableau =  [[ 1 , [ 1 ]],
                [ 1 , [ 1 ]]]
    return diagonal_implicit_runge_kutta_method(tableau,**kwargs)


def runge_kutta_crank_nicolson(**kwargs):
    """
    2-stage, 2nd order, implicit.
    Equivalent to Crank-Nicolson.
    
    Notes
    -----
    Tableau:\n
    +-----+-----+-----+
    | 0   |  0  |     |
    +-----+-----+-----+
    | 1   | 1/2 | 1/2 |
    +-----+-----+-----+
    | 1   | 1/2 | 1/2 |
    +-----+-----+-----+
    """
    tableau =  [[ 0 , [ 0 ]],
                [ 1 , [1/2,1/2]],
                [ 1 , [1/2,1/2]]]
    return diagonal_implicit_runge_kutta_method(tableau,**kwargs)


def runge_kutta_implicit_midpoint(**kwargs):
    """
    single-stage, 2nd order, diagonal implicit.
    
    Notes
    -----
    Tableau:\n
    +-----+-----+
    | 1/2 | 1/2 |
    +-----+-----+
    | 1   |  1  |
    +-----+-----+
    """
    tableau =  [[1/2, [1/2]],
                [ 1 , [ 1 ]]]
    return diagonal_implicit_runge_kutta_method(tableau,**kwargs)


def runge_kutta_crouzeix(**kwargs):
    """
    2-stage, 3rd order, diagonal implicit.
    
    Notes
    -----
    Tableau:\n
    wikipedia.org/wiki/List_of_Runge-Kutta_methods#Diagonally_Implicit_Runge-Kutta_methods
    """
    c1 = 1/2 + numpy.sqrt(3)/6
    c2 = 1/2 - numpy.sqrt(3)/6
    a11 = a22 = c1
    a21 = c2-a22
    tableau =  [[ c1 , [a11]],
                [ c2 , [a21,a22]],
                [ 1  , [1/2,1/2]]]
    return diagonal_implicit_runge_kutta_method(tableau,**kwargs)


def runge_kutta_norsett(**kwargs):
    """"
    3-stage, 4th order, diagonal implicit.
    
    Notes
    -----
    Tableau:\n
    wikipedia.org/wiki/List_of_Runge-Kutta_methods#Diagonally_Implicit_Runge-Kutta_methods
    """
    x = 1.06858
    w1 = w3 = 1/(6*(1-2*x)**2)
    w2 = 1-w1-w3
    tableau =  [[ x , [  x  ]],
                [1/2, [1/2-x,  x  ]],
                [1-x, [ 2*x ,1-4*x, x  ]],
                [ 1 , [  w1 ,  w2 , w3 ]]]
    return diagonal_implicit_runge_kutta_method(tableau,**kwargs)


def diagonal_implicit_runge_kutta_method(tableau, t=0,dt=0,inertia=None,residual=None,solution_dict_prev_projected={},constraint_vector=None,newtontol=1E-12,**kwargs):
    """
    Implementation of an implicit Runge Kutta method for an implicit tableau
    with a zero upper triangular block.
    Various tableau's: ``RKIE``, ``RKCN``, ``RKIM``, ``RKC`` and ``RKN``.

    Parameters
    ----------
    t : float
        Time at start of timestep
    dt : float
        Timestep
    inertia : integral
        The mass integral on the basis of the new solution
    residual : integral
        The stiffness integral on the basis of the new solution
    solution_dict_prev_projected : dict of array
        The solution vectors of the solutions of the last time step
        projected on the basis of the new solution.
    constraint_vector : dict of array
        The masks of Dirichlet constraints, as held by the ``DomainDefinition``.
    newtontol : float, optional
        Newton tolerance in case of implicit solves
        
    Returns
    -------
    dict of array
        The solution vectors for all solution fields
    """
    targets = tuple(solution_dict_prev_projected.keys())
    target_ddt = tuple([key+"ddt" for key in targets])
    constraint_vector_ddt = _get_cons_ddt(constraint_vector)
    
    replace_new_ddt = { key:function.Argument(key+'ddt',solution_dict_prev_projected[key].shape) for key in targets }
    inertia_ddt = inertia.replace(replace_new_ddt)
    residual_ddt = residual.replace(replace_new_ddt)

    arguments_full = [  ]
    for dtfact,weights in tableau[:-1]:
        arguments = {'t':t+dtfact*dt}
        for key in targets:
            arguments[key] = solution_dict_prev_projected[key] + \
                dt*sum( [weight*arguments_full[i][key+"ddt"] for i,weight in enumerate(weights[:-1])] )
        # NOTE: How to precompute this? -> requires adding a dt*weight argument
        transient_residual = inertia_ddt + residual + dt*weights[-1]*residual_ddt # This now solves for dudt in M dudt + K u + w*dt*K dudt = 0 with u = u^old+dt*dudt^prevs
        residual_list = [ transient_residual.derivative(key[:-4]+"testdofs") for key in targets ]
        sol_new_and_old = solver.newton(target_ddt, residual=residual_list, constrain=constraint_vector_ddt, arguments=arguments).solve(tol=newtontol)
        arguments_full.append( { keyddt:sol_new_and_old[keyddt] for keyddt in target_ddt } )
    
    sol_new = {}
    final_weights = tableau[-1][1]
    for key in targets:
        sol_new[key] = solution_dict_prev_projected[key] + dt*sum( [weight*arguments_full[i][key+"ddt"] for i,weight in enumerate(final_weights)] )
    return sol_new


def _get_cons_ddt(cons):
    """
    Replaces the constraints of the displacement by zero constraints of the velocity.
    NOTE: this introduces a mistake in case of non-stationary boundary conditions.
    """
    if cons is None:
        return cons
    cons_ddt = {}
    for key in cons:
        conskey = numpy.zeros(cons[key].shape)
        conskey[numpy.isnan(cons[key])] = numpy.nan
        cons_ddt[key+"ddt"] = conskey
    return cons_ddt
