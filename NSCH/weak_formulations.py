from __future__ import annotations
from nutils.expression_v2 import Namespace
from nutils import function
import numpy
import treelog
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from .solution_set import SolutionSet
    from .problem_definition import ProblemDefinition


def get_residual(problem_definition:ProblemDefinition,solution_set:SolutionSet,solution_set_prev:SolutionSet=None):
    """ 
    Get the residual integral of the problem defined in ``problem_definition``
    on the domain/basis/fields of ``solution_set``.

    Parameters
    ----------
    problem_definition : ProblemDefinition
        All the settings that define the appropriate residual integral.
    solution_set : SolutionSet
        Holds the domain/basis/fields on which the residual integral is 
        carried out.

    Returns
    -------
    integral
        Scalar value that defines the complete residual.
    """
    res_list = get_residual_list(problem_definition, solution_set_prev)
    return sum( [res(solution_set, solution_set_prev=solution_set_prev) for res in res_list]  )


def get_residual_list(problem_definition:ProblemDefinition, solution_set_prev:SolutionSet=None):
    """
    Get the list of functions that return the relevant segments
    of the full residual.

    Parameters
    ----------
    problem_definition : ProblemDefinition
        All the settings that define the appropriate residual integral.

    Returns
    -------
    list of function
        Is to be looped over and summed in ``get_residual`` to
        obtain the full residual.
    """
    # If manual problem, return the specified residual
    manual_residual = problem_definition.get("model","manual")
    if manual_residual is not None:
        return [ manual_residual[0] ]

    # Else, append residual components based on flags in problem_definition
    res_list = [get_residual_Stokes,get_residual_body_force]
    if not problem_definition.get("model",'Stokes_linearization'):
        res_list.append(get_residual_NS_advection)
        if problem_definition.get("model",'directional_do_nothing_boundaries') is not None:
            res_list.append(get_residual_directional_do_nothing)
    if problem_definition.get("model",'chemical_potential'):
        if problem_definition.get("model",'potential_split') \
                and not solution_set_prev is None:
            res_list.append(get_residual_CH_mixed_potential_split)
        else:
            res_list.append(get_residual_CH_mixed_potential)
        if problem_definition.get("model","Conservative_CH"):
            res_list.append(get_residual_CH_mixed_transport_conservative)
        else:
            res_list.append(get_residual_CH_mixed_transport)
    else:
        res_list.append(get_residual_CH_primal)
    if problem_definition.get("model","zero_mean_pressure"):
        res_list.append(get_residual_zero_mean_pressure)
    if problem_definition.get('stab','τLSIC') is not None:
        res_list.append(get_residual_LSIC)
    if problem_definition.get('stab','τpskel') is not None:
        res_list.append(get_residual_skeleton_p)
    if problem_definition.get("model","Navier_slip_boundaries") is not None:
        res_list.append(get_residual_Navier_slip)
    return res_list


def get_residual_bdy(problem_definition:ProblemDefinition,solution_set:SolutionSet):
    """ 
    Get the residual integral specific to the boundary terms 
    from the implicit integration-by-parts steps of the problem
    defined in ``problem_definition`` on the domain/basis/fields 
    of ``solution_set``.
    This is needed for a residual-based adaptive refinement scheme.

    Parameters
    ----------
    problem_definition : ProblemDefinition
        All the settings that define the appropriate residual integral.
    solution_set : SolutionSet
        Holds the domain/basis/fields on which the residual integral is 
        carried out.

    Returns
    -------
    integral
        Scalar value that defines the complete boundary residual.
    """
    res_list = get_residual_list_bdy(problem_definition)
    return sum( [res(solution_set) for res in res_list]  )


def get_residual_list_bdy(problem_definition:ProblemDefinition):
    """
    Get the list of functions that return the relevant segments
    of the full boundary residual (see ``get_residual_bdy``).

    Parameters
    ----------
    problem_definition : ProblemDefinition
        All the settings that define the appropriate residual integral.

    Returns
    -------
    list of function
        Is to be looped over and summed in ``get_residual_bdy`` to 
        obtain the full boundary residual.
    """
    manual_residual = problem_definition.get("model","manual")
    if manual_residual is not None:
        # Not supported
        return [ ]

    res_list = [get_residual_Stokes_bdy]
    if problem_definition.get("model","chemical_potential"):
        res_list.append(get_residual_CH_mixed_bdy)
    else:
        res_list.append(get_residual_CH_primal_bdy)
    return res_list


def get_inertia(problem_definition:ProblemDefinition,solution_set:SolutionSet):
    """ 
    Get the inertia integral of the problem defined in ``problem_definition``
    on the domain/basis/fields of ``solution_set``.

    Parameters
    ----------
    problem_definition : ProblemDefinition
        All the settings that define the appropriate residual integral.
    solution_set : SolutionSet
        Holds the domain/basis/fields on which the inertia integral is 
        carried out.

    Returns
    -------
    integral
        Scalar value that defines the complete inertia.
    """
    inertia_list = get_inertia_list(problem_definition)
    return sum( [inertia(solution_set) for inertia in inertia_list]  )


def get_inertia_list(problem_definition:ProblemDefinition):
    """
    Get the list of functions that return the relevant segments
    of the full interia.

    Parameters
    ----------
    problem_definition : ProblemDefinition
        All the settings that define the appropriate residual integral.

    Returns
    -------
    list of function
        Is to be looped over and summed in ``get_inertia`` to 
        obtain the full inertia integral.
    """
    # If manual problem, return the specified inertia
    manual_inertia = problem_definition.get("model","manual")
    if manual_inertia is not None:
        return [ manual_inertia[1] ]
    # Else, append interia components based on flags in problem_definition
    return [get_inertia_NS,get_inertia_CH]


## Below follow the residual (stiffness) definition segments for all flavors of models

def get_residual_NS_advection(solution_set:SolutionSet,solution_set_prev:SolutionSet=None):
    """ 
    The residual integral of the non-linear avective part of the Navier-Stokes equation.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    # Nonlinear advective contribution to the momentum residual
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    res =  domain.integral(intθ('0.5 ∇_j(u_i utest_i ρmix u_j) dV' @ ns), degree=get_degree('uuuφ'))  # Note: requires div u = 0
    res += domain.integral(intθ('- 0.5 ρmix u_j u_i ∇_j(utest_i) dV' @ ns ), degree=get_degree('uuuφ'))
    res += domain.integral(intθ('0.5 u_j ρmix utest_i ∇_j(u_i) dV' @ ns ), degree=get_degree('uuuφ'))
    res += domain.integral(intθ('0.5 u_i utest_i u_j ∇_j(ρmix) dV' @ ns ), degree=get_degree('uuuφ'))
    return res


def get_residual_directional_do_nothing(solution_set:SolutionSet,solution_set_prev:SolutionSet=None):
    """ 
    The residual integral of the non-linear avective part of the Navier-Stokes equation.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    # Nonlinear advective contribution to the momentum residual
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    boundaries = solution_set.problem_definition.get("model", "directional_do_nothing_boundaries")
    res = domain.boundary[boundaries].integral(intθ(' -0.5 ( u_i n_i - abs(u_i n_i) ) 0.5 ρmix u_j utest_j dV' @ ns), degree=get_degree('uuuφ'))  # Note: requires div u = 0
    return res


def get_residual_Stokes(solution_set:SolutionSet,solution_set_prev:SolutionSet=None):
    """ 
    The residual integral of the Stokes equation.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    # Residual for the Stokes part (with AGG term)
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    # Momentum:
    res = domain.integral(intθ('∇_j(utest_i) ρdiff m u_i ∇_j(μ) dV ' @ ns ), degree=get_degree('uuuφ')) # NOTE: ρdiff is defined -ρdiff from Abels,Garke,Grun
    res += domain.integral(intθ('- p ∇_i(utest_i) dV' @ ns ), degree=get_degree('pu'))
    res += domain.integral(intθ('∇_j(utest_i) τmix_ij dV' @ ns ), degree=get_degree('uuφ'))
    res += domain.integral(intθ('∇_j(utest_i) ζmix_ij dV' @ ns ), degree=get_degree('uφφφ'))
    # Incompressibility:
    res += domain.integral(intθ('ptest ∇_i(u_i) dV' @ ns ), degree=get_degree('pu'))
    return res


def get_residual_Stokes_bdy(solution_set:SolutionSet,solution_set_prev:SolutionSet=None):
    """ 
    The boundary residual integral of the Navier-Stokes equation. Required for 
    adaptive refinement.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    # Used for residual-based adaptive refinement
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    res =  domain.boundary["exterior"].integral(intθ('p utest_i n_i dS' @ ns ), degree=get_degree('pu'))
    res += domain.boundary["exterior"].integral(intθ('- utest_i τmix_ij n_j dS' @ ns ), degree=get_degree('uuφ'))
    res += domain.boundary["exterior"].integral(intθ('- utest_i ζmix_ij n_j dS' @ ns ), degree=get_degree('uφφφ'))
    return res


def get_residual_LSIC(solution_set:SolutionSet,solution_set_prev:SolutionSet=None):
    """ 
    The residual integral of LSIC stabilization.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    return domain.integral(intθ('τLSIC μmix ∇_i(utest_i) ∇_j(u_j) dV' @ ns ), degree=get_degree('uu'))


def get_residual_skeleton_p(solution_set:SolutionSet,solution_set_prev:SolutionSet=None):
    """ 
    The residual integral of skeleton stabilization on `p`.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    s = solution_set.field_data['p']['k']+1 # Order of the p Sobolev space
    if s == 0:
        return domain.interfaces.integral(intθ("τ_pskel hF [ p ] [ ptest ] dS" @ ns), degree=get_degree('pp'))       
    return domain.interfaces.integral(intθ(f"τpskel μmix^(-1) hF^{2*s+1} [ {partial_n('p',s)} ] [ {partial_n('ptest',s)} ] dS" @ ns), degree=get_degree('pp'))


def get_residual_body_force(solution_set:SolutionSet,solution_set_prev:SolutionSet=None):
    """ 
    The residual integral for the resultant body force, as encoded by
    the physical parameters Ff and Fg, through F=Ff+Fg*rho_mix.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    problem_definition = solution_set.problem_definition
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    if problem_definition.get("phys","Ff") is None and problem_definition.get("phys","Fg") is None:
        res = 0
    else:
        res = domain.integral(intθ('- utest_i F_i dV' @ ns), degree=get_degree('u+'))
    return res


def get_residual_zero_mean_pressure(solution_set:SolutionSet,solution_set_prev:SolutionSet=None):
    """ 
    The residual integral of the zero mean pressure (Lagrange multiplier) formulation.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    res = domain.integral(intθ('ptest lmp dV' @ ns), degree=get_degree('p'))
    res += domain.integral(intθ('lmptest p dV' @ ns), degree=get_degree('p'))
    return res
    
def get_residual_CH_mixed_transport(solution_set:SolutionSet,solution_set_prev:SolutionSet=None):
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    res = domain.integral(intθ('μtest ∇_i(φ) u_i dV' @ ns), degree=get_degree('φφu')) # Note: requires div u = 0
    res += domain.integral(intθ('∇_i(μtest) m ∇_i(μ) dV' @ ns), degree=get_degree('φμ'))
    return res

def get_residual_CH_mixed_transport_conservative(solution_set:SolutionSet,solution_set_prev:SolutionSet=None):
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    res =  domain.integral(intθ('- u_i φ ∇_i( μtest ) dV' @ ns), degree=get_degree('uφμ-'))
    res += domain.integral(intθ('∇_i(μtest) m ∇_i(μ) dV' @ ns), degree=get_degree('φμ'))
    res += domain.boundary.integral(intθ('u_i n_i φ μtest dS' @ ns),degree=get_degree('uφμ'))
    return res

def get_residual_CH_mixed_potential(solution_set:SolutionSet,solution_set_prev:SolutionSet=None):
    """ 
    The residual integral of the mixed Cahn-Hilliard equation.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    res = domain.integral(intθ('φtest μ dV' @ ns), degree=get_degree('μμ'))
    res += domain.integral(intθ(' - ∇_i(φtest) (σ ε) ∇_i(φ) dV' @ ns), degree=get_degree('φμ'))
    res += domain.integral(intθ('- φtest (σ / ε) dψ dV' @ ns), degree=get_degree('φφφμ'))
    return res

def get_residual_CH_mixed_potential_split(solution_set:SolutionSet,solution_set_prev:SolutionSet=None):
    """ 
    The residual integral of the mixed Cahn-Hilliard equation.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    domain = solution_set_prev.domain; ns = solution_set.ns.copy_()
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    ns.φprev = solution_set_prev.ns.φ.replace( { "φdofs":function.Argument("φdofsold",solution_set_prev.solution_dict["φdofs"].shape) } )
    ns.dψ = '- φ / 2 - φprev / 2 + 3 φ φprev^2 / 2 - φprev^3 / 2'
    res = domain.integral(intθ('φtest μ dV' @ ns), degree=get_degree('μμ'))
    res += domain.integral(intθ(' - ∇_i(φtest) (σ ε) ∇_i(φ) dV' @ ns), degree=get_degree('φμ'))
    res += domain.integral(intθ('- φtest (σ / ε) dψ dV' @ ns), degree=get_degree('φφφμ'))
    return res


def get_residual_CH_mixed_bdy(solution_set:SolutionSet):
    """ 
    The boundary residual integral of the mixed Cahn-Hilliard equation. Required for
    adaptive refinement.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.

    Returns
    -------
    integral
        Scalar integral.
    """
    # Used for residual-based adaptive refinement
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    res = domain.boundary["exterior"].integral(intθ('- μtest m ∇_i(μ) n_i dS' @ ns), degree=get_degree('φu'))
    res += domain.boundary["exterior"].integral(intθ(' φtest (σ ε) ∇_i(φ) n_i dS' @ ns), degree=get_degree('φμ'))
    return res


def get_residual_CH_primal(solution_set:SolutionSet,solution_set_prev:SolutionSet=None):
    """ 
    The residual integral of the primal Cahn-Hilliard equation.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    res = domain.integral(intθ('φtest ∇_i(φ) u_i dV' @ ns), degree=get_degree('φφu')) # Note: requires div u = 0
    res += domain.integral(intθ('(σ ε) ∇_j( ∇_j( φ ) ) ∇_i( m ∇_i( φtest ) ) dV' @ ns), degree=get_degree('φφ'))
    res += domain.integral(intθ('(σ / ε) ( 3 φ^2 - 1 ) ∇_i( φ ) m ∇_i( φtest ) dV' @ ns), degree=get_degree('φφφφ'))
    return res


def get_residual_CH_primal_bdy(solution_set:SolutionSet):
    """ 
    The boundary residual integral of the primal Cahn-Hilliard equation. Required for 
    adaptive refinement.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.

    Returns
    -------
    integral
        Scalar integral.
    """
    # Used for residual-based adaptive refinement
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    res = domain.boundary["exterior"].integral(intθ(' - m ∇_i( μ ) n_i φtest dS' @ ns), degree=get_degree('φφφ'))
    res += domain.boundary["exterior"].integral(intθ('- (σ12 ε) ∇_j( ∇_j( φ ) ) m ∇_i( φtest ) n_i dS' @ ns), degree=get_degree('φφ'))
    return res


def get_residual_Navier_slip(solution_set:SolutionSet,solution_set_prev:SolutionSet=None):
    """ 
    The residual integral of the Navier-slip condition.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    boundaries = solution_set.problem_definition.get("model", "Navier_slip_boundaries")
    res = domain.boundary[boundaries].integral(intθ('αNS utest_i u_i dS - utest_i ∇_i( φ ) dσSF dS' @ ns), degree=get_degree('uu uφ+'))
    if hasattr(ns, 'uwall'):
        res += domain.boundary[boundaries].integral(intθ('- αNS utest_i uwall_i dS' @ ns), degree=get_degree('uu+'))
    return res
    
## Below follow the inertia definition segments for all flavors of models

def get_inertia_NS(solution_set:SolutionSet):
    """ 
    The inertia integral of a usual Navier-Stokes equation: int rho u utest.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.

    Returns
    -------
    integral
        int rho u utest
    """
    # Mass for the first NSCH model: mixed form.
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    return domain.integral(intθ('ρmix u_i utest_i dV' @ ns), degree=get_degree('φuu'))


def get_inertia_CH(solution_set:SolutionSet):
    """ 
    The inertia integral of the Cahn-Hilliard equation: int φ μtest

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.

    Returns
    -------
    integral
        int φ μtest
    """
    # Mass for the NSCH model
    domain = solution_set.domain; ns = solution_set.ns
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    return domain.integral(intθ('φ μtest dV' @ ns), degree=get_degree('φφ'))


# Below follow additional helper functions

def compute_element_sizes(solution_set:SolutionSet):
    """ 
    A per-element `volume^(1/d)`.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.

    Returns
    -------
    nutils function
        `h` measure.
    """
    domain = solution_set.domain
    ns = solution_set.ns if not solution_set.domain_definition.axisymmetric \
        else _get_ns_for_geom(solution_set.domain_definition.geometry2D)
    volumes = domain.integrate_elementwise( '1 dV' @ ns , degree=1)
    return function.get( numpy.power(volumes,1/domain.ndims), 0, domain.f_index)


def compute_element_facet_sizes(solution_set:SolutionSet):
    """ 
    A per-facet `area^(1/(d-1))`.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.

    Returns
    -------
    nutils function
        `h_F` measure.
    """
    domain = solution_set.domain
    ns = solution_set.ns if not solution_set.domain_definition.axisymmetric \
        else _get_ns_for_geom(solution_set.domain_definition.geometry2D)
    domain.interfaces
    areas = domain.interfaces.integrate_elementwise( '1 dS' @ ns , degree=1)
    return function.get( numpy.power(areas,1/(domain.ndims-1)), 0, domain.interfaces.f_index)


def _get_ns_for_geom(geometry):
    """
    Creates a lighweight Namespace filled only with the specified
    geometry and derived quantities (jacobians, normal, gradient).
    Used to determine element sizes for axisymmetric domains.
    """
    ns = Namespace()
    ns.x = geometry
    ns.define_for('x', gradient='∇', normal='n', jacobians=('dV', 'dS'))
    return ns


def partial(field,indices):
    """
    partial^k field / partial (x_m^a x_n^b ..)  with a+b+.. = k
    Used for edge stabilization methods.

    Parameters
    ----------
    field : str
        fieldname on which to perform the operation.
    indices : str
        list of indices, its magnitude is `k`

    Returns
    -------
    str
        Expression of the above.
    """
    if len(indices) == 1: return f" ∇_{indices}( {field} ) "
    return f" ∇_{indices[-1]}({partial(field,indices[:-1])}) "


def partial_n(field,k):
    """
    partial^k field / partial (x_m^a x_n^b ..)  n_m n_n ...  with a+b+.. = k
    Used for edge stabilization methods.

    Parameters
    ----------
    field : str
        fieldname on which to perform the operation.
    k : int
        differential order.

    Returns
    -------
    str
        Expression of the above.
    """
    indices = "mnopqrstuvw"[:k] if "test" in field else "abcdefghijk"[:k]
    return partial(field,indices) + ' '.join([f'n_{index}' for index in indices])
