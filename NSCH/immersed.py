""" 
This file represents an example of how the NSCH core functionality can be extended with new modules.
The key feature is that all additional functionality can be collected in a single file, without the 
need for editing any of the core files. This is achieved by inheriting from the key classes.

The actual immersed functionality is still quite broken (although the example runs). Have a look at
the ToDo section in the README.md of the immersed branch for open actions.
"""
from .problem_definition import ProblemDefinition
from .domain_definitions import DomainDefinition
from .solution_set import SolutionSet
from .weak_formulations import partial_n
from nutils import topology, elementseq, transformseq, mesh, function
from nutils.expression_v2 import Namespace
from numpy import linspace
import numpy as np
import treelog


class ProblemDefinitionImmersed(ProblemDefinition):
    def __init__(self, default=False):
        """
        Additional parameters may be introduced by first calling the parent's initializer
        and then extending the relevant dictionary.
        """
        super().__init__(default=default)
        self._ProblemDefinition__model_parameters = { **self._ProblemDefinition__model_parameters, **{
            'Nitsche': 0, # 0: Penalty method, -1: Non-symmetric Nitsche, 1: Only viscous symmetric terms, 2: full symmetric, 3: ...
            'Conservative_NS_Ekin': False, # Whether to subtract (effectively 'weakly set equal to') the Ekin u*n term that appears from ibp of the skew symmetry term
        } }
        self._ProblemDefinition__stabilization_parameter = { **self._ProblemDefinition__stabilization_parameter, **{
            'βu': None, # Penalty factor for immersed no-flow enforcement
            'γu': None, # Ghost penalty on u
            'γp': None, # Ghost penalty on p
            'γφ': None, # Ghost penalty on φ
            'γμ': None, # Ghost penalty on μ
        } }

    def get_residual(self,solution_set,solution_set_prev=None):
        """
        By overwriting this method from the parent class, a module can introduce new
        components to the weak formulation. In practice one would add on top of the
        ``get_residual`` method from the parent. Other methods that can be overwritten
        to introduce more functionality are ``get_residual_bdy`` to fix residual-based
        refinement for your addition to the weak formulation, ``get_inertia`` in case
        you add new time-dependent fields, ``get_solution_fields_list`` if you add new
        fields and ``initialize_namespace_model_expressions`` (also done in this module).
        """
        return super().get_residual(solution_set,solution_set_prev=solution_set_prev) \
            + get_residual_immersed(self,solution_set)
    
    def initialize_namespace_model_expressions(self,solution_set):
        """
        By overwriting this method from the parent class, a module can introduce new
        expression to the ``Namespace``. In the current example, the background element
        size is added, as this is used for various penalty terms.
        """
        super().initialize_namespace_model_expressions(solution_set)
        _add_background_element_sizes_to_namespace(solution_set.ns,solution_set)


def _add_background_element_sizes_to_namespace(ns,solution_set):
    """
    Calculates the element and facet sizes of the background mesh and adds 
    these to the namespace.

    Parameters
    ----------
    ns : namespace
        namespace to which the element and facet sizes are added.
    solution_set: SolutionSet
        Contains the mesh of which the element and facet sizes are determined. 
    """
    background_domain = solution_set.domain_definition.untrimmed_domain
    physical_domain = solution_set.domain
    volumes = background_domain.integrate_elementwise( '1 dV' @ ns , degree=1)
    ns.hBG = function.get( np.power(volumes,1/physical_domain.ndims), 0, physical_domain.f_index)


class DomainDefinitionImmersed(DomainDefinition):
    def __init__(self,trimmed_domain,untrimmed_domain,geometry,trimnames, geometry2D=None,rot=None,intθ=lambda I: I,get_plot_domain=lambda I: I,plot_vec_axes=(0,1)):
        """
        Immersed computations require information from the untrimmed and trimmed domain,
        as well as additional domain information in terms of the ghost facets. Furthermore,
        a new class of boundary conditions introduced: weakly enforced Dirichlet.
        """
        super().__init__(trimmed_domain,geometry, geometry2D,rot,intθ,get_plot_domain,plot_vec_axes)
        self.untrimmed_domain = untrimmed_domain
        self.trimmed_bdy_names = trimnames
        self.ghost_faces = get_ghost_mesh(trimmed_domain,untrimmed_domain)
        self.Dirichlet_bcs_weak = []
    
    def add_Dirichlet_condition(self, *field_boundary_expression, time_dependent=False):
        """
        The `immersed` keyword is accepted as a special boundary case, for which weak 
        enforcement is instroduced. For all other cases, refer back to the parent 
        implementation of this method
        """
        fbe = field_boundary_expression
        for name,bdy,val in zip(fbe[::3],fbe[1::3],fbe[2::3]):
            if not bdy in self.trimmed_bdy_names:
                super().add_Dirichlet_condition(name,bdy,val,time_dependent=time_dependent)
            else:
                self.Dirichlet_bcs_weak += [ [name,bdy,val] ]

    def refine(self,refinement_indices=None):
        """
        This method needs to be overwritten to refine both the trimmed and untrimmed domains.
        This is required to correctly flag ghost facets on refined domains.
        """
        if refinement_indices is None:
            refined_untrimmed_domain = self.domain.refined
            refined_domain = self.domain.refined
        else:
            refined_untrimmed_domain = self.domain.refined.refined_by(refinement_indices)
            refined_domain = self.domain.refined_by(refinement_indices)
        return self.copy(refined_domain,refined_untrimmed_domain)
    
    def copy(self,domain=None,untrimmed_domain=None):
        """
        This method needs to be overwritten to ensure all new information is also copied over.
        """
        domain = self.domain if domain is None else domain
        untrimmed_domain = self.untrimmed_domain if untrimmed_domain is None else untrimmed_domain
        new_domain_definition = DomainDefinitionImmersed(domain,untrimmed_domain,self.geometry,self.trimmed_bdy_names,self.geometry2D,self.rot,self.intθ,self.get_plot_domain,self.plot_vec_axes)
        new_domain_definition.axisymmetric = self.axisymmetric
        new_domain_definition.Dirichlet_bcs = self.Dirichlet_bcs
        new_domain_definition.Neumann_bcs = self.Neumann_bcs
        new_domain_definition.Dirichlet_transient = self.Dirichlet_transient
        new_domain_definition.Neumann_transient = self.Neumann_transient
        new_domain_definition.plot_sample_degree = self.plot_sample_degree
        new_domain_definition.pvd_sample_degree = self.pvd_sample_degree
        new_domain_definition.Dirichlet_bcs_weak = self.Dirichlet_bcs_weak
        return new_domain_definition
    
    
def get_ghost_mesh(trimmed_domain, untrimmed_domain):
    """
    Obtain all facets for ghost-penalty.

    Parameters
    ----------
    trimmed_domain: Domain
        Nutils domain after trim operation.
    untrimmed_domain : Domain
        Nutils domain before trim operation.
    
    Returns
    -------
    topology.TransformChainsTopology
        The ghost facets.
    """


    # First determine all edges in the untrimmed mesh with partial overlap in the trimmed mesh
    baseitopo = untrimmed_domain.interfaces
    references = []
    for iface_transform, iface_reference, iface_opposite in zip(baseitopo.transforms, baseitopo.references, baseitopo.opposites):
        try:
            trimmed_domain.transforms.index_with_tail(iface_transform)
            trimmed_domain.transforms.index_with_tail(iface_opposite)
        except ValueError:
            references.append(iface_reference.empty)
        else:
            references.append(iface_reference)
    skeleton = topology.SubsetTopology(baseitopo, references)

    # For each of those edges, determine those that neighbor cut elements
    ghost_references = []
    ghost_transforms = []
    ghost_opposites  = []
    for iface_transform, iface_reference, iface_opposite in zip(skeleton.transforms, skeleton.references, skeleton.opposites):
        for trans in iface_transform, iface_opposite:
            ielemb,_ = untrimmed_domain.transforms.index_with_tail(trans)
            ielemt,_ = trimmed_domain.transforms.index_with_tail(trans)
            if untrimmed_domain.references[ielemb] != trimmed_domain.references[ielemt]:
                assert untrimmed_domain.transforms[ielemb] == trimmed_domain.transforms[ielemt]
                assert untrimmed_domain.opposites[ielemb]  == trimmed_domain.opposites[ielemt]
                ghost_references.append(iface_reference)
                ghost_transforms.append(iface_transform)
                ghost_opposites.append(iface_opposite)
                break
    
    # Create the ghost-facet topology from that information
    ghost_references = elementseq.References.from_iter(ghost_references, untrimmed_domain.ndims-1)
    ghost_transforms = transformseq.PlainTransforms(ghost_transforms, fromdims=untrimmed_domain.ndims-1, todims=untrimmed_domain.ndims)
    ghost_opposites  = transformseq.PlainTransforms(ghost_opposites, fromdims=untrimmed_domain.ndims-1, todims=untrimmed_domain.ndims)
    space, = trimmed_domain.spaces
    return topology.TransformChainsTopology(space, ghost_references, ghost_transforms, ghost_opposites)


def create_immersed_domain_2D(trimfuncs, W,H,nelems, trimnames="immersed", maxrefine=3,origin='corner',rotation=0,periodic=[]):
    """
    Create an immersed domain, as the positive part of the trimfunc on the rectangular domain
    defined by ``W``, ``H`` and the origin. ``Maxrefine`` refers to octree depth.

    Parameters
    ----------
    trimfunc: str or list of str
        Interpretable as a nutils function through trimfunc@Namespace. Trimmed domain becomes
        the positive part of this indicator function.
    W : float
        Width.
    H : float
        Height.
    nelems : int or list of int
        The elements in x and y. Examples: 5 or [5,10].
    trimnames: str or list of str, optional
        The names of the trimmed boundaries. Can be used for individual penalty-type bc inforcement.
    maxrefine: int, optional
        Depth of the octree algorithm for doing volume and boundary integration.
    origin : string, optional
        Either 'corner' or 'center' for the location of the origin. Corner
        refers to the lower left corner.
        Default: 'corner'
    rotation : float, optional
        Angle (in rad) by which the background mesh is rotated. Used for studies on immersed
        robustness.
    periodic : list of int 
        A list of dimension indices to be periodic

    Returns
    -------
    DomainDefinitionImmersed
        A ``DomainDefinitionImmersed`` object with the ``domain``, ``geometry``, etc.
    """
    if not type(trimfuncs) == list:
        trimfuncs = [trimfuncs]
    if not type(trimnames) == list:
        trimnames = [trimnames]
    if len(trimfuncs) != 1 and len(trimnames) == 1:
        trimnames *= len(trimfuncs)

    ns = Namespace()
    nelems = [nelems,nelems] if not type(nelems) == list else nelems
    offset = 0.5 if origin == 'center' else 0 if origin == 'corner' else None
    untrimmed_domain, geometry = mesh.rectilinear([linspace(-offset*W, (1-offset)*W, nelems[0]+1), 
                                                   linspace(-offset*H, (1-offset)*H, nelems[1]+1)],
                                                   periodic=periodic)

    if rotation != 0:
        R = np.mat( [[np.cos(rotation),-np.sin(rotation)],[np.sin(rotation),np.cos(rotation)]] )
        geometry = function.matmul(R,geometry)
    ns.x = geometry
    trimmed_domain = untrimmed_domain
    for trimfunc,trimname in zip(trimfuncs,trimnames):
        trimmed_domain = trimmed_domain.trim( trimfunc@ns , maxrefine=maxrefine, name=trimname)
    trimmed_domain = trimmed_domain.withboundary(exterior="left,top,right,bottom,immersed")
    return DomainDefinitionImmersed(trimmed_domain,untrimmed_domain,geometry,trimnames)


def get_residual_immersed(problem_definition,solution_set):
    """ 
    Get the residual integral of the problem defined in ``problem_definition``
    on the domain/basis/fields of ``solution_set``.

    Parameters
    ----------
    problem_definition : ProblemDefinition
        All the settings that define the appropriate residual integral.
    solution_set : SolutionSet
        Holds the domain/basis/fields on which the residual integral is 
        carried out.

    Returns
    -------
    integral
        Scalar value that defines the complete residual.
    """
    res_list = get_residual_list_immersed(problem_definition)
    return sum( [res(solution_set) for res in res_list]  )


def get_residual_list_immersed(problem_definition):
    """
    Get the list of functions that return the relevant segments
    of the full residual for the immersed extension of the framework.

    Parameters
    ----------
    problem_definition : ProblemDefinition
        Defines the appropriate residual integral.

    Returns
    -------
    list of function
        Is to be looped over and summed in ``get_residual`` to
        obtain the full residual.
    """
    residual_list = []
    if problem_definition.get("stab",'βu') is not None:
        residual_list.append(get_residual_penalty_u)
    if problem_definition.get("model",'Nitsche') == 1:
        residual_list.append(get_residual_Nitsche_consistency_NS)
        residual_list.append(get_residual_Nitsche_full_consistency_CH)
        residual_list.append(get_residual_Nitsche_symmetry_viscous_u)
        residual_list.append(get_residual_Nitsche_symmetry_p)
    if problem_definition.get("model",'Nitsche') == 2:
        residual_list.append(get_residual_Nitsche_consistency_NS)
        residual_list.append(get_residual_Nitsche_simplified_consistency_CH)
        residual_list.append(get_residual_Nitsche_symmetry_viscous_u)
        residual_list.append(get_residual_Nitsche_symmetry_p)
    if problem_definition.get("model",'Nitsche') == 3:
        residual_list.append(get_residual_Nitsche_consistency_NS)
        residual_list.append(get_residual_Nitsche_simplified_consistency_CH)
        residual_list.append(get_residual_Nitsche_symmetry_viscous_u)
        residual_list.append(get_residual_Nitsche_asymmetry_p)
    if problem_definition.get("stab",'γu') is not None:
        residual_list.append(get_residual_ghost_u)
    if problem_definition.get("stab",'γp') is not None:
        residual_list.append(get_residual_ghost_p)
    if problem_definition.get("stab",'γφ') is not None:
        residual_list.append(get_residual_ghost_φ)
    if problem_definition.get("stab",'γμ') is not None:
        residual_list.append(get_residual_ghost_μ)
    if problem_definition.get("model","Conservative_CH") is True:
        residual_list.append(get_residual_CH_conservative_immersed)
    if problem_definition.get("model","Conservative_NS_Ekin") is True:
        residual_list.append(get_residual_NS_conservative_Ekin)
    return residual_list


def get_residual_penalty_u(solution_set,solution_set_prev=None):
    """ 
    Penalty enforcement of the no-flow condition at immersed boundaries.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    # Used for residual-based adaptive refinement
    domain = solution_set.domain; ns = solution_set.ns; get_degree=solution_set.get_degree
    res = 0
    for name,bdy,val in solution_set.domain_definition.Dirichlet_bcs_weak:
        if not "u" in name: # Only penalty on u in this function
            continue
        if ((name[:2] == 'u_' and len(name)==3) or name == 'u') and (val == '0' or val == 0): # Homogeneous no-slip
            res += domain.boundary[bdy].integral(f' ( βu μmix / hBG ) u_i utest_i dS' @ ns, degree=get_degree('uu'))
        elif name[:2] == 'u_' and len(name)==3: # Non-homogeneous
            ind = name[-1]
            val_ind = val if ind in ['0','1','2'] else val+'_'+ind if not '_' in val else val # Scalar or vector
            res += domain.boundary[bdy].integral(f' ( βu μmix / hBG ) ( u_{ind} - {val_ind} ) utest_{ind} dS' @ ns, degree=get_degree('uu'))
        elif name[:2] == 'u': # Non-homogeneous vector
            res += domain.boundary[bdy].integral(f' ( βu μmix / hBG ) ( u_i - {val}_i ) utest_i dS' @ ns, degree=get_degree('uu'))
        elif (name[:2] == 'u_' and name[3:6] == ' n_' and len(name) == 7) and (val == '0' or val == 0): # Homogeneous normal component
            res += domain.boundary[bdy].integral(f' ( βu μmix / hBG ) u_i n_i utest_j n_j dS' @ ns, degree=get_degree('uu'))
        else:
            treelog.warning(f"Don't know what to do for penalty terms for bdy-condition {name} = {val} on {bdy}.")
    return res


def get_residual_Nitsche_consistency_NS(solution_set,solution_set_prev=None):
    """ 
    The residual integral of a consistency terms of the Nitsche formulation for the
    typical NSCH formulation. By itself this would be the non-symmetric Nitsche term.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    domain = solution_set.domain; ns = solution_set.ns; get_degree=solution_set.get_degree
    res = 0
    for name_,bdy,val in solution_set.domain_definition.Dirichlet_bcs_weak:
        if not "u" in name_: # Only Nitsche on u in this function
            continue
        name = name_ if "_" in name_ else name_+"_i"
        if name[:2] == 'u_' and len(name)==3:
            ind = name[-1]
            ind2 = 'j' if not ind=='j' else 'i'
            res += domain.boundary[bdy].integral(f'p utest_{ind} n_{ind} dS' @ ns, degree=get_degree('pu'))
            res += domain.boundary[bdy].integral(f'- utest_{ind} τmix_{ind}{ind2} n_{ind2} dS' @ ns, degree=get_degree('uuφ'))
        elif name[:2] == 'u_' and name[3:6] == ' n_' and len(name) == 7:
            res += domain.boundary[bdy].integral(f'p utest_i n_i dS' @ ns, degree=get_degree('pu'))
            res += domain.boundary[bdy].integral(f'- utest_i n_i n_j τmix_jk n_k dS' @ ns, degree=get_degree('uuφ'))
        else:
            treelog.warning(f"Don't know what to do for Nitsche terms for bdy-condition {name_} = {val} on {bdy}.")
    return res


def get_residual_Nitsche_full_consistency_CH(solution_set,solution_set_prev=None):
    """ 
    The residual integral of a consistency terms of the Nitsche formulation for the
    typical NSCH formulation. By itself this would be the non-symmetric Nitsche term.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    domain = solution_set.domain; ns = solution_set.ns; get_degree=solution_set.get_degree
    res = 0
    for name_,bdy,val in solution_set.domain_definition.Dirichlet_bcs_weak:
        if not "u" in name_: # Only Nitsche on u in this function
            continue
        name = name_ if "_" in name_ else name_+"_i"
        if name[:2] == 'u_' and len(name)==3:
            ind = name[-1]
            ind2 = 'j' if not ind=='j' else 'i'
            res += domain.boundary[bdy].integral(f'- utest_{ind} ζmix_{ind}{ind2} n_{ind2} dS' @ ns, degree=get_degree('uφφφ'))
        elif name[:2] == 'u_' and name[3:6] == ' n_' and len(name) == 7:
            res += domain.boundary[bdy].integral(f'- utest_i n_i n_j ζmix_jk n_k dS' @ ns, degree=get_degree('uφφφ'))
        else:
            treelog.warning(f"Don't know what to do for Nitsche terms for bdy-condition {name_} = {val} on {bdy}.")
    return res


def get_residual_Nitsche_simplified_consistency_CH(solution_set,solution_set_prev=None):
    """ 
    The residual integral of a consistency terms of the Nitsche formulation for the
    typical NSCH formulation. By itself this would be the non-symmetric Nitsche term.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    domain = solution_set.domain; ns = solution_set.ns; get_degree=solution_set.get_degree
    res = 0
    for name_,bdy,val in solution_set.domain_definition.Dirichlet_bcs_weak:
        if not "u" in name_: # Only Nitsche on u in this function
            continue
        name = name_ if "_" in name_ else name_+"_i"
        if name[:2] == 'u_' and name[3:6] == ' n_' and len(name) == 7:
            res += domain.boundary[bdy].integral(f'- utest_i n_i ( .5 ε σ ∇_k(φ) ∇_k(φ) + (σ / ε) ψ ) dS' @ ns, degree=get_degree('uφφφ'))
        else:
            treelog.warning(f"Don't know what to do for Nitsche terms for bdy-condition {name_} = {val} on {bdy}.")
    return res


def get_residual_Nitsche_symmetry_viscous_u(solution_set,solution_set_prev=None):
    """ 
    The viscous symmetric term of the Nitsche formulation.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    domain = solution_set.domain; ns = solution_set.ns; get_degree=solution_set.get_degree
    res = 0
    for name_,bdy,val in solution_set.domain_definition.Dirichlet_bcs_weak:
        if not "u" in name_: # Only Nitsche on u in this function
            continue
        name = name_ if "_" in name_ else name_+"_i"
        if ((name[:2] == 'u_' and len(name)==3) or name == 'u') and (val == '0' or val == 0):
            res += domain.boundary[bdy].integral(f'- u_i μmix ( ∇_j(utest_i) + ∇_i(utest_j) ) n_j dS' @ ns, degree=get_degree('uuφ'))
        elif name[:2] == 'u_' and len(name)==3:
            ind = name[-1]
            ind2 = 'j' if not ind=='j' else 'i'
            res += domain.boundary[bdy].integral(f'- ( u_{ind} - {val} ) μmix ( ∇_{ind2}(utest_{ind}) + ∇_{ind}(utest_{ind2}) ) n_{ind2} dS' @ ns, degree=get_degree('uuφ'))
        elif name[:2] == 'u_' and name[3:6] == ' n_' and len(name) == 7:
            res += domain.boundary[bdy].integral(f'- ( u_i n_i - {val} ) n_j μmix ( ∇_j(utest_k) + ∇_k(utest_j) ) n_k dS' @ ns, degree=get_degree('uuφ'))
        else:
            treelog.warning(f"Don't know what to do for Nitsche terms for bdy-condition {name_} = {val} on {bdy}.")
    return res


def get_residual_Nitsche_symmetry_p(solution_set,solution_set_prev=None):
    """ 
    The viscous symmetric term of the Nitsche formulation.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    domain = solution_set.domain; ns = solution_set.ns; get_degree=solution_set.get_degree
    res = 0
    for name_,bdy,val in solution_set.domain_definition.Dirichlet_bcs_weak:
        if not "u" in name_: # Only Nitsche on u in this function
            continue
        name = name_ if "_" in name_ else name_+"_i"
        if ((name[:2] == 'u_' and len(name)==3) or name == 'u') and (val == '0' or val == 0):
            res += domain.boundary[bdy].integral(f'ptest u_i n_i dS' @ ns, degree=get_degree('pu'))
        elif name[:2] == 'u_' and len(name)==3:
            ind = name[-1]
            ind2 = 'j' if not ind=='j' else 'i'
            res += domain.boundary[bdy].integral(f'ptest ( u_{ind} - {val} ) n_{ind} dS' @ ns, degree=get_degree('pu'))
        elif name[:2] == 'u_' and name[3:6] == ' n_' and len(name) == 7:
            res += domain.boundary[bdy].integral(f'ptest ( u_i n_i - {val} ) dS' @ ns, degree=get_degree('pu'))
        else:
            treelog.warning(f"Don't know what to do for Nitsche terms for bdy-condition {name_} = {val} on {bdy}.")
    return res


def get_residual_Nitsche_asymmetry_p(solution_set,solution_set_prev=None):
    """ 
    The viscous symmetric term of the Nitsche formulation.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    domain = solution_set.domain; ns = solution_set.ns; get_degree=solution_set.get_degree
    res = 0
    for name_,bdy,val in solution_set.domain_definition.Dirichlet_bcs_weak:
        if not "u" in name_: # Only Nitsche on u in this function
            continue
        name = name_ if "_" in name_ else name_+"_i"
        if ((name[:2] == 'u_' and len(name)==3) or name == 'u') and (val == '0' or val == 0):
            res += domain.boundary[bdy].integral(f'- ptest u_i n_i dS' @ ns, degree=get_degree('pu'))
        elif name[:2] == 'u_' and len(name)==3:
            ind = name[-1]
            ind2 = 'j' if not ind=='j' else 'i'
            res += domain.boundary[bdy].integral(f'- ptest ( u_{ind} - {val} ) n_{ind} dS' @ ns, degree=get_degree('pu'))
        elif name[:2] == 'u_' and name[3:6] == ' n_' and len(name) == 7:
            res += domain.boundary[bdy].integral(f'- ptest ( u_i n_i - {val} ) dS' @ ns, degree=get_degree('pu'))
        else:
            treelog.warning(f"Don't know what to do for Nitsche terms for bdy-condition {name_} = {val} on {bdy}.")
    return res


def get_residual_NS_conservative_Ekin(solution_set,solution_set_prev=None):
    """ 
    The viscous symmetric term of the Nitsche formulation.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    domain = solution_set.domain; ns = solution_set.ns; get_degree=solution_set.get_degree
    res = 0
    for name_,bdy,val in solution_set.domain_definition.Dirichlet_bcs_weak:
        if not "u" in name_: # Only Nitsche on u in this function
            continue
        name = name_ if "_" in name_ else name_+"_i"
        if ((name[:2] == 'u_' and len(name)==3) or name == 'u') and (val == '0' or val == 0):
            res += domain.boundary[bdy].integral(f'- ( 0.5 ρmix u_j utest_j ) u_i n_i dS' @ ns, degree=get_degree('uuuφ'))
        elif name[:2] == 'u_' and len(name)==3:
            ind = name[-1]
            ind2 = 'j' if not ind=='j' else 'i'
            res += domain.boundary[bdy].integral(f'- ( 0.5 ρmix u_j utest_j ) ( u_{ind} - {val} ) n_{ind} dS' @ ns, degree=get_degree('uuuφ'))
        elif name[:2] == 'u_' and name[3:6] == ' n_' and len(name) == 7:
            res += domain.boundary[bdy].integral(f'- ( 0.5 ρmix u_j utest_j ) ( u_i n_i - {val} ) dS' @ ns, degree=get_degree('uuuφ'))
        else:
            treelog.warning(f"Don't know what to do for Nitsche terms for bdy-condition {name_} = {val} on {bdy}.")
    return res


def get_residual_ghost_u(solution_set,solution_set_prev=None):
    """ 
    The residual integral of ghost penalty on the velocity field.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    ghost_faces = solution_set.domain_definition.ghost_faces; ns = solution_set.ns; get_degree=solution_set.get_degree
    P,k = solution_set.field_data['u']['P'], solution_set.field_data['u']['k']
    res = 0
    for s in range(k+1,P+1):
        res += ghost_faces.integral( f"γu μmix hBG^{2*s-1} [ {partial_n('u_i',s)} ] [ {partial_n('utest_i',s)} ] dS" @ ns, degree=get_degree('uu'+'--'*s))
    return res


def get_residual_ghost_p(solution_set,solution_set_prev=None):
    """ 
    The residual integral of ghost penalty on the pressure field.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    ghost_faces = solution_set.domain_definition.ghost_faces; ns = solution_set.ns; get_degree=solution_set.get_degree
    P,k = solution_set.field_data['p']['P'], solution_set.field_data['p']['k']
    res = 0
    for s in range(k+1,P+1):
        res += ghost_faces.integral( f"γp ( 1 / μmix ) hBG^{2*s+1} [ {partial_n('p',s)} ] [ {partial_n('ptest',s)} ] dS" @ ns, degree=get_degree('pp'+'--'*s))
    return res


def get_residual_ghost_φ(solution_set,solution_set_prev=None):
    """ 
    The residual integral of ghost penalty on the phasefield.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    ghost_faces = solution_set.domain_definition.ghost_faces; ns = solution_set.ns; get_degree=solution_set.get_degree
    P,k = solution_set.field_data['φ']['P'], solution_set.field_data['φ']['k']
    res = 0
    for s in range(k+1,P+1):
        res += ghost_faces.integral( f"- γφ σ ε hBG^{2*s-1} [ {partial_n('φ',s)} ] [ {partial_n('φtest',s)} ] dS" @ ns, degree=get_degree('φφ'+'--'*s))
    return res


def get_residual_ghost_μ(solution_set,solution_set_prev=None):
    """ 
    The residual integral of ghost penalty on the chemical potential field.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.
    solution_set_prev : SolutionSet
        From the solution at the previous time-step, for forcing explicit treatment
        of some terms. In particular for `potential splitting`.

    Returns
    -------
    integral
        Scalar integral.
    """
    ghost_faces = solution_set.domain_definition.ghost_faces; ns = solution_set.ns; get_degree=solution_set.get_degree
    P,k = solution_set.field_data['μ']['P'], solution_set.field_data['μ']['k']
    res = 0
    for s in range(k+1,P+1):
        res += ghost_faces.integral( f"γμ m hBG^{2*s-1} [ {partial_n('μ',s)} ] [ {partial_n('μtest',s)} ] dS" @ ns, degree=get_degree('μμ'+'--'*s))
    return res

def get_residual_CH_conservative_immersed(solution_set,solution_set_prev=None):
    """ 
    The residual integral to subtract the additional conservative terms on the
    the immersed boundary from the total integral over all boundaries.

    Parameters
    ----------
    solution_set : SolutionSet
        Holds the domain/basis/fields.

    Returns
    -------
    integral
        Scalar integral.
    """
    domain = solution_set.domain; ns = solution_set.ns; get_degree=solution_set.get_degree
    res = domain.boundary["immersed"].integral('- u_i n_i φ μtest dS'@ns,degree=get_degree('uφμ'))
    return res
