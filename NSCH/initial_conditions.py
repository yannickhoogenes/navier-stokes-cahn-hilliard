from __future__ import annotations
from nutils import function
from numpy import pi
import numpy
import treelog
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from .solution_set import SolutionSet

def ic_generator(funcs,fields):
    """
    Initial condition generator projects fields in ``fields`` to functions in 
    ``funcs``. These functions must either be stand-along expressions or else 
    they must be defined in the ``Namespace`` later on, which can be ensured 
    with the ``define_expression`` method of the ``ProblemDefinition`` instance.

    Parameters
    ----------
    funcs : list of str
        The function expressions
    fields : list of str
        The fields that are projected

    Returns
    -------
    function
        A function that takes a ``SolutionSet`` and returns the integrand that
        represents the L2-projection of the fields on the basis of the ``SolutionSet``.
    """
    def ic_projector(solution_set:SolutionSet):
        domain = solution_set.domain; ns = solution_set.ns.copy_()
        get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
        sqr = _get_zero_integral(solution_set,ns,exclude=fields)
        for func,field in zip(funcs,fields):
            if solution_set.field_data[field]['type'] == "scalar":
                sqr += domain.integral(intθ(f"( ({field} - {func}) {field}test ) dV" @ ns ),degree=get_degree(f'{field}{field}++')) 
            if solution_set.field_data[field]['type'] == "vector":
                sqr += domain.integral(intθ(f"( ({field}_i - {func}_i) {field}test_i ) dV" @ ns ),degree=get_degree(f'{field}{field}++')) 
        return sqr
    return ic_projector

def ic_generator_straight_interface(center=0.5,eps_fact=1,flip=False,u_0=0,u_1=0,u_2=0):
    """
    Initial condition generator that projects the phase-field of a straight
    (but potentially stretched) interface and a constant velocity field.

    Parameters
    ----------
    center : float, optional
        x-location of the interface.
    eps_fact : float, optional
        The factor by which the phase-field is stretched in the interface.
    flip : boolean, optional
        Multiply the phase-field by -1 if true, effectively reversing phases.
    u_0 : float, optional
        x-component of the velocity
    u_1 : float, optional
        y-component of the velocity
    u_2 : float, optional
        z-component of the velocity

    Returns
    -------
    function
        A function that takes a ``SolutionSet`` and returns the integrand that
        represents the L2-projection of the fields on the basis of the ``SolutionSet``.
    """
    return lambda sol_set, center=center,eps_fact=eps_fact,flip=flip,u_0=u_0,u_1=u_1,u_2=u_2: _curved_stretched_interface(sol_set, center,0,1,eps_fact,flip,u_0,u_1,u_2)


def ic_generator_curved_interface(amplitude=0.02,wavelength=1,center=0.5,flip=False,u_0=0,u_1=0,u_2=0):
    """
    Initial condition generator that projects the phase-field of a curved
    interface.

    Parameters
    ----------
    amplitude : float, optional
        The amplitude of the cosine-wave shaped curve
    center : float, optional
        x-location of the interface.
    flip : boolean, optional
        Multiply the phase-field by -1 if true, effectively reversing phases.

    Returns
    -------
    function
        A function that takes a ``SolutionSet`` and returns the integrand that
        represents the L2-projection of the fields on the basis of the ``SolutionSet``.
    """
    return lambda sol_set, center=center,amplitude=amplitude,flip=flip: _curved_stretched_interface(sol_set, center,amplitude,wavelength,1,flip,u_0,u_1,u_2)


def _curved_stretched_interface(solution_set:SolutionSet, center,amplitude,wavelength,eps_fact,flip,u_0,u_1,u_2):
    """
    Front facing implementations:
    ic_generator_straight_interface
    ic_generator_curved_interface
    """
    domain = solution_set.domain; ns = solution_set.ns.copy_()
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    flip_func = -2 * flip + 1
  
    # Set up variables
    ns.xx     = 'x_0'
    ns.yy     = 'x_1'
    ns.xinter = center + amplitude*numpy.cos(2*pi/wavelength*ns.yy)
    ns.φinit = flip_func * numpy.tanh( (ns.xx-ns.xinter) / (ns.ε* eps_fact * numpy.sqrt(2)) ) 
    ns.uinit = numpy.array([u_0,u_1]) if (solution_set.domain.ndims == 2 and solution_set.domain_definition.axisymmetric == False) else numpy.array([u_0,u_1,u_2])

    # Specify least-squares integral
    sqr = domain.integral(intθ("( (u_i - uinit_i) utest_i + (φ - φinit) φtest  ) dV" @ ns ),degree=get_degree('uu φφ++')) \
        + _get_zero_integral(solution_set,ns,exclude=['u','φ'])
    return sqr


def ic_generator_infinite_filament(R=0.5,A=0.1,L=0.3):
    """
    Initial condition generator that projects the phase-field of an infinite
    filament with a small perturbation on an axisymmetric domain.

    Parameters
    ----------
    R : float, optional
        Radius of the filament.
    A : float, optional
        Amplitude of the perturbation.
    L : float, optional
        Wave length of the perturbation.

    Returns
    -------
    function
        A function that takes a ``SolutionSet`` and returns the integrand that
        represents the L2-projection of the fields on the basis of the ``SolutionSet``.
    """
    return lambda sol_set, R=R, A=A, L=L: _infinite_filament(sol_set, R,A,L)


def _infinite_filament(solution_set:SolutionSet, R0,A0,h0):
    """
    Front facing implementations:
    ic_generator_infinite_filament
    """
    domain = solution_set.domain; ns = solution_set.ns.copy_()
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ

    # Set up variables
    ns.r     = ns.x[0]
    ns.z     = ns.x[1]
    ns.Rφ0   = R0 + A0*numpy.cos( ns.z / h0 * pi ) # Radius where φ=0
    ns.φinit = numpy.tanh( (ns.Rφ0 - ns.r) / (ns.ε * numpy.sqrt(2)) )

    # Specify least-squares integral
    sqr = domain.integral(intθ(" (φ - φinit) φtest dV " @ ns ), degree=get_degree('φφ++')) \
        + _get_zero_integral(solution_set,ns,exclude=['φ'])
    return sqr


def ic_generator_perturbed_droplet(x_axis=0.4,y_axis=0.3):
    """
    Initial condition generator that provides the L2-optimal weak form for a
    first-order perturbed droplet.

    Parameters
    ----------
    x_axis : float, optional
        Half of the major/minor axis along the x-axis
    y_axis : float, optional
        Half of the major/minor axis along the y-axis

    Returns
    -------
    function
        A function that takes a ``SolutionSet`` and returns the integrand that
        represents the L2-projection of the fields on the basis of the ``SolutionSet``.
    """
    return lambda sol_set, a=x_axis,b=y_axis: _perturbed_droplet(sol_set,a,b)


def _perturbed_droplet(solution_set:SolutionSet, a,b):
    """
    Front facing implementations:
    ic_generator_perturbed_droplet
    """
    domain = solution_set.domain; ns = solution_set.ns.copy_()
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ

    ns.distance = _ellipse_distance(ns.x,a,b)
    ns.φinit = numpy.tanh( ns.distance / (ns.ε * numpy.sqrt(2)) )
    sqr = domain.integral(intθ("( (φ - φinit) φtest ) dV" @ ns ),degree=get_degree('φφ++')) \
        + _get_zero_integral(solution_set,ns,exclude=['φ'])
    return sqr


def ic_generator_two_droplets(R0=0.2,R1=0.2,Δcenter=0.5):
    """
    Initial condition generator that projects the phase-field of two
    neighboring bubbles.

    Parameters
    ----------
    R0 : float, optional
        Radius of first droplet.
    R1 : float, optional
        Radius of second droplet.
    Δcenter : float, optional
        Distance between both droplets, as half-spacing from the origin
        in x-direction.

    Returns
    -------
    function
        A function that takes a ``SolutionSet`` and returns the integrand that
        represents the L2-projection of the fields on the basis of the ``SolutionSet``.
    """
    # Meant to be used on a 2D box/rectangular domain in which the droplets fit
    # Two droplets are created with Radius R0 with center at x = ±Δcenter and y=0
    return lambda sol_set, R0=R0,R1=R1,Δcenter=Δcenter: _two_droplets(sol_set, R0,R1,Δcenter)


def _two_droplets(solution_set:SolutionSet, R0,R1,Δcenter):
    """
    Front facing implementations:
    ic_generator_two_droplets
    """
    domain = solution_set.domain; ns = solution_set.ns.copy_()
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
  
    # Set up variables
    ns.xx = 'x_0'
    ns.yy = 'x_1'
    ns.r0 = R0 - numpy.sqrt( function.power(ns.xx+Δcenter/2,2) + function.power(ns.yy,2))
    ns.r1 = R1 - numpy.sqrt( function.power(ns.xx-Δcenter/2,2) + function.power(ns.yy,2))
    ns.φinit = function.max(numpy.tanh( ns.r0 / (ns.ε * numpy.sqrt(2)) ), numpy.tanh( ns.r1 / (ns.ε * numpy.sqrt(2)) ) )
    
    # Specify least-squares integral
    sqr = domain.integral(intθ("( (φ - φinit) φtest ) dV" @ ns ),degree=get_degree('φφ++')) \
        + _get_zero_integral(solution_set,ns,exclude=['φ'])
    return sqr


def _get_zero_integral(solution_set:SolutionSet,ns,exclude=[]):
    """
    Returns the integrals of the homogeneous L2 projections for all fields in the 
    solution set that are not in the exclude list. Used to more flexibly define 
    intial conditions for different models that involve different solution fields.

    Parameters
    ----------
    solution_set : SolutionSet
        The solution set that defines the fields and domains
        with which / on which to carry out the integrals.
    ns : Namespace
        May be different from solution_set.ns due to added 
        expressions in the other IC L2-integral terms.
    excludes : list of str
        List of fields that are already specified in the other
        L2-integral terms.
    
    Returns
    -------
    integral
        That projects each remaining field to zero
    """
    domain = solution_set.domain
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
    sqr = 0
    for name,field_type,_,_ in solution_set.solution_fields:
        if not 'test' in name and not name in exclude:
            if field_type == "scalar":
                sqr += domain.integral(intθ(f"( {name} {name}test ) dV"  @ ns ), degree=get_degree(f'{name}{name}'))
            elif field_type == "vector":
                sqr += domain.integral(intθ(f"( {name}_i {name}test_i ) dV"  @ ns ), degree=get_degree(f'{name}{name}'))
            elif field_type == "R":
                sqr += domain.integral(intθ(f"( {name} {name}test ) dV"  @ ns ), degree=1)
    return sqr


def _ellipse_distance(point,a,b):
    """
    Distance from ``point`` to an ellipse with axes ``a`` and ``b``
    in `x` and `y`. Used in the perturbed droplet.
    """
    # Work in quadrant 1.
    px = abs(point[0])
    #  px = function.abs
    py = abs(point[1])

    # Initial guess for parameter t corresponding to shortest point on ellipse.
    t = pi / 4

    # Approximation loop; total number of loops should be dependent on rate of reduction of error.
    for i in range(0, 20):

        # Parametrization of ellipse in point t.
        x = a * numpy.cos(t)
        y = b * numpy.sin(t)

        # Parametrization of evolute in point t.
        ex = (a*a - b*b) * numpy.cos(t)**3 / a
        ey = (b*b - a*a) * numpy.sin(t)**3 / b

        # Distance of coordinates of ellipse and evolute for t.
        rx = x - ex
        ry = y - ey

        # Distance of coordinates between point and evolute for t.
        qx = px - ex
        qy = py - ey

        # Distance between ellipse and evolute for t.
        r = numpy.sqrt(rx**2 + ry**2)

        # Distance between point and evolute for t.
        q = numpy.sqrt(qx**2 + qy**2)

        # Approximate distance between current guess a and new guess a'.
        delta_c = r * numpy.arcsin((rx*qy - ry*qx)/(r*q))

        # Approximate distance in t coordinate.
        delta_t = delta_c / numpy.sqrt(a*a + b*b - x*x - y*y)

        # Add increment delta_t to t.
        t += delta_t

        # Enforce t between 0 and pi/2.
        t = function.min(pi/2, function.max(0, t))

        # Recompute parametrization of ellipse for the new t.
        x = a * numpy.cos(t)
        y = b * numpy.sin(t)

        # Reintroduce different quadrants.
        xy = [function.abs(x)*function.sign(point[0]), function.abs(y)*function.sign(point[1])]

        # Compute distance between point and ellipse.
        distance = function.piecewise( px, [a], \
            function.sign(py - b * numpy.sqrt(1-(px/a)**2)) * numpy.sqrt((point[0] - xy[0])**2 + (point[1] - xy[1])**2),\
            numpy.sqrt((point[0] - xy[0])**2 + (point[1] - xy[1])**2) )

    return distance
    
def _single_droplet(solution_set:SolutionSet, R, center):
    """
    Front facing implementation:
    ic_generator_single_droplet
    """
    domain = solution_set.domain; ns = solution_set.ns.copy_()
    get_degree=solution_set.get_degree; intθ = solution_set.domain_definition.intθ
  
    # Set up variables
    ns.xx = 'x_0'
    ns.yy = 'x_1'
    ns.r = R - numpy.sqrt((ns.xx - center[0])**2 + (ns.yy - center[1])**2)
    ns.φinit = -numpy.tanh( ns.r / (ns.ε * numpy.sqrt(2)) )
    
    # Specify least-squares integral
    sqr = domain.integral(intθ("( (φ - φinit) φtest ) dV" @ ns ),degree=get_degree('φφ++'))\
        + _get_zero_integral(solution_set,ns,exclude=['φ'])
    return sqr

def ic_generator_single_droplet(R=0.2, center=[0, 0]):
    """
    Initial condition generator that projects the phase-field of a single
    droplet or bubble.

    Parameters
    ----------
    R : float, optional
        Radius of the droplet or bubble.
    center : list, optional
        The center of the droplet or bubble in the form [x_c, y_c].

    Returns
    -------
    function
        A function that takes a ``SolutionSet`` and returns the integrand that
        represents the L2-projection of the fields on the basis of the ``SolutionSet``.
    """
    return lambda sol_set, R=R, center=center: _single_droplet(sol_set, R, center)