from __future__ import annotations
from nutils import function
from nutils.expression_v2 import Namespace
import treelog
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from .problem_definition import ProblemDefinition
    from .domain_definitions import DomainDefinition

class SolutionSet:
    """
    Data-holder for the bases, test and trial functions relevant for the current 
    refinement level of the current time-step. Largely a wrapper around the 
    Namespace concept from nutils, with knowledge of the domain and functionality 
    that is required for adaptivity etc.

    Parameters
    ----------
    problem_definition : ProblemDefinition
        The data-holder for all simulation parameters and settings.
    domain_definition : DomainDefinition
        The data-holder for the domain, geometry and boundary conditions.
    t : float, optional
        The current time.
        Default=0.
    refinement_level : int, optional
        How often the domain and bases of this ``SolutionSet`` are refined
        compared to the base mesh.
        Default=0.
    fill_ns : bool, optional
        Whether to initialize all parameters/expressions in the namespace.
        During refinements, this is set to ``False`` and they are copied over.
        Default=True.
    
    Attributes
    ----------
    problem_definition : ProblemDefinition
    domain_definition : DomainDefinition
    domain : domain
    t : float
    refinement_level : int
        Includes the physical parameters and solution fields
    solution_dict : dict of array
        Solution vectors for the different fields in ``solution_fields``.
    ns : namespace
    bases : dict
        map of (P,k,field_type) -> basis
    solution_fields : list of (str,str,int,int)
        [ (field,type,P,k) , ... ,  (fieldtest,type,P,k) , ... ], where 
        the types are either the string "vector", "scalar" or "R", P is the
        polynomial order of the basis functions and k the order of continuity.
    """
    def __init__(self, problem_definition:ProblemDefinition, domain_definition:DomainDefinition, t=0, refinement_level=0, fill_ns = True): 
        self.problem_definition:ProblemDefinition = problem_definition
        self.domain_definition:DomainDefinition = domain_definition # Includes initial domain & geometry, and boundary condition data
        self.domain = domain_definition.domain
        self.t = t
        self.refinement_level = refinement_level
        self.solution_dict = {}
        self.__field_data = None
        self.solution_fields = problem_definition.get_solution_fields_list()
        self.initialize_namespace() # Sets up self.ns with x, gradient, normal, dV, dS
        self.initialize_bases()     # Sets up the bases for the fields in self.ns
        self.initialize_fields()    # Initializes the fields in self.ns
        if fill_ns:
            problem_definition.initialize_namespace_model_expressions(self)

    @property
    def field_data(self):
        """
        dict of dicts of {'P'->int,'k'->int,'type'->str}
            Handy dictionary of all field-information.

        >>> self.field_data[fieldname]['P'] # Polynomial order of the field
        >>> self.field_data[fieldname]['k'] # Continuity order of the field
        >>> self.field_data[fieldname]['type'] # Type of the field (scalar, vector, real)
        """
        if self.__field_data is None:
            self.__field_data = { field:{'P':P,'k':k,'type':t} for field,t,P,k in self.solution_fields}
        return self.__field_data
        
    def initialize_namespace(self):
        """
        Defines ``self.ns`` and specifies the spatial geometry and time.
        """
        self.ns:Namespace = Namespace()
        self.ns.x = self.domain_definition.geometry
        self.ns.t = self.t
        self.ns.define_for('x', gradient='∇', normal='n', jacobians=('dV', 'dS'))

    def initialize_bases(self):
        """
        Defines ``self.bases`` as the dict {(P,k,field_type) -> basis, ...}.
        """
        self.bases = {}
        for _,field_type,P,k in self.solution_fields:
            if not (P,k,field_type) in self.bases:
                if field_type == "scalar":
                    basis = self.domain.basis('th-spline', degree=P, continuity=k) if not(k == -1) else \
                            self.domain.basis('discont', degree=P)
                elif field_type == "vector":
                    ndims = self.domain.ndims
                    basis = self.domain.basis('th-spline', degree=P, continuity=k).vector(ndims)
                elif field_type == "R":
                    basis = None
                self.bases[ (P,k,field_type) ] = basis

    def initialize_fields(self):
        """
        Adds all solution fields to ``self.ns``.
        """
        for name,field_type,P,k in self.solution_fields:
            basis = self.bases[(P,k,field_type)]
            # setattr(self.ns, name, function.dotarg(name+'dofs',basis)) 
            if field_type == 'R':
                setattr(self.ns, name, function.Argument( name+'dofs',() ) )
            elif field_type == 'scalar':
                setattr(self.ns, name, function.dotarg(name+'dofs',basis))
            elif field_type == 'vector':
                setattr(self.ns, name, function.dotarg(name+'dofs',basis) @ self.domain_definition.rot ) 
    
    def get_fieldnames(self):
        """
        Returns
        -------
        list of str
            List of solutionfield names, i.e., [u, p, ...]
        """
        return [name for name,_,_,_ in self.solution_fields if not 'test' in name]
    
    def get_doflist(self):
        """
        Returns
        -------
        list of str
            List of names of solutionfield-dofs (name + 'dofs'). Includes those
            for trial and for test functions. I.e., [udofs, utestdofs, pdofs, ...]
        """
        return [name+'dofs' for name,_,_,_ in self.solution_fields]
    
    def get_doflist_trial(self):
        """
        Returns
        -------
        list of str
            List of names of solutionfield-dofs (name + 'dofs') for the 
            trial functions. I.e., [udofs, pdofs, ...]
        """
        return [name+'dofs' for name,_,_,_ in self.solution_fields if not 'test' in name]
    
    def get_doflist_test(self):
        """
        Returns
        -------
        list of str
            List of names of solutionfield-dofs (name + 'dofs') for the 
            test functions. I.e., [utestdofs, ptestdofs, ...]
        """
        return [name+'dofs' for name,_,_,_ in self.solution_fields if 'test' in name]
    
    def get_degree(self,degree_string):
        """
        Get the polynomial degree of an expression used for subsequent integration.
        Simply returns the `problem_definition`'s "FE" "degree", if this value is set.
        Else, takes the maximum of substrings between spaces.

        Parameters
        ----------
        degree_string : str
            Solution fields and their multiplicities.

        Returns
        -------
        int
            Polynomial degree based on the orders of the different fields.

        Examples
        --------
        >>> self.get_degree("up-") # for int div(v)*p
        >>> self.get_degree("φφ φ+++") # The largest between degree of φ^2 and degree of φ+3
        """
        if self.problem_definition.get("FE","degree") is not None:
            return self.problem_definition.get("FE","degree")
        degree = 0
        for substring in degree_string.split(' '):
            degree = max(degree,self._get_degree_substring(substring))
        if self.domain_definition.axisymmetric: # Constant solution in theta
            degree = (degree,degree,0)
        return degree
    
    def _get_degree_substring(self,substring):
        """
        Finds the degree of a char/small string. 
        '+' gives 1
        '-' gives -1
        'fieldname' gives the degree of that solution field
        """
        degree = 0
        for char in substring:
            if char=="+": degree += 1
            elif char=="-": degree -= 1
            else: degree += self.field_data[char]['P']
        return max(0,degree)
    
    def merge(self,solution_set:SolutionSet, t=None):
        """
        Merges the current ``SolutionSet`` with the given solution set, where the 
        resulting merged set includes the domain and testfunctions of the current 
        set, but the trial functions from the given set.
        
        Parameters
        ----------
        solution_set : SolutionSet
            The set to be merged with

        Returns
        -------
        SolutionSet
            With the domain and test functions of ``self``, but with the trial functions
            of the input ``solution_set`` and with the merged ``domain``.
        """
        t = self.t if t is None else t
        merged_solution_set = SolutionSet(self.problem_definition,self.domain_definition,t,fill_ns = False)
        for fieldname in merged_solution_set.get_doflist_trial():
            new_trial = getattr( solution_set.ns, fieldname[:-4] )
            setattr( merged_solution_set.ns , fieldname[:-4], new_trial)
        merged_solution_set.domain = self.merge_domain(solution_set)
        self.problem_definition.initialize_namespace_model_expressions(merged_solution_set)
        merged_solution_set.solution_dict = solution_set.solution_dict
        return merged_solution_set

    def merge_domain(self,solution_set:SolutionSet):
        """
        Merges the ``domain`` of the current ``SolutionSet``
        with the given solution set

        Parameters
        ----------
        solution_set : SolutionSet
            The set which contains the integration domain with which
            the current integration domain needs to be merged

        Returns
        -------
        domain
            On this integration domain it is possible to integrate
            functions and solutions from both solution sets correctly
        """
        return self.domain & solution_set.domain

    def refine(self,refinement_indices=None):
        """
        Creates a new ``SolutionSet`` where the ``domain_definition`` of self 
        is refined according to the passed indices.
        
        Parameters
        ----------
        refinement_indices : Array?, optional
            The elements to refine from the ``domain`` in 
            ``self.domain_definition``. If ``None`` then homogeneous refinement.

        Returns
        -------
        SolutionSet
        """
        refined_domain_definition = self.domain_definition.refine(refinement_indices)
        return SolutionSet(self.problem_definition,refined_domain_definition,self.t,self.refinement_level+1)
    
    def copy(self,t=None,fill_ns=False):
        """
        Creates a new ``SolutionSet`` that is the copy of the current, with an empty 
        ``solution_dict``.
        
        Parameters
        ----------
        t : float, optional
            The time for the new ``SolutionSet``, defaults to the time of self.

        Returns
        -------
        SolutionSet
        """
        t = self.t if t is None else t
        copy = SolutionSet(self.problem_definition,self.domain_definition,t,fill_ns=fill_ns)
        if not fill_ns:
            copy.ns = self.ns.copy_()
        copy.ns.t = t
        return copy