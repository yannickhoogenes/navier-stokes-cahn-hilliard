from __future__ import annotations
from nutils import function
from .weak_formulations import compute_element_sizes, compute_element_facet_sizes
import treelog, numpy
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from nutils.expression_v2 import Namespace
    from .solution_set import SolutionSet
    from .problem_definition import ProblemDefinition

    
def get_solution_fields_list(problem_definition:ProblemDefinition):
    """
    Generate the list of required solution fields and their types for this 
    particular flavor of NSCH-model.

    Parameters
    ----------
    problem_definition : ProblemDefinition
        The data-holder for all simulation parameters and settings.

    Returns
    -------
    list of (str,str,int,int)
        [ (field,type,P,k) , ... ,  (fieldtest,type,P,k) , ... ], where 
        the types are either the string "vector", "scalar" or "R", P is the
        polynomial order of the basis functions and k the order of continuity.
    """
    # If manual problem, return the specified solution field list
    manual_problem = problem_definition.get("model","manual")
    if manual_problem is not None:
        return manual_problem[2]

    # Basic variables
    states = [ "u"     , "p"     , "φ"     ]
    types  = [ "vector", "scalar", "scalar"]

    # Add μ for mixed Cahn-Hilliard
    if problem_definition.get("model","chemical_potential"):
        states.append( "μ" )
        types.append("scalar")

    # Add lmp for Lagrange multiplier on pressure
    if problem_definition.get("model","zero_mean_pressure"):
        states.append( "lmp" )
        types.append("R")

    # Collect polynomial orders and continuity orders for all fields
    Ps = [problem_definition.get('FE',"P"+state) for state in states]
    ks = [problem_definition.get('FE',"k"+state) for state in states]
    kdefault = problem_definition.get('FE',"k")
    ks = [kdefault if k is None else k for k in ks]

    return [ (s,t,P,k)        for s,t,P,k in zip(states,types,Ps,ks) ] + \
           [ (s+"test",t,P,k) for s,t,P,k in zip(states,types,Ps,ks) ] 


def initialize_namespace_model_expressions(problem_definition:ProblemDefinition,solution_set:SolutionSet):
    """
    Fills the Namespace in the SolutionSet with all relevant physical, 
    model and stabilization parameters.
    
    Parameters
    ----------
    problem_definition : ProblemDefinition
        The data-holder for all simulation parameters and settings.
    solutions_set : SolutionSet
        The data-holder for domain, geometry, bases, solution dictionary,
        and Namespace specific for one refinement level of one time-step.
    """
    ns = solution_set.ns


    # Add all physical and stabilization parameters to the namespace
    _add_parameters_to_namespace(ns,problem_definition,'phys')
    _add_parameters_to_namespace(ns,problem_definition,'stab')
    _add_element_sizes_to_namespace(ns,solution_set)

    # Stop here if the user specified a manual problem formulation
    if problem_definition.get("model","manual") is not None:
        _add_user_defined_expressions(ns,problem_definition)
        return

    # Add NS/CH model expressions
    _add_model_dependent_fields_to_namespace(ns,problem_definition)
    _add_mixed_densities_to_namespace(ns,problem_definition)
    _add_mixed_viscosities_to_namespace(ns,problem_definition)
    _add_stress_tensor_to_namespace(ns,problem_definition)
    _add_phasefield_potential_to_namespace(ns,problem_definition)
    _add_surface_tension_to_namespace(ns,problem_definition)
    _add_capillary_stress_tensor_to_namespace(ns,problem_definition)
    _add_body_force_to_namespace(ns,problem_definition)

    # Add user-defined expressions to namespace
    _add_user_defined_expressions(ns,problem_definition)


def _add_user_defined_expressions(ns:Namespace,problem_definition:ProblemDefinition):
    """Takes the ``user_defined_expressions`` from the ``problem_definition``
    and adds them to the ``Namespace``."""
    for name,expression,args in problem_definition.user_defined_expressions:
        if args is not None: # expression takes arguments
            args = [ getattr(ns,arg) if type(arg)==str else arg for arg in args ]
            setattr(ns, name, expression(*args) )
        else:
            setattr(ns, name, expression )


def _add_parameters_to_namespace(ns:Namespace,problem_definition:ProblemDefinition,setname):
    """
    Adds the parameters from the ``setname`` parameter sets in
    ``problem_definition`` to the namespace ``ns``.

    Parameters
    ----------
    ns : Namespace
        namespace to which the parameters are added.
    problem_definition : ProblemDefinition
        Contains the problem parameters that are added to the
        namespace.
    setname : str
        String containing the name of the parameter sets in
        the ProblemDefinition.
    """
    for param, value in problem_definition.get(setname).items():
        if value is not None:
            val = value
            if type(val) == str:
                try:
                    val = float(value)
                except:
                    val = getattr(ns,value)
            setattr(ns,param,val)


def _add_element_sizes_to_namespace(ns:Namespace,solution_set:SolutionSet):
    """
    Calculates the element and facet sizes and adds these to the namespace.

    Parameters
    ----------
    ns : Namespace
        namespace to which the element and facet sizes are added.
    solution_set: SolutionSet
        Contains the mesh of which the element and facet sizes are determined. 
    """
    ns.hE = compute_element_sizes(solution_set)
    ns.hF = compute_element_facet_sizes(solution_set)


def _add_model_dependent_fields_to_namespace(ns:Namespace,problem_definition:ProblemDefinition):
    """
    Adds additional field (expressions) that are relevant only for 
    particular models (e.g., the chemical potential for a primal form).

    Parameters
    ----------
    ns : Namespace
        namespace to which the density functional is added.
    problem_definition: ProblemDefinition
        Can be used to indicate different density
        interpolations if needed
    """

    if not problem_definition.get('model','chemical_potential'):
        ns.μ = '(σ / ε) ( φ^3 - φ ) - (σ ε) ∇_k( ∇_k( φ ) )'

def _add_mixed_densities_to_namespace(ns:Namespace,problem_definition:ProblemDefinition):
    """
    Creates an expression for the density as a function of the 
    order parameter and adds this to the namespace.

    Parameters
    ----------
    ns : Namespace
        namespace to which the density functional is added.
    problem_definition: ProblemDefinition
        Can be used to indicate different density interpolations
        if needed
    """
    ns.ρmean = '(ρ1 + ρ2) / 2' # Kg/m3
    ns.ρdiff = '(ρ1 - ρ2) / 2' # Kg/m3
    if problem_definition.get("model","extended_density_function"):
        # a, b, c, d, e = function.partition(ns.φ * ns.ρdiff, - ns.ρmean, -ns.ρ1/2, ns.ρ1/2, ns.ρmean)
        
        a, b, c, d, e = function.partition(ns.φ , - 1-2*ns.ρ2/(ns.ρ1-ns.ρ2), -1-ns.ρ2/(ns.ρ1-ns.ρ2), 1+ns.ρ2/(ns.ρ1-ns.ρ2), 1+2*ns.ρ2/(ns.ρ1-ns.ρ2))
        ns.ρmix = (a+b) * .25*ns.ρ2 + b * (ns.ρdiff*ns.φ+ns.ρmean)**2/ns.ρ2 + c * (ns.ρdiff*ns.φ+ns.ρmean) - d * (ns.ρdiff*ns.φ-ns.ρmean)**2/ns.ρ2 + (d+e) * (ns.ρ1+.75*ns.ρ2)
    else:
        ns.ρmix = 'ρmean + φ ρdiff'


def _add_mixed_viscosities_to_namespace(ns:Namespace,problem_definition:ProblemDefinition):
    """
    Creates an expression for the viscosity as a function of the
    order parameter and adds this to the namespace.

    Parameters
    ----------
    ns : Namespace
        namespace to which the viscosity functional is added
    problem_definition : ProblemDefinition
        Contains the information which viscosity functional to use
    """
    if problem_definition.get('phys','Λ') is None:
        ns.μmean = '(μ1 + μ2) / 2' # Pa*s
        ns.μdiff = '(μ1 - μ2) / 2' # Pa*s\
        ns.μmix = 'μmean + φ μdiff'
    else:
        ns.μmix = 'exp(((1 + φ) Λ log(μ1) + (1 - φ) log(μ2)) / ((1 + φ) Λ + (1 - φ)))'


def _add_stress_tensor_to_namespace(ns:Namespace,problem_definition:ProblemDefinition):
    """
    Adds the expression for the viscous stress tensor to the namespace.

    Parameters
    ----------
    ns : Namespace
        namespace to which the viscous stress tensor is added.
    problem_definition : ProblemDefinition
        Can be used to indicate different stress tensors if needed
    """
    ns.εmix_ij = '∇_j(u_i) + ∇_i(u_j)'
    ns.τmix_ij = 'μmix εmix_ij'


def _add_body_force_to_namespace(ns:Namespace,problem_definition:ProblemDefinition):
    """
    Adds the expression for the body force as Ff + Fg rho to the namespace.

    Parameters
    ----------
    ns : Namespace
        namespace to which the viscous stress tensor is added.
    problem_definition : ProblemDefinition
        Can be used to indicate different stress tensors if needed
    """
    if problem_definition.get('phys','Ff') is not None \
            and problem_definition.get('phys','Fg') is not None:
        ns.F_i = 'Fg_i ρmix + Ff_i'
    elif problem_definition.get('phys','Ff') is not None:
        ns.F_i = 'Ff_i'
    elif problem_definition.get('phys','Fg') is not None:
        ns.F_i = 'Fg_i ρmix'


def _add_phasefield_potential_to_namespace(ns:Namespace,problem_definition:ProblemDefinition):
    """
    Adds the expression for the phasefield potential to the namespace. 

    Parameters
    ----------
    ns : Namespace
        namespace to which the expression for the phasefield potential 
        is added.
    problem_definition : ProblemDefintion
        Indicates which expression for the phasefield potential is used.
    """
    if problem_definition.get('phys','dwa') is None:
        ns.ψ = '0.25 (φ^2 - 1)^2'
        ns.dψ = 'φ^3 - φ'
    else:
        left, mid, right = function.partition(ns.φ, -1, 1)
        dwa = problem_definition.get('phys','dwa')
        dwm = problem_definition.get('phys','dwm')
        ns.dψ = left*( dwa * ( -1/(dwm-1) + 1/(dwm+ns.φ) ) ) + \
                mid*( ns.φ**3 - ns.φ ) + \
                right*( dwa * ( 1/(dwm-1) -1/(dwm-ns.φ) ) )


def _add_surface_tension_to_namespace(ns:Namespace,problem_definition:ProblemDefinition):
    """
    Adds the expression for the surface tension, σ, as a
    function of σ12, to the namespace.

    Parameters
    ----------
    ns : Namespace
        namespace to which the viscous stress 
        tensor is added.
    problem_definition : ProblemDefinition
        Can be used to indicate different stress
        tensors if needed
    """
    ns.σ = ' ( 3 / (2 sqrt(2)) ) σ12'
    ns.σSF = '0.25 (φ^3 - 3 φ) ( σ01 - σ02 ) + 0.5 ( σ01 + σ02 )'
    ns.dσSF = '0.75 (φ^2 - 1) (σ01 - σ02)'


def _add_capillary_stress_tensor_to_namespace(ns:Namespace,problem_definition:ProblemDefinition):
    """
    Adds the expression for the capillary stress tensor to the namespace.

    Parameters
    ----------
    ns : Namespace
        namespace to which the viscous stress tensor is added.
    problem_definition : ProblemDefinition
        Can be used to indicate different stress tensors if needed
    """
    ns.δ = function.eye(ns.x.shape[0])
    ns.ζmix_ij = '- (σ ε) ∇_i(φ) ∇_j(φ) + .5 ε σ ∇_k(φ) ∇_k(φ) δ_ij + (σ / ε) ψ δ_ij' # excluding  '- μ φ δ_ij' used in 'van Brummelen et al. 2020'
