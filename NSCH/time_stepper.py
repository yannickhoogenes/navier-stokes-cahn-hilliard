from __future__ import annotations
from nutils import solver
from .adaptive_refinement import *
from .solution_set import SolutionSet
from .time_stepping_algorithms import take_time_step,get_implemented_RK_methods,get_implemented_theta_methods
from .post_processing import *
import treelog, time
import itertools
from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from .domain_definitions import DomainDefinition
    from .problem_definition import ProblemDefinition


def logcontext(fn):
    """
    Decorator to put cleanly puy ``TimeStepper`` actions into a ``treelog`` context.
    """
    try:
        contextname = fn.__name__.replace("action_",'').replace('_',' ')
    except:
        contextname = fn.__name__ 
    @treelog.context(contextname)
    def wrapper(*args,**kwargs):
        return fn(*args,**kwargs)
    wrapper.__name__ = fn.__name__
    return wrapper

class TimeStepper:
    """
    Data-holder for one time-step. Contains data about simulation parameters, refinement
    level solutions, previous time-step data and actions performed in a time-step.
    Initialization arguments are optional: these objects may be passed after initialization
    too with the specific setter functions.

    Parameters
    ----------
    problem_definition : ProblemDefinition, optional
        The data-holder for all simulation settings and parameters.
    domain_definition : DomainDefinition, optional
        The data-holder for the domain, geometry and boundary conditions.
    initial_condition_generator : function, optional
        A function that takes a ``SolutionSet`` and returns the integrand that
        represents the projection of all fields on the basis of the ``SolutionSet``.

    Attributes
    ----------
    problem_definition : ProblemDefinition
        The data-holder for all simulation settings and parameters.
    domain_definition : DomainDefinition
        The data-holder for the domain, geometry and boundary conditions.
    solution_sets : list of SolutionSet
        List of SolutionSet of refinement levels of the current time-step.
    previous_solution_sets : list of SolutionSet
        List of SolutionSet of refinement levels of the previous time-step.
    last_constraint_vector : dict
        Dictionary of dofs constrained by boundary conditions.
    t : float
    time_step_nr : int
    base_solution_set : SolutionSet
        Used as the base from which all subsequent timesteps are copies and 
        of which all refinements for derived.
    dt : float
        Time-step size of current time-step.
    dt_tracker : list of float
        List of time-step sizes for next time-steps.
    last_inertia : None 
    last_residual : None
    time_step_action_list : list of function calls
        List of actions that are performed in this time-step.
    terminate_action_list : list of function calls
        List of actions that are performed when the simulation is terminated.
    solver_error_action_list : list of function calls
        List of actions that are performed when the solver runs into an error.
    """
    def __init__(self,problem_definition:ProblemDefinition=None,domain_definition:DomainDefinition=None,initial_condition_generator=None):
        self.problem_definition:ProblemDefinition = None
        self.domain_definition:DomainDefinition = None
        self.solution_sets:list[SolutionSet] = []
        self.previous_solution_sets:list[SolutionSet] = []
        self.last_constraint_vector = None
        self.__cached_inertia = None
        self.__cached_residual = None
        if problem_definition is not None:
            self.set_problem_definition(problem_definition)
        if domain_definition is not None:
            self.set_domain_definition(domain_definition)
        if initial_condition_generator is not None:
            self.set_initial_condition(initial_condition_generator)
    
    def set_problem_definition(self,problem_definition:ProblemDefinition):
        """
        Sets the ``problem_definition`` and extracts all required information. This
        is automatically called during initialization if the ``problem_definition``
        is provided to the constructor

        Parameters
        ----------
        problem_definition : ProblemDefinition
            Includes all parameters to run the simulation.
        """
        treelog.user(problem_definition)
        self.problem_definition = problem_definition
        self.t = problem_definition.get('time',"t")
        self.dt = problem_definition.get('time',"dt")
        self.dt_tracker = [self.dt]
        self.output_directory = get_output_directory(problem_definition.get('pp',"export_folder"))
        self.fill_action_list()
        self.fill_action_list_termination()
        self.fill_action_list_solver_error()
        if self.domain_definition is not None:
            self._finalize_set_problem_and_domain_definition()
        self.time_step_log = logcontext_counter("Timestep")
    
    def set_domain_definition(self,domain_definition:DomainDefinition):
        """
        Sets the ``set_domain_definition`` creates the ``base_solution_set``. This
        is automatically called during initialization if the ``set_domain_definition``
        is provided to the constructor

        Parameters
        ----------
        domain_definition : DomainDefinition
            Includes information on geometry, topology, axsisymmetry and BCs.
        """
        self.domain_definition = domain_definition
        if self.problem_definition is not None:
            self._finalize_set_problem_and_domain_definition()
    
    def _finalize_set_problem_and_domain_definition(self):
        """
        Is called when both a ``ProblemDefinition`` object and a ``DomainDefinition``
        have been passed, to specify some final settings.
        """
        self.domain_definition.plot_sample_degree = self.problem_definition.get("pp","plot_sample_degree")
        self.domain_definition.pvd_sample_degree = self.problem_definition.get("pp","pvd_sample_degree")
        self.base_solution_set = SolutionSet(self.problem_definition,self.domain_definition,t=self.t)

    def set_initial_condition(self,ic_generator):
        """
        Sets the initial condition of the problem for all refinement levels. The 
        initial condition can be loaded from a file or generated in a function.

        Parameters
        ----------
        ic_generator : function or str
            File name from which the initial condition is loaded as a .dill file,
            or a function that takes a ``SolutionSet`` and returns the projection
            bilinear form for all relevant fields.
        """
        if self.problem_definition is None or (self.domain_definition is None and type(ic_generator) != str):
            treelog.error("First provide the ProblemDefinition and DomainDefinition before supplying the ic_generator.")
            exit()
        if type(ic_generator) == str: # Load from file
            self.t, self.solution_sets = load_dill(ic_generator)
            self.set_domain_definition(self.solution_sets[0].domain_definition)
            self.action_export_plot()
        else: # Project given function
            self.solution_sets = [self.base_solution_set.copy(t=self.t)]
            refinement_level = logcontext_fraction("Initialization",self.problem_definition.get("FE","refinement_levels"))
            for r in range(0,self.problem_definition.get("FE","refinement_levels") + 1):
                self.append_refined_initial_condition(ic_generator,refinement_level)
                export_plot(self.solution_set,['0'],plotmesh=True,filename="mesh_ref"+str(r)+".png")
                self.action_export_plot()
                next(refinement_level)
        self.action_export_pvd()
        next(self.time_step_log)
        
    def append_refined_initial_condition(self,ic_generator,refinement_level):
        """
        Sets the initial condition of the problem for all refinement levels.
        The initial condition can be loaded from a file or generated in a function.

        Parameters
        ----------
        ic_generator : function or str
            Function handle or file name from which
            the initial condition is generated.
        refinement_level : int
            Refinement level for which the initial
            condition is determined.
        """
        self.problem_definition.refinement = refinement_level.get() # Updates physical parameters that affect the IC
        if refinement_level.get() > 0:
            refdom,vbt = get_refined_residual_vector_steady(self.solution_set,ic_generator,self.solution_set.get_fieldnames())
            vbt = get_error_bounds_vector(self.problem_definition.get("FE","refinement_measure"),refdom,vbt)
            refinement_indices = mark( refdom, vbt, self.problem_definition.get("FE","refinement_fraction") \
                                                  , self.problem_definition.get("pp","plot_refinement_details"))
            self.solution_sets.append( self.solution_set.refine(refinement_indices) )
        self.action_determine_constraint_vector(refinement_level=refinement_level)
        res_list = [ ic_generator(self.solution_set).derivative(testdofs) for testdofs in self.solution_set.get_doflist_test() ]
        ic_solution_dict = solver.solve_linear(self.solution_set.get_doflist_trial(), res_list, constrain=self.last_constraint_vector)
        self.solution_set.solution_dict = ic_solution_dict

    @property
    def solution_set(self):
        """Returns the latest solution set"""
        return self.solution_sets[-1]
    
    @property
    def time_step_nr(self):
        """Returns current timestep number from logger"""
        return self.time_step_log.get()

    def get_previous_solution_set(self,index=-1):
        """
        Returns a SolutionSet of refinement level ``index``
        of the previous time-step.

        Parameters
        ----------
        index : integer
            Refinement level of the SolutionSet of the previous
            time-step that needs to be returned .
            Default : -1
        Returns
        -------
        SolutionSet
            SolutionSet of refinement level ``index``
            of the previous time-step.
        """
        return self.previous_solution_sets[index]

    def get_current_solution_set(self,index=-1):
        """
        Returns a SolutionSet of refinement level ``index``
        of the current time-step.

        Parameters
        ----------
        index : integer
            Refinement level of the SolutionSet of the current
            time-step that needs to be returned .
            Default : -1
        Returns
        -------
        SolutionSet
            SolutionSet of refinement level ``index``
            of the current time-step.
        """
        return self.solution_sets[index]

    def fill_action_list(self):
        """
        Fills ``time_step_action_list`` with actions. Actions
        are function calls that perform an operation on the
        Timestepper. This action list contains all actions for 
        one time-step.
        """
        actions_t_refine = []
        if self.problem_definition.get("time","adaptive_method") == 1:
            actions_t_refine = [self.action_double_timestep_size]
        actions_t       = [self.action_obtain_previous_solution_set, \
                        self.action_project_previous_solution, \
                        self.action_obtain_current_forms, \
                        self.action_obtain_previous_forms, \
                        self.action_determine_constraint_vector, \
                        self.action_get_initial_guess, \
                        self.action_take_time_step, \
                        self.action_perform_user_operation, \
                        self.action_export_plot]
        actions_export  = [self.action_export_pvd, \
                        self.action_export_dill]
        actions         = actions_t_refine + [ self.action_initialize_timestep ] \
                        + (actions_t + [ self.action_refine ] ) * self.problem_definition.get("FE","refinement_levels") \
                        + actions_t  + [ self.action_increment_time ] + actions_export + [ self.action_end_timestep ]
        self.time_step_action_list = actions

    def fill_action_list_termination(self):
        """
        Fills ``terminate_action_list`` with actions. Actions
        are function calls that perform an operation on the
        Timestepper. This action list contains all actions for 
        termination of the simulation.
        """
        actions = []
        if self.problem_definition.get("pp","dill_filename") != "":
            actions = [ self.action_reset_timestep,
                        lambda **kwargs: self.action_export_dill(overwrite=True, **kwargs)]
        self.terminate_action_list = actions + [self.action_raise_error ]

    def fill_action_list_solver_error(self):
        """
        Fills ``solver_error_action_list`` with actions. Actions
        are function calls that perform an operation on the
        Timestepper. This action list contains all actions to
        decrease the time-step size when the solver runs into an
        error.
        """
        if self.problem_definition.get("time","adaptive_method") == 1:
            # Succesive halving
            actions = [ self.action_halve_timestep_size,
                        self.action_reset_timestep ]
        else:
            actions = self.terminate_action_list
        self.solver_error_action_list = actions

    def perform_time_step(self):
        """
        Performs all actions in an action list. Actions
        are function calls that perform an operation on the
        Timestepper.
        """
        t_start = time.perf_counter()
        in_out = {}
        try:
            for action in self.time_step_action_list:
                t_action = time.perf_counter()
                in_out = action(**in_out)
                treelog.debug(f"{action.__name__} completed in {time.perf_counter()-t_action} sec.")
        except solver.SolverError as e:
            for action in self.solver_error_action_list:
                in_out = {**in_out,"exception":e}
                in_out = action(**in_out)
        except KeyboardInterrupt as e:
            for action in self.terminate_action_list:
                in_out = {**in_out,"exception":e}
                in_out = action(**in_out)
        except Exception as e:
            for action in self.terminate_action_list:
                in_out = {**in_out,"exception":e}
                in_out = action(**in_out)
        treelog.debug(f"====== Finished timestep in {time.perf_counter()-t_start} seconds ======")

    def stop_criterion_reached(self):
        """
        Checks if the simulation has reached the final
        time.

        Returns
        -------
        bool
            True if the current simulation time is larger
            than the final time of this simulation.
        """
        return self.t + 1E-3*self.dt > self.problem_definition.get('time',"T")
    
    def _kwargsr_to_kwargs(self,kwargsr,r):
        """
        The kwargs passed between action list items have the refinement in the name.
        This function creates a new dictionary that only includes the arguments from
        refinement `r`.
        """
        return {key.replace("_"+str(r),''):val for key,val in kwargsr.items() if key.endswith("_"+str(r))}

    # Below follow check functions that determine whether actions should be executed

    def should_recompute_constrain_vectors(self):
        """
        Checks if the boundary constrain vectors need to be recomputed
        
        Returns
        -------
        bool
            True if there is no constrain vector from a previous time-step
            or if there is a transient dirichlet condition
            or if there are refinements
        """
        return self.last_constraint_vector is None \
               or self.domain_definition.Dirichlet_transient \
               or not self.problem_definition.get("FE","refinement_levels") == 0

    def should_obtain_current_forms(self):
        """
        Checks if the current inertia and residual integral need to be calculated.
        
        Returns
        -------
        bool
            True if there is not residual integral from the previous time-step
            or if there are refinements.
        """
        return self.__cached_residual is None \
            or (not self.problem_definition.get("FE","refinement_levels") == 0) \
               or self.domain_definition.Dirichlet_transient

    def should_obtain_previous_forms(self):
        """
        Checks if the previous inertia and residual integral need to be calculated.
        
        Returns
        -------
        bool
            True if the time-step method is a theta-method.
        """
        return self.problem_definition.get("time","method") in get_implemented_theta_methods()

    def should_project_previous_solution(self):
        """
        Checks if the previous solution must be projected to the current domain.
        
        Returns
        -------
        bool
            True if the time-step method is a Runge-Kutta method.
        """
        return self.problem_definition.get("time","method") in get_implemented_RK_methods()

    def should_get_initial_guess(self):
        """
        Checks if an initial guess needs to be obtained.
        
        Returns
        -------
        bool
            True if the time-step method is a theta-method
        """
        return self.problem_definition.get("time","method") in get_implemented_theta_methods()

    def should_export_plot(self):
        """
        Checks if a plot needs to be made
        
        Returns
        -------
        bool
            True if there is a plot name and (the simulation is terminated
            or the time-step is in the plotting intervals)
        """
        return self.should_export("plot")
    
    def should_export_pvd(self):
        """
        Checks if a pvd-file needs to be updated and vtu file needs to be made
        
        Returns
        -------
        bool
            True if there is a pvd-file name and (the simulation is terminated
            or the time-step is in the pvd-file export intervals)
        """
        return self.should_export("pvd")

    def should_export_dill(self):
        """
        Checks if a dill-file needs to be made
        
        Returns
        -------
        bool
            True if there is a dill-file name and (the simulation is terminated
            or the time-step is in the dill-file export intervals)
        """
        return self.should_export("dill")

    def should_export(self,type):
        """
        Checks if the simulation has reached the final time.

        Parameters
        ----------
        type : str
            ``"plot"``, ``"pvd"``, or ``"dill"`` depending on the exported file

        Returns
        -------
        bool
            True if there is a ``type`` file name and (the simulation is terminated
            or the time-step is in the ``type`` export intervals)
        """
        interval = self.problem_definition.get('pp',f"{type}_interval")
        return ((interval==-1 and self.stop_criterion_reached()) 
                    or (interval > 0 and not self.time_step_nr % interval)  )

    # Below follow potential actions, that can be stacked for execution in each time step

    def action_initialize_timestep(self, **kwargs):
        """
        Creating and setting all variables required in further actions.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the refinement=0 and the kwargs.
        """
        self.problem_definition.refinement = 0 # for refinement level based parameters
        self.previous_solution_sets = self.solution_sets
        self.solution_sets = [self.base_solution_set.copy(t=self.t+self.dt,fill_ns=True)]
        ref_log = logcontext_fraction("Refinement",self.problem_definition.get("FE","refinement_levels"))
        return { "refinement_level": ref_log, **kwargs}

    @logcontext
    def action_obtain_previous_solution_set(self, **kwargs):
        """
        Obtains the SolutionSet of the current refinement level
        of the previous time-step.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the SolutionSet of the
            user specified refinement level of the previous 
            time-step and the kwargs.
        """
        r = kwargs["refinement_level"].get()
        previous_level = get_refinement_level_previous(r,self.problem_definition)
        solution_set_prev = self.get_previous_solution_set(previous_level)
        cross_solution_set = self.solution_set.merge(solution_set_prev, t=solution_set_prev.t)
        return {"solution_set_prev_%i"%r:cross_solution_set, **kwargs}

    @logcontext
    def action_obtain_previous_forms(self, **kwargs):
        """
        Obtains the inertia and residual integrals for the previous
        time-step on the current domain if needed.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the inertia and residual integral
            of the previous time-step on the current domain and the kwargs.
        """
        r = kwargs["refinement_level"].get()
        if self.should_obtain_previous_forms():
            solution_set_prev = kwargs["solution_set_prev_%i"%r]
            if self.problem_definition.get("FE","refinement_levels") == 0 and self.__cached_inertia is not None:
                mixed_previous_inertia = self.__cached_inertia
            else:
                mixed_previous_inertia = self.problem_definition.get_inertia(solution_set_prev)
            if self.problem_definition.get("FE","refinement_levels") == 0 and self.__cached_residual is not None and not self.domain_definition.Dirichlet_transient:
                mixed_previous_residual = self.__cached_residual
            else:
                mixed_previous_residual = self.problem_definition.get_residual(solution_set_prev)
            return {"inertia_prev_%i"%r:mixed_previous_inertia,"residual_prev_%i"%r:mixed_previous_residual,**kwargs}
        return kwargs

    @logcontext
    def action_project_previous_solution(self, **kwargs):
        """
        Projects the solution from the previous time-step and
        the current refinement level onto the domain of the
        current time-step and refinement level if needed.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the solution from the current
            refinement level and the previous solution projected
            onto the domain of the current refinement level and
            current time-step and the kwargs.
        """
        r = kwargs["refinement_level"].get()
        if self.should_project_previous_solution():
            solution_set_prev = kwargs["solution_set_prev_%i"%r]
            if solution_set_prev.refinement_level == r == 0: # Previous and current state both on base mesh
                return {"solution_dict_prev_projected_0":kwargs["solution_dict_prev_0"],**kwargs}
            solution_dict_prev = solution_set_prev.solution_dict
            projected_previous_solution = inter_mesh_projection(self.solution_set,solution_set_prev,solution_dict_prev)
            return {"solution_dict_prev_projected_%i"%r:projected_previous_solution,**kwargs}
        return kwargs
    
    @logcontext
    def action_obtain_current_forms(self, **kwargs):
        """
        Obtains the inertia and residual integrals for the current
        time-step if needed.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the inertia and residual integral
            of the current time-step and the kwargs.
        """
        r = kwargs["refinement_level"].get()
        solution_set_prev = kwargs["solution_set_prev_%i"%r]
        if self.should_obtain_current_forms():
            last_inertia = self.problem_definition.get_inertia(self.solution_set)
            last_residual = self.problem_definition.get_residual(self.solution_set,solution_set_prev=solution_set_prev)
            if self.__cached_inertia is None: self.__cached_inertia = last_inertia
            if self.__cached_residual is None: self.__cached_residual = last_residual
        else:
            last_inertia = self.__cached_inertia
            last_residual = self.__cached_residual
        return {"inertia_%i"%r:last_inertia,"residual_%i"%r:last_residual,**kwargs}

    @logcontext
    def action_determine_constraint_vector(self,**kwargs):
        """
        If needed determines the constrained values of the dofs
        according to the boundary conditions.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the dictionary with the
            nodal values imposed by the boundary condition and
            the kwargs.
        """
        r = kwargs["refinement_level"].get()
        if self.should_recompute_constrain_vectors():
            domain = self.solution_set.domain; ns = self.solution_set.ns
            get_degree=self.solution_set.get_degree; intθ = self.solution_set.domain_definition.intθ
            treelog.info(f"Recomputing boundary conditions at t={ns.t.eval()}")
            sqr = 0; doflist= []
            for field, boundary, expression in self.domain_definition.Dirichlet_bcs:
                sqr += domain.boundary[boundary].integral(intθ( f"({field} - {expression}) ({field} - {expression}) dS" @ ns ), degree=get_degree(f'{field[0]}{field[0]}+'))
                doflist.append(field+"dofs") if not '_' in field else doflist.append(field.partition("_")[0]+"dofs") # Determine field dofs, also in case of vector component
            doflist = list(set(doflist)) # Remove duplicates
            self.last_constraint_vector = solver.optimize(doflist, sqr, droptol=1e-14) if not len(doflist) == 0 else None
        return {"constraint_vector_%i"%r:self.last_constraint_vector, **kwargs}

    @logcontext
    def action_get_initial_guess(self,**kwargs):
        """
        If needed determines initial guess for the solution
        of the current time-step based on the previous
        time-step.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the dictionary with the
            nodal values of the intial guess and
            the kwargs.
        """
        r = kwargs["refinement_level"].get()
        if self.should_get_initial_guess():
            if r == 0:
                return { "initial_guess_0":self.get_previous_solution_set(0).solution_dict, **kwargs}
            guess_solution_set = self.get_current_solution_set(r-1) # self.get_solution_set("prev",r)
            initial_guess = inter_mesh_projection(self.solution_set,guess_solution_set,guess_solution_set.solution_dict)
            return { "initial_guess_%i"%r:initial_guess, **kwargs}
        return { "initial_guess_%i"%r:{}, **kwargs} # No initial guess

    @logcontext
    def action_take_time_step(self, **kwargs):
        """
        Calculate the solution of the current refinement level
        of the current time-step and increases the simulation
        time of the SolutionSet by the time-step size.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the kwargs.
        """
        r = kwargs["refinement_level"].get()
        kwargs_current_refinement_level = self._kwargsr_to_kwargs(kwargs,r)
        solution_dict = take_time_step(self.problem_definition.get('time',"method"), \
                t=self.t,dt=self.dt,**kwargs_current_refinement_level, \
                newtontol=self.problem_definition.get('FE',"tol_newton_solver"),failrelax=self.problem_definition.get('FE',"fail_relaxation"))
        self.solution_set.solution_dict = solution_dict
        # self.solution_set.t = self.t+self.dt
        return kwargs

    @logcontext
    def action_perform_user_operation(self, **kwargs):
        """
        Perform user defined action.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the kwargs.
        """
        self.problem_definition.get("model","user_operation")(self,self.solution_set,**kwargs)
        return kwargs

    def action_refine(self,**kwargs):
        """
        Create a new SolutionSet with a domain that is refined
        according to the refinement parameters. The new
        SolutionSet is appended to ``solution_sets``.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the kwargs.
        """
        r = kwargs["refinement_level"].get()
        previous_level = get_refinement_level_previous(r,self.problem_definition)
        solution_set_prev = self.get_previous_solution_set(previous_level)
        self.problem_definition.refinement = r+1 # Set prior to estimate & mark so that these relates to updated parameters
        refinement_indices = get_refinement_indices(self.problem_definition,self.solution_set,solution_set_prev)
        new_solution_set = self.solution_set.refine(refinement_indices)
        self.solution_sets.append(new_solution_set)
        next(kwargs["refinement_level"])
        export_plot(self.solution_set,['0'],plotmesh=True,filename="mesh_ref"+str(r)+".png")
        return kwargs

    def action_export_plot(self, **kwargs):
        """
        If needed exports the field names of the fields
        that need to be plotted and the file name to the 
        post processing to make plots.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the kwargs.
        """
        if self.should_export_plot():
            r = self.solution_set.refinement_level
            plot_fields = self.problem_definition.get('pp',"plot_fields")
            plot_mesh = self.problem_definition.get('pp',"plot_mesh_overlay")
            export_plot(self.solution_set,plot_fields,plotmesh=plot_mesh,filename=f"%s_ref{r}.png")
        return kwargs

    @logcontext
    def action_export_pvd(self, **kwargs):
        """
        If needed updates the pvd file and exports the vtu file
        name and the SolutionSet to the post processing to 
        export a vtu file.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the kwargs.
        """
        if self.should_export_pvd():
            if not os.path.isdir("{}/{}_VTU/".format(self.output_directory,self.problem_definition.get('pp',"pvd_filename"))):
                os.makedirs("{}/{}_VTU/".format(self.output_directory,self.problem_definition.get('pp',"pvd_filename")))

            vtu_fname     = "{}/{}_VTU/VTU_{}".format(self.output_directory,self.problem_definition.get('pp',"pvd_filename"),self.time_step_nr)
            export_vtu(vtu_fname,self.solution_set)
            pvd_fname     = self.problem_definition.get('pp',"pvd_filename")
            export_folder = self.output_directory
            export_pvd(export_folder,pvd_fname, self.time_step_nr, self.t)
        return kwargs

    @logcontext
    def action_export_dill(self, overwrite=False, **kwargs):
        """
        If needed exports the dill file name and the ``solution_sets``
        to the post processing to export a dill file.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the kwargs.
        """
        if self.should_export_dill() or overwrite:
            if not os.path.isdir(self.output_directory):
                os.mkdir(self.output_directory)
            fname_base =  "{}/{}_t{}.dill".format(self.output_directory,self.problem_definition.get('pp',"dill_filename"),self.time_step_nr)
            export_dill(fname_base,self.solution_sets)
        return kwargs

    @logcontext
    def action_increment_time(self, **kwargs):
        """
        Increases the simulation time of the TimeStepper by the time-step size.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the kwargs.
        """
        self.t += self.dt
        return kwargs

    def action_end_timestep(self, **kwargs):
        """
        All wrap-up of all time-step actions.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the kwargs.
        """
        next(kwargs["refinement_level"]) # Finish last refinement level
        next(self.time_step_log)
        if self.stop_criterion_reached():
            self.time_step_log.exit()
        return kwargs

    @logcontext
    def action_double_timestep_size(self,**kwargs):
        """
        Increases the time-step size to the last value in
        ``dt_tracker``

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the kwargs.
        """
        self.dt = self.dt_tracker[-1]
        if len(self.dt_tracker) > 1:
            treelog.warning(f"Timestep size {self.dt}, at {self.time_step_nr}-th step.")
            self.dt_tracker.pop()
        return kwargs

    @logcontext
    def action_halve_timestep_size(self,**kwargs):
        """
        Halves the time-step size and appends the small
        time-step size a number of times to ``dt_tracker``
        as determined in ProblemDefinition.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the kwargs.
        """
        self.dt /= 2
        treelog.warning(f"Halved timestep size to {self.dt}, at {self.time_step_nr}-th step.")
        if self.dt+1E-16 < self.problem_definition.get("time","minimal_dt"):
            treelog.error("Smallest permitted time-step reached")
            raise Exception("Smallest permitted time-step reached")
        self.dt_tracker += [self.dt]*self.problem_definition.get("time","repeat_halved_dt")
        return kwargs
    
    def action_reset_timestep(self,**kwargs):
        """
        Resets the ``solution_sets`` of the current
        time-step to the ``solution_sets`` of the
        previous time-step.

        Parameters
        ----------
        kwargs : \*\*kwargs

        Returns
        -------
        dict
            A dictionary containing the kwargs.
        """
        kwargs["refinement_level"].exit()
        self.solution_sets = self.previous_solution_sets
        return kwargs

    def action_raise_error(self,**kwargs):
        """Raises an exception"""
        raise( kwargs["exception"] )


class logcontext_counter(object):
    """
    Wrapper around the ``treelogger`` context concept that counts from a certain
    start value and with a title. Used as the "Timestep: %i" context.
    
    Parameters
    ----------
    title : float
        Title printed at each new step of the context.
    offset : int , optional
        Start value of the counting.
    """
    def __init__(self, title, offset=0):
        self.current = offset
        title = map((title + ': {}').format, itertools.count(offset))
        self.treelog_instance = treelog.iter.wrap(title, itertools.count(offset+1))
        self.generator = self.treelog_instance.__enter__()

    def __next__(self):
        try:
            self.current = next(self.generator)
        except StopIteration as e:
            self.exit(type(e),e,e.__traceback__)
        return self.current
    
    def exit(self, exc_type=None, exc_val=None, exc_tb=None):
        self.treelog_instance.__exit__(exc_type, exc_val, exc_tb)

    def get(self):
        return self.current

        
class logcontext_fraction(logcontext_counter):
    """
    Wrapper around the ``treelogger`` context concept that counts until a certain
    max is achieved and with a title. Used as the "Refinement: %i/%i" context.
    
    Parameters
    ----------
    title : float
        Title printed at each new step of the context.
    amount : int , optional
        Maximum value of the fraction.
    """
    def __init__(self, title, amount):
        title = map((title + ' {}/' + str(amount)).format, itertools.count(0))
        self.treelog_instance = treelog.iter.wrap(title, range(1,amount+1))
        # self.treelog_instance = treelog.iter.fraction(title+":", range(amount))
        self.generator = self.treelog_instance.__enter__()
        self.current = 0 # Start at 1 instead of 0
