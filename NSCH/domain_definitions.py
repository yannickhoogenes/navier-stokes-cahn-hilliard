from nutils import mesh
from numpy import linspace
import numpy
import treelog

class DomainDefinition:
    """
    Data-holder for the topology, the geometry and the boundary conditions.
    Also includes the required information for handling more complex cases,
    such as axi-symmetric domains (requiring spatialy varying unit-vectors).

    The user typically does not have to deal with this class. It is simply
    returned by the different implementations of the ``create_..._domain``
    functions. Take a look at those implementations for creating aditional 
    domains.

    Parameters
    ----------
    domain : Topology
        The Nutils topology from the mesh construction.
    geometry : numpy.Array
        The position vector for all nodes on the mesh.
    geometry2D : numpy.Array , optional
        The position vector for all nodes on the mesh for the 2D
        slice of an axisymmetric domain. For computing element size.
        Default : None.
    rot : nutils.function.Array , optional
        Mapping as a ``function.stack([..])`` to transform vectors defined on 
        ``domain`` to vectors a 3D-axysymmetric domain.
        Default : None.
    intθ : function , optional
        Takes a Nutils integrable string, and integrates out the theta 
        dependency for axisymmetric cases. Else is simply identity.
        Default : lambda I: I.
    get_plot_domain : function , optional
        Takes the domain and returns the plane on which to plot. Used
        mostly for 3D cases, else just identity.
        Default : lambda I: I.
    plot_vec_axes : tuple(int,int) , optional
        Which two vector components to plot. Used for 3D and axisymmetric cases.
        Default : (0,1)

    Attributes
    ----------
    geometry : Array
    geometry2D : Array
    domain : Topology
    axisymmetric : bool
        Set to ``True`` if ``rot`` is not ``None``.
    rot : nutils.function.Array
    intθ : function
    Dirichlet_bcs : list of [str, str, str]
        [ [fieldname, boundaries, expression], [...], ...]
    Neumann_bcs : list of [str, str, str]
        [ [fieldname, boundaries, expression], [...], ...]
    Dirichlet_transient : bool
        Whether Dirichlet data needs to be updated each timestep
    Neumann_transient : bool
        Whether Neumann data needs to be updated each timestep
    get_plot_domain : function
    plot_vec_axes : tuple(int,int)
    plot_sample_degree : int
    pvd_sample_degree : int
    plot_sample : nutils.Sample
    pvd_sample : nutils.Sample
    """
    def __init__(self, domain, geometry, geometry2D=None,rot=None,intθ=lambda I: I,get_plot_domain=lambda I: I,plot_vec_axes=(0,1)):
        self.geometry = geometry
        self.geometry2D = geometry if geometry2D is None else geometry2D # Axisymmetry: to be able to integrate 2D element sizes
        self.domain = domain

        self.axisymmetric = (rot is not None)
        self.rot = numpy.eye(domain.ndims) if rot is None else rot
        self.intθ = intθ # Used to transform integrals to axi-symmetric integrals

        self.Dirichlet_bcs = []
        self.Neumann_bcs = []
        self.Dirichlet_transient = False
        self.Neumann_transient = False

        self.get_plot_domain = get_plot_domain
        self.plot_vec_axes = plot_vec_axes
        self.plot_sample_degree = 5 # default, will be overwritted in TimeStepper initialization
        self.pvd_sample_degree = 5 # default, will be overwritted in TimeStepper initialization
        self.__plot_sample = None
        self.__pvd_sample = None
    
    def add_Dirichlet_condition(self, *field_boundary_expression, time_dependent=False):
        """
        Appends to the list of Dirichlet boundary conditions, as assembled by 
        the `TimeStepper` object.

        Parameters
        ----------
        field_boundary_expression : arguments
            A repeated set of three concurrent string arguments: 
            str(field name), str(boundaries), str(expression), ...
        time_dependent : boolean, optional
            Whether the boundary expression needs to be re-evaluated each time-step.
        """
        fbe = field_boundary_expression
        for name,bdy,val in zip(fbe[::3],fbe[1::3],fbe[2::3]):
            if type(val) in [int,float] and val < 0:
                val = f"0 + {-1*val}" # Becomes ( func - 0 + val )
            self.Dirichlet_bcs += [ [name,bdy,val] ]
        self.Dirichlet_transient = self.Dirichlet_transient or time_dependent

    def define_Neumann_condition(self, *field_boundary_expression, time_dependent=False):
        """
        Appends to the list of Neumann boundary conditions. Functionality not yet
        implemented.

        Parameters
        ----------
        field_boundary_expression : arguments
            A repeated set of three concurrent string arguments: 
            str(name), str(boundaries), str(expression), ...
        time_dependent : boolean, optional
            Whether the boundary expression needs to be re-evaluated each time-step.
        """
        fbe = field_boundary_expression
        for i in range(0,len(fbe),3):
            self.Neumann_bcs += [ [fbe[i],fbe[i+1],fbe[i+2]] ]
        self.Neumann_transient = self.Neumann_transient or time_dependent
    
    def copy(self,domain=None):
        """
        Creates a new ``DomainDefinition`` that is the copy of the current, 
        potentially with a new ``domain``.
        
        Parameters
        ----------
        domain : Topology, optional
            ``domain`` for the new ``DomainDefinition``.

        Returns
        -------
        DomainDefinition
        """
        domain = self.domain if domain is None else domain
        new_domain_definition = DomainDefinition(domain, self.geometry, self.geometry2D,self.rot,self.intθ,self.get_plot_domain,self.plot_vec_axes)
        new_domain_definition.axisymmetric = self.axisymmetric
        new_domain_definition.Dirichlet_bcs = self.Dirichlet_bcs
        new_domain_definition.Neumann_bcs = self.Neumann_bcs
        new_domain_definition.Dirichlet_transient = self.Dirichlet_transient
        new_domain_definition.Neumann_transient = self.Neumann_transient
        new_domain_definition.plot_sample_degree = self.plot_sample_degree
        new_domain_definition.pvd_sample_degree = self.pvd_sample_degree
        return new_domain_definition
    
    def refine(self,refinement_indices=None):
        """
        Creates a new ``DomainDefinition`` where the elements of ``domain`` 
        are refined based on ``refinement_indices``.
        
        Parameters
        ----------
        refinement_indices : Array?, optional
            The elements to refine from the ``domain``. If ``None``,
            then homogeneous refinement.

        Returns
        -------
        DomainDefinition
        """
        if refinement_indices is None:
            refined_domain = self.domain.refined
        else:
            refined_domain = self.domain.refined_by(refinement_indices)
        return self.copy(refined_domain)
    
    @property
    def plot_sample(self):
        """
        Obtains and caches the sample for treelog plotting. Mostly implemented 
        to generically handle 2D/3D/axisymmetric cases the same in the post-processing.
        """
        if self.__plot_sample is not None:
            return self.__plot_sample
        self.__plot_sample = self.get_plot_domain(self.domain).sample('bezier', self.plot_sample_degree)
        return self.__plot_sample

    @property
    def pvd_sample(self):
        """
        Obtains and caches the sample for pvd exporting.
        """
        if self.__pvd_sample is not None:
            return self.__pvd_sample
        self.__pvd_sample = self.domain.sample('bezier', self.pvd_sample_degree)
        return self.__pvd_sample
        

def create_square_domain(W, nelems, origin='corner'):
    """
    Create a square domain with given size, the given numbers of elements 
    in x and y direction, and the given location of the origin.

    Parameters
    ----------
    W : float
        Square width.
    nelems : int or list of int
        The elements in x and y. Examples: 5 or [5,10].
    origin : string, optional
        Either 'corner' or 'center' for the location of the origin. Corner
        refers to the lower left corner.
        Default: 'corner'

    Returns
    -------
    DomainDefinition
        A ``DomainDefinition`` object with the ``domain``, ``geometry``, etc.
    """
    return create_rectangular_domain(W,W, nelems, origin=origin)


def create_rectangular_domain(W,H, nelems, origin='corner', periodic=[]):
    """
    Create a rectangular domain with given dimensions, the given numbers 
    of elements in x and y direction, and the given location of the origin.

    Parameters
    ----------
    W : float, or list of float
        Width (of separate sections).
    H : float, or list of float
        Height (of separate sections).
    nelems : int; or list of int; or list of mix of {list of int, and int}
        The elements in x and y. Examples: 5, or [5,10], or [[1, 5, 1], 10].
    origin : string, optional
        Either 'corner' or 'center' for the location of the origin. Corner
        refers to the lower left corner.
        Default: 'corner'

    Returns
    -------
    DomainDefinition
        A ``DomainDefinition`` object with the ``domain``, ``geometry``, etc.
    """
    W = [W] if not type(W) == list else W
    H = [H] if not type(H) == list else H
    if type(nelems) == int:
        nelems = [[nelems],[nelems]]
    else:
        if type(nelems[0]) == int:
            nelems = [[nelems[0]], nelems[1]]
        if type(nelems[1]) == int:
            nelems = [nelems[0], [nelems[1]]]
    assert len(nelems[0]) == len(W), "nelems does not correspond with number of width sections."
    assert len(nelems[1]) == len(H), "nelems does not correspond with number of height sections."
    offset = 0.5 if origin == 'center' else 0 if origin == 'corner' else None
    
    linspace1 = numpy.linspace(0, W[0]-W[0]/nelems[0][0], nelems[0][0])
    for i in range(1, len(W)):
        linspace1 = numpy.concatenate((
            linspace1, numpy.linspace(
                sum(W[:i]), sum(W[:(i+1)])-W[i]/nelems[0][i], nelems[0][i]
            )
        ))
    linspace1 = numpy.concatenate((linspace1, [sum(W)]))
    linspace1 -= offset * sum(W)
    
    linspace2 = numpy.linspace(0, H[0]-H[0]/nelems[1][0], nelems[1][0])
    for i in range(1, len(H)):
        linspace2 = numpy.concatenate((
            linspace2, numpy.linspace(
                sum(H[:i]), sum(H[:(i+1)])-H[i]/nelems[1][i], nelems[1][i]
            )
        ))
    linspace2 = numpy.concatenate((linspace2, [sum(H)]))
    linspace2 -= offset * sum(H)
    
    domain, geometry = mesh.rectilinear([linspace1, linspace2],periodic=periodic)
    domain = domain.withboundary(exterior="left,top,right,bottom")
    return DomainDefinition(domain, geometry)


def create_cube_domain(W, nelems, origin='corner', plotface="front"):
    """
    Create a cube domain with given size, the given numbers of elements
    in x, y and z direction, and the given location of the origin.

    Parameters
    ----------
    W : float
        Cube width.
    nelems : int or list of int
        The elements in x, y and z. Examples: 5 or [5,10,5].
    origin : string, optional
        Either 'corner' or 'center' for the location of the origin. Corner
        refers to the lower bottom left corner.
        Default: 'corner'

    Returns
    -------
    DomainDefinition
        A ``DomainDefinition`` object with the ``domain``, ``geometry``, etc.
    """
    return create_box_domain(nelems, W,W,W, origin=origin, plotface=plotface)


def create_box_domain(nelems, W,H,D, origin='corner', plotface="front"):
    """
    Create a 3D-box domain with given dimensions, the given numbers of 
    elements in x, y and z direction, and the given location of the origin.

    Parameters
    ----------
    W : float
        Width.
    H : float
        Height.
    D : float
        Depth.
    nelems : int or list of int
        The elements in x, y and z. Examples: 5 or [5,10,5].
    origin : string, optional
        Either 'corner' or 'center' for the location of the origin. Corner
        refers to the lower left corner.
        Default: 'corner'

    Returns
    -------
    DomainDefinition
        A ``DomainDefinition`` object with the ``domain``, ``geometry``, etc.
    """
    nelems = [nelems,nelems,nelems] if type(nelems) != list else nelems
    offset = 1/2 if origin == 'center' else 0 if origin == 'corner' else None
    domain, geometry = mesh.rectilinear([linspace(-offset*W, (1-offset)*W, nelems[0]+1), 
                                         linspace(-offset*H, (1-offset)*H, nelems[1]+1),
                                         linspace(-offset*D, (1-offset)*D, nelems[2]+1)])
    domain = domain.withboundary(exterior="left,top,right,bottom,front,back")
    plot_vec_axes = (0,2) if plotface in ["top","bottom"] else (1,2) if plotface in ["left","right"] else (0,1)
    return DomainDefinition(domain, geometry, get_plot_domain=lambda domain: domain.boundary[plotface],plot_vec_axes=plot_vec_axes)


def create_axisymmetric_cylindrical_domain(R,H, nelems):
    """
    Create an axi-symmetric cylindrical domain with given dimensions and
    the given numbers of elements in radial and height direction.

    Parameters
    ----------
    R : float
        Radius.
    H : float
        Height.
    nelems : int or list of int
        The elements in r and z. Examples: 5 or [5,10].

    Returns
    -------
    DomainDefinition
        A ``DomainDefinition`` object with the ``domain``, ``geometry``, etc.
    """
    from nutils import function, topology, transform, sample, pointsseq, points

    nelems = [nelems,nelems] if type(nelems) != list else nelems
    domain2D, geom2D = mesh.rectilinear([linspace(0, R, nelems[0]+1), 
                                         linspace(0, H, nelems[1]+1)])
    r, z = geom2D
    rev = topology.StructuredLine('H', transform.Index(1, 0), 0, 1, bnames=('__axisymmetric_front', '__axisymmetric_back'))
    θ, = function.transforms_coords('H', rev.transforms) * (2 * numpy.pi)
    geom3D = numpy.stack([r * numpy.cos(θ), z, r * numpy.sin(θ)])
    rot = function.stack([function.scatter(function.trignormal(θ), 3, [0,2]), function.kronecker(1., 0, 3, 1)])
    axi = sample.Sample.new('H', (rev.transforms, rev.opposites),
        pointsseq.PointsSequence.from_iter([points.CoordsUniformPoints(numpy.zeros([1,1]), 1.)], 1)).integral
    domain2D = domain2D.withboundary(exterior="top,right,bottom")
    return DomainDefinition(domain2D, geom3D, geometry2D=geom2D,rot=rot, intθ=axi)