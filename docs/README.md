To generate the docs, you must first install all packages required to import the NSCH module, as well as:

-  sudo apt-get install python3-sphinx
-  python3 -m pip install sphinx-rtd-theme
-  python3 -m pip install m2r2
-  python3 -m pip install sphinx-gallery

Then, while in the `docs` directory, simply run:
- bash generate_docs