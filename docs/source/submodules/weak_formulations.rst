
Weak formulations
=================

.. automodule:: NSCH.weak_formulations
   :members:
   :undoc-members:
   :show-inheritance: