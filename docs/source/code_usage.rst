Usage  and extensibility
========================

There are three types of usage of the NSCH code framework, ranging in the extend to which the user is interested in extending the framework.


Basic usage
-----------

Simply running a NS/CH computation requires a small script that goes through the following steps:


1. Create a ``ProblemDefinition`` object and set the parameters. See :py:mod:`NSCH.problem\_definition` and its source code for an overview of the parameters that may be set.
2. Create a domain and specify boundary conditions. See :py:mod:`NSCH.domain_definitions` for the different predefined options.
3. Specify the initial condition. See :py:mod:`NSCH.initial_conditions` for the different predefined options.
4. Create a ``TimeStepper`` object and pass it the above information. See :py:mod:`NSCH.time_stepper`.
5. Run the ``TimeStepper``'s :py:mod:`perform_time_step<NSCH.time_stepper.TimeStepper.perform_time_step>` method in a loop until its :py:mod:`stop_criterion_reached<NSCH.time_stepper.TimeStepper.stop_criterion_reached>` method returns ``True``.

A full example implementation of the above steps may be viewed in the example of a :ref:`curved fluid front<Basic example of a fluid front>` 
or the example of :ref:`merging droplets<Basic example with merging droplets>`.




Predefined extensibility
------------------------

If the core implementation is not sufficient, then some options are supported for mild extensions in functionality. 
First of all, the user may specify a custom domain and initial condition by writing their own version of the functions in :py:mod:`NSCH.domain_definitions` and :py:mod:`NSCH.initial_conditions`.

Secondly, a custom `action` can be added to the ``TimeStepper``'s action list by passing a function to the ``user_operation`` parameter in the ``model`` parameterset.
This function is called at the end of each solve step in each refinement level of each timestep. 
he function is passed the ``TimeStepper`` instance, the ``SolutionSet``, and all ``kwargs`` that are passed along among actions in the action stack.

To alter the partial differential equation itself, the ``Problem_definition`` provides the parameter ``manual`` in the ``model`` set. 
This must be set to a list of three entries, namely the residual getter function, the mass getter function, and the list of fields with degrees of freedom.
This approach is not entirly opportune; most likely, adoption of a completely new weak formulation would call for the creation of a seperate module (see below).

All three of these extensions are illustrated in the example of a :ref:`heat equation<Manual example of heat equation>`.




Writing a seperate module
-------------------------

If the features that need to be implemented are more involved, then the user may create their own module. The ``TimeStepper`` class can be inherited from to add
new actions, and the ``Problem_definition`` class can be inherited from to introduce new parameters. The ``Problem_defition`` class also has methods that may be overwritten to
define aditional fields, new components to the weak formulation, and aditional model expressions. These methods are collected at the bottom of the class definition. Most of this
extensibility is illustrated in the ``immersed.py`` file, which acts as an example "module" in the main `NSCH` codebase. In that module, you'll find:
- A new ``ProblemDefinition`` class, called ``ProblemDefinitionImmersed`` with additional stabilization parameters.
- A new ``DomainDefinition`` class which keeps reference to cut and uncut domains, ghost facets, boundary conditions on the immersed boundary. These new components to 
the class affect the `refinement` and `copy` methods, which thus have to be overwritten.
- New components to the weak formulation (penalty, Nitsche and ghost-penalty). These are introduced to the framework by overwriting the ``ProblemDefinition``s ``get_residual`` method.

Use of this module is illustrated in the :ref:`immersed<Example of the immersed module>` example.