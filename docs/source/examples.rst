Examples
========

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   ./auto_examples/fluid_front.rst
   ./auto_examples/drop_merge.rst
   ./auto_examples/heat_equation.rst
   ./auto_examples/immersed.rst
   ./auto_examples/oscillating_droplet.rst
   ./auto_examples/Rayleigh_breakup.rst
