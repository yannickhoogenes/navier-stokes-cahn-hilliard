Code design
===========

The code framework consists of three core components:

The parameter definition
~~~~~~~~~~~~~~~~~~~~~~~~

NSCH simulations typically involve a plethora of parameters. 
To keep this managable, the :py:mod:`ProblemDefinition<NSCH.problem\_definition.ProblemDefinition>` class house all the information that the user can specify to run a NS/CH simulation. 
This includes the specific model, the finite element approximation settings, the time-integration settings and the post-processing settings.
All different parameters are not documented rigorously, as they are continuously updated, but they can be inferred directly from the source code.


The time stepper
~~~~~~~~~~~~~~~~

The :py:mod:`TimeStepper<NSCH.time\_stepper.TimeStepper>` class is the main work horse. 
It includes all the implementation for all the different simulation components as "actions". 
Depending on the parameters set in the parameter definition, the ``TimeStepper`` object will stack these actions on top of eachother, and execute the stack in each time step. 
Modularity and extensibility of the code framework then hinges on the proper design of these actions. 
They must be sufficiently stand-alone, and require/give clearly defined input/output that may be picked up by other actions in the stack.


The solution set
~~~~~~~~~~~~~~~~

Objects of the :py:mod:`SolutionSet<NSCH.solution_set.SolutionSet>` class hold the Nutils ``NameSpace`` with all the relevant fields and variables, the domain and the geometry. 
For each mesh refinement and each new timestep, a new ``SolutionSet`` object is created. All data is encapsulated to the point that a complete simulation restart only
requires storage and reloading of the last relevant series of these ``SolutionSet``.


