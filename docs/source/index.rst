.. Navier-Stokes Cahn-Hilliard documentation master file, created by
   sphinx-quickstart on Wed Dec 22 09:51:13 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. mdinclude:: ../../README.md

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   code_description
   code_usage
   examples
   NSCH


