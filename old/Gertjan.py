from nutils import mesh, function, evaluable, solver, cli, topology, export, unit, sample
from nutils.expression_v2 import Namespace
from matplotlib import collections, patches, colors
import numpy, itertools, typing
import treelog as log


def makeplots(domain, cut, iface, ns, contactradius, overlay):

  solid = (domain['solid']*cut).sample('bezier', 5)
  fluid = (domain['fluid']*cut).sample('bezier', 5)
  iface = (iface*cut).sample('bezier', 5)

  r0, r, y, ur, uy, p, λ = iface.eval([ns.x0[0], ns.x[0], ns.x[1], ns.d[0], ns.d[1], ns.pres, ns.λ])

  if overlay:
    overlaydata = expdata(overlay)
    with export.mplfigure('surface.jpg') as fig:
      ax = fig.add_subplot(111, xlim=(0,numpy.max(r)), xlabel='radius [μm]', ylabel='surface height [μm]')
      ax.plot(*overlaydata, '.', label='measurements', zorder=-1)
      ax.add_collection(collections.LineCollection(numpy.array([r, y]).T[iface.tri]/unit('μm'), colors='k'))
      ax.legend(frameon=False, loc=4)
      ax.grid(True)
      ax.autoscale(enable=True, axis='y')

  with export.mplfigure('iface-pressure.jpg') as fig:
    ax = fig.add_subplot(111, xlim=(0,numpy.max(r)), xlabel='radius [μm]', ylabel='pressure [kPa]')
    ax.add_collection(collections.LineCollection(numpy.array([r/unit('μm'), p/unit('kPa')]).T[iface.tri], colors='k'))
    ax.grid(True)
    ax.autoscale(enable=True, axis='y')

  xlim = -numpy.max(r0), numpy.max(r0)
  with export.mplfigure('stretch.jpg') as fig:
    ax = fig.add_subplot(311, xlim=xlim, ylabel='ur [μm]')
    ax.add_collection(collections.LineCollection(numpy.array([r0/unit('μm'), ur/unit('μm')]).T[iface.tri], colors='k'))
    ax.add_collection(collections.LineCollection(numpy.array([-r0/unit('μm'), -ur/unit('μm')]).T[iface.tri], colors='k'))
    ax.grid(True)
    ax.autoscale(enable=True, axis='y')
    ax = fig.add_subplot(312, xlim=xlim, ylabel='uy [μm]')
    ax.add_collection(collections.LineCollection(numpy.array([r0/unit('μm'), uy/unit('μm')]).T[iface.tri], colors='k'))
    ax.add_collection(collections.LineCollection(numpy.array([-r0/unit('μm'), uy/unit('μm')]).T[iface.tri], colors='k'))
    ax.grid(True)
    ax.autoscale(enable=True, axis='y')
    ax = fig.add_subplot(313, xlim=xlim, xlabel='r [μm]', ylabel='λ')
    ax.add_collection(collections.LineCollection(numpy.array([r0/unit('μm'), λ]).T[iface.tri], colors='k'))
    ax.add_collection(collections.LineCollection(numpy.array([-r0/unit('μm'), λ]).T[iface.tri], colors='k'))
    ax.grid(True)
    ax.autoscale(enable=True, axis='y')

  xy_solid, ΔJ = solid.eval([ns.x[:2], ns.ΔJ])
  xy_fluid, φ, η, pres = fluid.eval([ns.x[:2], ns.φ, ns.η, ns.pres])
  nan = numpy.isnan(pres)
  pres[nan] = 0 # matplotlib hates nans
  Jnan = numpy.isnan(ΔJ)
  ΔJ[Jnan] = 0 # matplotlib hates nans

  with export.mplfigure('jacobian.jpg') as fig:
    ax = fig.add_subplot(111, aspect='equal', xlim=(0,numpy.max(r)), xlabel='radius [μm]', ylabel='height [μm]')
    im = ax.tripcolor(*xy_solid.T/unit('μm'), solid.tri, ΔJ**2, mask=Jnan[solid.tri].any(axis=1), shading='gouraud', cmap='jet', norm=colors.LogNorm())
    ax.add_collection(collections.LineCollection(xy_solid[solid.hull]/unit('μm'), colors='k', linewidths=.1))
    fig.colorbar(im, orientation='horizontal', label='squared jacobian error')
    ax.autoscale(enable=True, axis='both', tight=True)

  for zoom in False, True:
    xlim, ylim = (numpy.array([contactradius,0])/unit('μm') + [[-30,-20],[30,40]] if zoom
             else numpy.array([xy_solid.min(axis=0), xy_fluid.max(axis=0)])/unit('μm')).T
    prefix = 'zoom' if zoom else ''

    with export.mplfigure(prefix+'pressure.jpg') as fig:
      ax = fig.add_subplot(111, aspect='equal', xlabel='radius [μm]', ylabel='height [μm]', xlim=xlim, ylim=ylim)
      im = ax.tripcolor(*xy_fluid.T/unit('μm'), fluid.tri, pres/unit('kPa'), mask=nan[fluid.tri].any(axis=1), shading='gouraud', cmap='jet')
      ax.add_collection(collections.LineCollection(xy_fluid[fluid.hull]/unit('μm'), colors='k', linewidths=.1))
      ax.add_collection(collections.LineCollection(xy_solid[solid.hull]/unit('μm'), colors='k', linewidths=.1))
      fig.colorbar(im, orientation='vertical', label='pressure [kPa]')
      ax.add_collection(collections.LineCollection(numpy.array([r, y]).T[iface.tri]/unit('μm'), colors='k'))

    with export.mplfigure(prefix+'phase.jpg') as fig:
      ax = fig.add_subplot(111, aspect='equal', xlabel='radius [μm]', ylabel='height [μm]', xlim=xlim, ylim=ylim)
      im = ax.tripcolor(*xy_fluid.T/unit('μm'), fluid.tri, φ, shading='gouraud', cmap='coolwarm')
      ax.add_collection(collections.LineCollection(xy_fluid[fluid.hull]/unit('μm'), colors='k', linewidths=.1))
      ax.add_collection(collections.LineCollection(xy_solid[solid.hull]/unit('μm'), colors='k', linewidths=.1))
      fig.colorbar(im, orientation='vertical', label='phase')
      im.set_clim(-1, 1)
      ax.add_collection(collections.LineCollection(numpy.array([r, y]).T[iface.tri]/unit('μm'), colors='k'))

    with export.mplfigure(prefix+'chempot.jpg') as fig:
      ax = fig.add_subplot(111, aspect='equal', xlabel='radius [μm]', ylabel='height [μm]', xlim=xlim, ylim=ylim)
      im = ax.tripcolor(*xy_fluid.T/unit('μm'), fluid.tri, η, shading='gouraud', cmap='coolwarm')
      ax.add_collection(collections.LineCollection(xy_fluid[fluid.hull]/unit('μm'), colors='k', linewidths=.1))
      ax.add_collection(collections.LineCollection(xy_solid[solid.hull]/unit('μm'), colors='k', linewidths=.1))
      fig.colorbar(im, orientation='vertical', label='chemical potential')
      ax.add_collection(collections.LineCollection(numpy.array([r, y]).T[iface.tri]/unit('μm'), colors='k'))


def expdata(testcase):
  import json
  wpd = json.load(open('wpd_plot_data.json'))
  for data in wpd['wpd']['dataSeries']:
    if data['name'] == testcase:
      return numpy.array([point['value'] for point in data['data']]).T


def _sum_of_squares(data):
  from nutils import sparse, numeric
  indices, values, shape = sparse.extract(data)
  return numeric.accumulate(values**2, indices, shape)


unit = unit.create(m=1e6, s=1e6, g=1e12, N='kg*m/s2', Pa='N/m2', L='dm3')

def main(melems:int, welems:int, timestep:unit['μs'], lqddensity:unit['kg/L'],
         lqdviscosity:unit['Pa*s'], airdensity:unit['kg/L'],
         airviscosity:unit['Pa*s'], soliddensity:unit['kg/L'],
         youngsmodulus:unit['kPa'], thickness:unit['μm'], contactradius:unit['μm'],
         mobility:unit['μL*s/kg'], epsilon:unit['μm'],
         surftens_fluid:unit['mN/m'], surftensinner:unit['mN/m'],
         surftensouter:unit['mN/m'], tol:float, degree:int, endtime:unit['ms'],
         adaptive:typing.Dict[str,float], nrefine:int, errormeasure2d:bool,
         pspoisson:float, skewsym:bool, arrhenius:bool, clipping:bool,
         volumeratio:float, c0:float, c1:float, revolved:bool, steady:bool,
         overlay:str):
  '''
  Deformation of droplet on flexible substrate.

  .. arguments::

     melems [1]
       Number of elements along solid thickness.
     welems [10]
       Number of elements along solid radius, fluid height.
     timestep [100μs]
       Time step.
     lqddensity [1kg/L]
       Liquid density.
     lqdviscosity [1Pa*s]
       Liquid viscosity.
     airdensity [1kg/L]
       Air density.
     airviscosity [1Pa*s]
       Air viscosity.
     soliddensity [1kg/L]
       Solid density.
     youngsmodulus [3kPa]
       Young's modulus.
     thickness [50μm]
       Solid thickness.
     contactradius [176.7μm]
       Droplet radius.
     mobility [.01μL*s/kg]
     epsilon [1μm]
       Interface thickness.
     surftens_fluid [46mN/m]
       Liquid-air surface tension.
     surftensinner [36mN/m]
       Liquid-solid surface tension.
     surftensouter [31mN/m]
       Solid-air surface tension.
     tol [1e-5]
     degree [3]
       Spline degree.
     endtime [1ms]
       Stop simulation at or after this time, 0 for never.
     adaptive [φ=.9,d=.6]
       Fields and refinement fraction to consider for adaptivity
     nrefine [7]
     errormeasure2d [yes]
     pspoisson [.25]
       Poisson's ratio for pseudosolid.
     skewsym [yes]
       Use skew symmetry.
     arrhenius [yes]
       Use Arrhenius expression for viscosity instead of linear interpolation.
     clipping [yes]
       Use density clipping.
     volumeratio [1]
       Intrinsic liquid-ambient volume ratio: ρL mA / ρA mL.
     c0 [0]
       First Shuttleworth constant.
     c1 [0]
       Second Shuttleworth constant.
     revolved [no]
       Make revolved 3D domain.
     steady [no]
       Solve for steady state.
     overlay []
       Overlay data.

  .. presets::

     r1
       contactradius=26.8μm
       overlay=r1

     r2
       contactradius=74.5μm
       overlay=r2

     r3
       contactradius=176.7μm
       overlay=r3

     r4
       contactradius=225.5μm
       overlay=r4

     TC1
       lqddensity=1.26kg/L
       lqdviscosity=1.412Pa*s
       airdensity=1.26kg/L
       airviscosity=1.412Pa*s
       soliddensity=1.26kg/L
       timestep=1ms
       nrefine=9
       melems=2
       welems=20

     TC2
       lqddensity=1.26kg/L
       lqdviscosity=1.412Pa*s
       airdensity=1g/L
       airviscosity=100mPa*s
       soliddensity=1kg/L
       timestep=10μs
       nrefine=9
       melems=2
       welems=20

     singledrop
       thickness=120μm
       melems=2
       welems=8
       surftens_fluid=41.66mN/m
       surftensinner=33.33mN/m
       surftensouter=50mN/m
       c0=1
       c1=1
       contactradius=240μm
       adaptive={φ=.9,d=.9}
       steady=yes
       pspoisson=.49
  '''

  contactangle = numpy.arccos((surftensouter-surftensinner) / surftens_fluid)
  R = contactradius / numpy.sin(contactangle)
  H = -R * numpy.cos(contactangle)

  log.user('initial contact radius: {:,.3f}μm'.format(contactradius/unit('μm')))
  log.user('initial contact angle: {:,.3f}deg'.format(contactangle*180/numpy.pi))
  log.user('initial droplet radius: {:,.3f}μm'.format(R/unit('μm')))
  log.user('initial droplet center: {:,.3f}μm'.format(H/unit('μm')))
  log.user('initial droplet volume: {:,.3f}μm3'.format(numpy.pi*(R+H)/6*(3*contactradius**2+(R+H)**2)/unit('μm3')))
  log.user('initial pascal pressure: {:,.3f}kPa'.format(2*surftens_fluid/R/unit('kPa')))

  domain0, indices = mesh.rectilinear([welems, melems+welems])
  geom2d = indices * (thickness/melems) - [0, thickness]
  log.info('created {0[0]}x{0[1]} element domain'.format(domain0.shape))
  domain0 = domain0.withsubdomain(solid=domain0[:,:melems], fluid=domain0[:,melems:])

  if revolved:
    rev, theta = mesh.line(1, bnames=['left', 'right'], space='θ')
    cut = rev.boundary['left']
    rot = function.stack([function.scatter(function.trignormal(theta), 3, [0,2]), function.kronecker(1., 0, 3, 1)])
  else:
    class Id: __rmul__ = lambda self, other: other # hack
    rev = cut = Id()
    rot = function.eye(2)

  nd = (domain0 * rev).ndims

  ns0 = Namespace()
  ns0.ε = function.Argument('ε', ())
  ns0.σ = surftens_fluid * (1.5/numpy.sqrt(2)) # N/m
  ns0.τliquid = surftensinner # N/m
  ns0.τair = surftensouter # N/m
  ns0.μ = youngsmodulus / 3 # Pa
  ns0.ρsolid = soliddensity # kg/m3
  ns0.ρliquid = lqddensity # kg/m3
  ns0.μliquid = lqdviscosity # Pa*s
  ns0.ρair = airdensity # kg/m3
  ns0.μair = airviscosity # Pa*s
  ns0.ρmean = '(ρliquid + ρair) / 2' # Kg/m3
  ns0.ρdiff = '(ρliquid - ρair) / 2' # Kg/m3
  ns0.μmean = '(μliquid + μair) / 2' # Pa*s
  ns0.μdiff = '(μliquid - μair) / 2' # Pa*s
  ns0.Λ = volumeratio # -
  ns0.M = mobility # m3*s/kg
  ns0.H = H # m
  ns0.R = R # m
  ns0.r, ns0.y = ns0.ry = geom2d
  ns0.x0 = geom2d @ rot
  ns0.define_for('x0', gradient='∇0', normal='n0', jacobians=('dV0',))
  ns0.xgrid_i = 'ry_i' if errormeasure2d else 'x0_i'
  ns0.νps = pspoisson
  ns0.dt = timestep # s
  ns0.δ = function.eye(nd)
  ns0.c0 = c0
  ns0.c1 = c1

  previous_domain = domain0
  previous_ns = ns0.copy_()
  previous_ns.u = function.zeros([nd])
  previous_ns.d = function.zeros([nd])
  previous_ns.v = function.zeros([nd])
  previous_ns.x_i = 'x0_i'
  previous_ns.define_for('x', gradient='∇', normal='n', jacobians=('dV', 'dS'))
  previous_ns.φ = 'tanh((R - sqrt(r^2 + (y - H)^2)) / ε sqrt(2))'
  previous_ns.ρfluid = 'ρmean + φ ρdiff'
  previous_ns.μfluid = 'μmean + φ μdiff'

  dofs = tuple('φηdqvp')
  if not steady:
    dofs += 'u',

  with log.iter.plain('timestep', itertools.count()) as timesteps:
   for istep in timesteps:

    totaldofhist = []
    domain = domain0

    with log.iter.fraction('adaptive', range(nrefine)) as counter:
     for irefine in counter:
      if irefine:
        log.info('refining {}/{} elements'.format(len(refine), len(domain)))
        domain = domain.refined_by(refine)

      creftopo = topology.DisjointUnionTopology([domain['solid'] & previous_domain['solid'], domain['fluid'] & previous_domain['fluid']], ['solid', 'fluid'])
      iface = domain['fluid'].boundary & ~domain['solid'].boundary

      ns = ns0.copy_()

      φbasis = ηbasis = domain['fluid'].basis('th-spline', degree=degree)
      pbasis = domain['fluid'].basis('th-spline', degree=degree-1)
      qbasis = domain['solid'].basis('th-spline', degree=degree-1)
      dbasis = ubasis = vbasis = domain.basis('th-spline', degree=degree, continuity=-2,
        knotmultiplicities=(None,None if degree <= 2 else tuple([degree+1]+[2]*(melems-1)+[degree]+[2]*(welems-1)+[degree+1])))

      ns.d = function.dotarg('d', dbasis, shape=[2]) @ rot # m (mesh displacement)
      ns.u = function.zeros([nd]) if steady else function.dotarg('u', dbasis, shape=[2]) @ rot # m/s (mesh velocity)
      ns.dv = function.dotarg('v', dbasis, shape=[2]) @ rot # m/s (relative fluid velocity)
      ns.p = function.dotarg('p', pbasis) # Pa
      ns.q = function.dotarg('q', qbasis) # Pa
      ns.φ = function.dotarg('φ', φbasis) # -
      ns.η = function.dotarg('η', ηbasis) # Pa

      ns.x_i = 'x0_i + d_i' # m
      ns.define_for('x', gradient='∇', normal='n', jacobians=('dV', 'dS'))
      ns.v_i = 'dv_i + u_i' # m/s (absolute fluid velocity)
      ns.φ0 = previous_ns.φ # -
      ns.ψ = 'φ0^3 φ - 3 φ0 φ + φ^2' # - double-well potential with linear derivative φ0^3 - 3 φ0 + 2 φ
      if clipping:
        ρ1 = airdensity # φ <= -1
        ρ2 = lqddensity # φ >= +1
        ρdiff = (ρ2-ρ1)/2
        ρmean = (ρ2+ρ1)/2
        a, b, c, d, e = function.partition(ns.φ * ρdiff, -ρmean, -ρ2/2, ρ2/2, ρmean)
        ns.ρfluid = (a+b) * .25*ρ1 + b * (ρdiff*ns.φ+ρmean)**2/ρ1 + c * (ρdiff*ns.φ+ρmean) - d * (ρdiff*ns.φ-ρmean)**2/ρ1 + (d+e) * (ρ2+.75*ρ1)
      else:
        ns.ρfluid = 'ρmean + φ ρdiff'
      if arrhenius:
        ns.μfluid = 'exp(((1 + φ) Λ log(μliquid) + (1 - φ) log(μair)) / ((1 + φ) Λ + (1 - φ)))'
      else: # linear
        ns.μfluid = 'μmean + φ μdiff'
      ns.εfluid_ij = '∇_j(v_i) + ∇_i(v_j) - (2 δ_ij / δ_nn) ∇_k(v_k)'
      ns.σfluid_ij = 'μfluid εfluid_ij - (σ ε) ∇_i(φ) ∇_j(φ) - p δ_ij'
      ns.pres = '(-σfluid_kk + ρfluid v_k dv_k) / δ_nn'
      ns.trac_i = '-σfluid_ij n_j'
      ns.F_ij = '∇0_j(x_i)'
      ns.C_ij = 'F_ki F_kj'
      ns.ΔJ = function.determinant(ns.F) - 1
      ns.λ = 'sqrt(C_ij (δ_ij - n0_i n0_j))'
      ns.εsolid_ij = '(C_ij - δ_ij) / 2'
      ns.τfluid = '.25 (τliquid - τair) (3 φ - φ^3) + .5 (τliquid + τair)' # N/m
      ns.shuttleworth = '1 - c0 log(λ) + c1 (λ - 1)'
      ns.eflkin = '.5 ρfluid v_k v_k' # Pa
      ns.eflmix = '.5 σ ε ∇_k(φ) ∇_k(φ) + ψ σ / ε' # Pa (ε=m)
      ns.eelkin = '.5 ρsolid u_k u_k' # Pa
      ns.eelpot = 'μ εsolid_kk + q ΔJ' # Pa
      ns.w_i = 'ρfluid dv_i - ρdiff M ∇_i(η)' # kg/m2/s
      ns.σps_ij = '∇_j(d_i) + ∇_k(d_k) δ_ij νps / (.5 - νps)' # m/s

      cons = solver.optimize(['d'], (domain['solid']*rev).integral('d_i d_i' @ ns, degree=degree*2), droptol=1e-14)
      isnan = numpy.isnan(cons['d'][:,0]).astype(float)

      ns.dtest = function.dotarg('dtest', dbasis, shape=[2]) @ rot
      ns.dftest = function.dotarg('dtest', dbasis * isnan, shape=[2]) @ rot
      ns.dstest = function.dotarg('dtest', dbasis * (1-isnan), shape=[2]) @ rot
      ns.vtest = function.dotarg('vtest', vbasis, shape=[2]) @ rot
      ns.vdstest = ns.vtest + ns.dstest
      ns.utest = function.dotarg('utest', ubasis, shape=[2]) @ rot
      ns.ptest = function.dotarg('ptest', pbasis)
      ns.ηtest = function.dotarg('ηtest', ηbasis)

      previous_ns.dtest = -ns.dtest
      previous_ns.vtest = -ns.vtest
      previous_ns.utest = -ns.utest

      X = (iface*rev).integral('τfluid shuttleworth dS' @ ns, degree=degree*2) # J (wall energy)
      W = (domain['solid']*rev).integral('eelpot dV0' @ ns, degree=degree*2) # J (solid energy)
      E = (creftopo['fluid']*rev).integral('(eflmix - (φ - φ0) η - .5 M ∇_k(η) ∇_k(η) dt) dV' @ ns, degree=degree*2) # J (mixture energy)
      T = (domain['fluid']*rev).integral('∇_j(vdstest_i) (σfluid_ij - v_i w_j) dV' @ ns, degree=degree*2) # J
      if skewsym: # add approximate skew-symmetrizer
        T += (domain['fluid']*rev).integral('.5 ∇_j(ρfluid vdstest_i v_i) v_j dV' @ ns, degree=degree*2)
        T -= (domain['fluid'].boundary['top,bottom']*rev).integral('.5 vdstest_i ρfluid v_i v_j n_j dS' @ ns, degree=degree*2)
      T += (domain['fluid']*rev).integral('∇_j(dftest_i) σps_ij dV0' @ ns, degree=degree*2)
      T -= (domain['fluid']*rev).integral('∇_k(ηtest) φ dv_k dt dV' @ ns, degree=degree*2)
      T += (domain['fluid']*rev).integral('ptest ∇_k(v_k) dV' @ ns, degree=degree*2)
      T += (domain*rev).integral('-utest_i u_i dV0' @ ns, degree=degree*2)
      if not steady:
        for topo, time_ns in (domain, ns), (creftopo, previous_ns):
          T += (topo['solid']*rev).integral('dtest_i ρsolid u_i dV0 / dt' @ time_ns, degree=degree*2)
          T += (topo['fluid']*rev).integral('vtest_k ρfluid v_k dV / dt' @ time_ns, degree=degree*2)
          T += (topo*rev).integral('utest_i d_i dV0 / dt' @ time_ns, degree=degree*2)

      res = dict(
        φ = (X + E).derivative('φ'),
        η = E.derivative('η') + T.derivative('ηtest'),
        d = (X + W).derivative('d') + T.derivative('dtest'),
        q = W.derivative('q'),
        v = T.derivative('vtest'),
        p = T.derivative('ptest'),
        u = T.derivative('utest'))

      log.user('degrees of freedom:', sum(res[d].size for d in dofs))

      ε = epsilon * 2**(nrefine-1-irefine)
      log.info('epsilon: {:.1f}μm'.format(ε/unit('μm')))

      sqr = (domain['fluid'].boundary['right,top']*rev).integral('(φ + 1)^2' @ ns, degree=degree*2)
      sqr += (domain['fluid'].boundary['left,right']*rev).integral('dv_0^2' @ ns, degree=degree*2)
      sqr += (domain['solid']*rev).integral('dv_i dv_i' @ ns, degree=degree*2)
      sqr += (domain.boundary['bottom']*rev).integral('d_i d_i + u_i u_i' @ ns, degree=degree*2)
      sqr += (domain.boundary['left,right']*rev).integral('d_0^2 + u_0^2' @ ns, degree=degree*2)
      sqr += (domain.boundary['top']*rev).integral('d_1^2 + u_1^2' @ ns, degree=degree*2)
      cons = solver.optimize(dofs, sqr, droptol=1e-14)

      if irefine:
        sqr = (domain['fluid']*rev).integral((ns.φ-nsc.φ)**2 + (ns.η-nsc.η)**2 + ((ns.dv-nsc.dv)**2).sum(0) + (ns.p-nsc.p)**2, degree=degree*2)
        sqr += (domain['solid']*rev).integral((ns.q-nsc.q)**2, degree=degree*2)
        sqr += (domain*rev).integral(((ns.d-nsc.d)**2).sum(0) + ((ns.u-nsc.u)**2).sum(0), degree=degree*2)
        lhs0 = solver.optimize(dofs, sqr, constrain=cons, arguments=dict(ε=ε))
      else:
        sqr = (domain['fluid']*rev).integral((ns.dv**2).sum(0) + ns.p**2, degree=degree*2)
        sqr += (domain['solid']*rev).integral(ns.q**2, degree=degree*2)
        sqr += (previous_domain*rev).integral(((ns.d-nsc.d)**2).sum(0) + ((ns.u-nsc.u)**2).sum(0), degree=degree*2) if istep \
          else (domain*rev).integral((ns.d**2).sum(0) + (ns.u**2).sum(0), degree=degree*2)
        initcons = solver.optimize(('v', 'p', 'q', 'd', 'u'), sqr, constrain=cons, droptol=1e-12)
        lhs0 = solver.optimize(('φ', 'η'), E, constrain=cons, arguments=dict(ε=ε, **initcons), tol=tol)

      lhs = solver.newton(dofs, [res[d] for d in dofs], constrain=cons, arguments=lhs0).solve(tol)
      nsc = ns.copy_(**lhs)

      criteria = dict(φ = E.derivative('φ'), d = (domain['solid']*rev).integral(dbasis * ns.ΔJ, degree=degree*2))
      refine = []
      for k, data in zip(criteria.keys(), evaluable.eval_sparse(tuple(criteria.values()), **lhs)):
        dof_errors = _sum_of_squares(data)
        dom = domain['fluid' if k in 'φηp' else 'solid' if k in 'q' else slice(None)]
        idofs = numpy.argsort(dof_errors)[::-1]
        idofs = idofs[:numpy.searchsorted(numpy.cumsum(dof_errors[idofs]), adaptive[k] * dof_errors.sum())]
        basis = locals()[k+'basis']
        refine.extend(dom.transforms[basis.get_support(idofs)])

        # vizualisation

        elem_errors = numpy.zeros(len(dom))
        for idof, dof_error in enumerate(dof_errors):
          elem_errors[basis.get_support(idof)] += dof_error
        bezier = (dom*cut).sample('bezier', 4)
        x, e = bezier.eval([ns.x0[:2], function.get(elem_errors, 0, dom.f_index)])
        with export.mplfigure('error'+k+'.jpg') as fig:
          ax = fig.add_subplot(111, aspect='equal')
          im = ax.tripcolor(x[:,0], x[:,1], bezier.tri, e, shading='gouraud', cmap='jet', norm=colors.LogNorm())
          fig.colorbar(im)
          for idof in idofs:
            corners = numpy.concatenate([x[bezier.getindex(ielem)] for ielem in basis.get_support(idof)], axis=0)
            xmin = corners.min(axis=0)
            xmax = corners.max(axis=0)
            ax.add_patch(patches.Ellipse((xmin+xmax)/2, *(xmax-xmin), linewidth=.5, edgecolor='w', facecolor='none'))
          ax.autoscale(enable=True, axis='both', tight=True)

    makeplots(domain, cut, iface, nsc, contactradius=contactradius, overlay=overlay)
    previous_domain = domain
    previous_ns = nsc

cli.run( main )
