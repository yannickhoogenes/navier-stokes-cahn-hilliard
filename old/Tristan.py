#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 10:23:59 2020

@author: s160154
"""

# Import of the Nutils package.
from nutils import *
import time
import math
from matplotlib import collections
import matplotlib
import pickle
import os.path
import itertools
import numpy
import treelog as log
import enum


class Precon(enum.Enum):

  full = None
  jacobi = 'Jacobi'
  gaussseidel = 'Gauss-Seidel'
  gaussseidelrev = 'reversed Gauss-Seidel'

  def __call__(self, n1, n2):
    if self is self.full:
      return 'direct'
    def user(matrix):
      assert matrix.shape == (n1+n2,)*2
      s1 = numpy.arange(n1+n2) < n1
      s2 = ~s1
      A11inv = matrix.submatrix(s1,s1).getprecon('direct')
      A22inv = matrix.submatrix(s2,s2).getprecon('direct')
      if self is self.jacobi:
        def precon(b):
          x1 = A11inv(b[:n1])
          x2 = A22inv(b[n1:])
          return numpy.hstack([x1, x2])
      elif self is self.gaussseidel:
        A21 = matrix.submatrix(s2,s1)
        def precon(b):
          x1 = A11inv(b[:n1])
          x2 = A22inv(b[n1:] - A21 @ x1)
          return numpy.hstack([x1, x2])
      else: # gaussseidelrev:
        A12 = matrix.submatrix(s1,s2)
        def precon(b):
          x2 = A22inv(b[n1:])
          x1 = A11inv(b[:n1] - A12 @ x2)
          return numpy.hstack([x1, x2])
      log.info('constructed {} preconditioner'.format(self.value))
      return precon
    return user

def _getsupp(basis, topo):
  index = evaluable.Tuple([indices[1] for indices, func in basis.prepare_eval(ndims=topo.ndims).blocks])
  supp = [[] for i in range(len(basis))]
  for ielem, trans in enumerate(topo.transforms):
    for indices in index.eval(_transforms=[trans]):
      for i in indices:
        supp[i].append(ielem)
  return supp


########################################################################################################################
#======================================================================================================================#
########################################################################################################################



def main(
 # Input parameters and their respective default values.
  maxtimestepsize: 'maximum time step size in s'                   = 2.5e-6,
  initialtimestepsize: 'initial time step size in s'               = 0e0,
  lqddensity:     'liquid density in kg m^{-3}'                    = 1e3,
  lqdviscosity:   'liquid viscosity in kg m^{-1} s^{-1}'           = 1e-3,
  airdensity:     'air density in kg m^{-3}'                       = 1,
  airviscosity:   'air viscosity in kg m^{-1} s^{-1}'              = 1e-4,
    # Mobility should be >= ε^2 / (16*viscosity) (Yue et al., 2011) [1].
  mobility:       'mobility coefficient in m^2 s^{-1}'             = 64e-12,
    # epsilon quick select for powers of 2:
    # 1.953125e-7 , 3.90625e-7 , 7.8125e-7 , 1.5625e-6 , 3.125e-6 , 6.25e-6 , 1.25e-5
  epsilon:        'interface thickness divided by 4'               = 2e-6,
  surftens_fluid: 'liquid-air surface tension in kg s^{-2}'        = 7.28e-2,
  surftensinner:  'liquid-solid surface tension in kg s^{-2}'      = 1e-1,
    # surftensouter quick select for contact angles:
    # 1e-1 , (1e-1)-(5.5e-2)*0.5*numpy.sqrt(2) , (1e-1)-(5.5e-2)*0 , (1e-1)+(5.5e-2)*0.5*numpy.sqrt(2)
  surftensouter:  'solid-air surface tension in kg s^{-2}'         = 1e-1,
  tol:            'residual norm tolerance for Newton convergence' = 1e-14,
  R0:             'Filament radius [m]'                            = 2e-5,
  L0:             'Domain height [m]'                              = 4.56471 * 2e-5,
  h0:             'Height of necking [m]'                          = 4.56471 * 2e-5,
  A0:             'Initial amplitude perturbation [m]'             = 1/100   * 2e-5,
  β:              'Lower limit phase field'                        = 0.005,
  int_degree:     'integration degree'                             = 6,
  xy_order:       'order of basis functions in rz plane'           = 3,
  nelems                                                           = 24 ,
  nrefine                                                          = 0,
  nrefinezero                                                      = 1,
    #nt_rep must be a multiple of 2, amount of repetitions of smaller timestep
  nt_rep                                                           = 8,
  plot_from                                                        = 0,
  alpha                                                            = 0.5,
  adaptivefields                                                   = 'φvd',
  fraction                                                         = 0.975,
  vareps                                                           = 0e0,
  vis_profile:    '0: linear, 1: logarithmic'                      = 1,
#  nu1inv:         'inverse of nu_1'                                = 1e-5,
  nu2inv:         'inverse of nu_2'                                = 0e0,#5e4,
  vprofile                                                         = 1,
  vmin                                                             = 0e0,
  max_iter0                                                        = 5, #5,
  max_iter1                                                        = 25, #10,
  double_well_type: '0: 4th order pnml, 1: log bounds'             = 0,
  import_IC                                                        = -1,
  make_plot                                                        = True,
  make_VTK                                                         = True,
  make_energy_plot                                                 = True,
  pickle_save                                                      = True,
  Lambda                                                           = -1e0,
  BE_only                                                          = 0,
  import_time                                                      = 0,
  precon: Precon = Precon.full,
  ):
  
  if initialtimestepsize == 0:
    initialtimestepsize = maxtimestepsize
  
  if import_IC == -1:
    import_time  = -initialtimestepsize
  
  timestepsize   = initialtimestepsize
  currentsimtime = import_time
  
  t_counter      = False
  t_rep          = []
  # Set default state of successful-timestep-flag as true.
  lastTimestepSuccess = True
  Timelist = [currentsimtime + timestepsize]
  numpy.save("Time.npy", Timelist)
  
  domain_pcb = time.perf_counter()
  
  # create domain + geom
  domain0, geom, Ambient, Symplane2, Symaxis, Symplane1, rdirection, θdirection, zdirection = createDomain(nelems, R0, L0)
  domain1 = domain0
  domain_prev_t = domain0
  
  domain_pce = time.perf_counter()
  log.debug('Construct domain/geom (perf counter):',domain_pce-domain_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
  energy_init_pcb = time.perf_counter()
  
  if make_energy_plot:
    energyInt1List = []
    energyInt2List = []
    energyExt1List = []
    energyExt2List = []
    energyExt3List = []
    energyExt4List = []
  
    energyMixtureList   = []
    energyInterfaceList = []
    energyWallList      = []
    energyKineticList   = []
    energyTotalList     = []
  
  # To save the equations of the previous timestep  
  ns_prev_t       = function.Namespace()
  # To save the equations of the last solution with values
  # updated for each irefine if nrefine>0  
  ns_prev_sol = function.Namespace()
  # To save the equations of the unrefined previous timestep  
  ns_prev_t_00    = function.Namespace()
  
  energy_init_pce = time.perf_counter()
  log.debug('energy initialization (perf counter):',energy_init_pce-energy_init_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
  for istep in log.iter.plain('timestep', itertools.count(start=import_IC+1)):
    log.info('Current istep is',istep)
    log.info('currentsimtime:',currentsimtime)
    log.info('timestepsize:',timestepsize)
    log.info('Next sim_time to be computed:', currentsimtime+timestepsize)
    timestep_pcb = time.perf_counter()
    
    # Pickle files should only be loaded when current time step is the one which requires pickle load AND pickle load
    #   is intended to be used.
    if istep == import_IC + 1 and import_IC >= 0:
      
      #PICKLE
      if pickle_save:
        file_directory          = os.path.dirname(os.path.realpath('__file__'))
        
        ns_pickle_path          = os.path.join(file_directory, 'pickle_files/')
        ns_pickle_name          = 'ns_backup_{}'.format(import_IC)
        ns_pickle_name_full     = os.path.join(ns_pickle_path, ns_pickle_name)
        ns_pickle_object        = open(ns_pickle_name_full,'rb')
        ns_prev_t               = pickle.load(ns_pickle_object)
        ns_prev_sol             = ns_prev_t
        ns_pickle_object.close()
        
        ns00_pickle_path        = os.path.join(file_directory, 'pickle_files/')
        ns00_pickle_name        = 'ns00_backup_{}'.format(import_IC)
        ns00_pickle_name_full   = os.path.join(ns00_pickle_path, ns00_pickle_name)
        ns00_pickle_object      = open(ns00_pickle_name_full,'rb')
        ns_prev_t_00            = pickle.load(ns00_pickle_object)
        ns00_pickle_object.close()
        
        domain_pickle_path      = os.path.join(file_directory, 'pickle_files/')
        domain_pickle_name      = 'domain_backup_{}'.format(import_IC)
        domain_pickle_name_full = os.path.join(domain_pickle_path, domain_pickle_name)
        domain_pickle_object    = open(domain_pickle_name_full,'rb')
        domain_prev_t           = pickle.load(domain_pickle_object)
        domain1                 = domain_prev_t
        domain_pickle_object.close()
    
    timestep_init_pcb = time.perf_counter()
    
    # ns and domainRefine are not defined when in time step 0 or when loading pickle file.
    if istep > 0 and lastTimestepSuccess:
      if istep != import_IC+1:
        ns_prev_sol = ns(**lhs)
        domain_prev_t = domainRefine
    domain = domain0
    
    # !!!
    ns_prev_t_save       = ns_prev_t
    ns_prev_sol_save     = ns_prev_sol
    domain_prev_t_save   = domain_prev_t
    domain1save          = domain1
    ns_prev_t_00_save    = ns_prev_t_00
    
    is_in_adap_fields = False
    for k in adaptivefields:
      if k == 'φ':
        is_in_adap_fields = True
    if is_in_adap_fields:
      errhist = {k: [] for k in adaptivefields}
    else:
      adaptivefields2 = 'φ'+adaptivefields
      errhist = {k: [] for k in adaptivefields2}
    
    
    timestep_init_pce = time.perf_counter()
    log.debug('timestep init (perf counter):',timestep_init_pce-timestep_init_pcb,'seconds on',
      time.strftime('%a %b %d %H:%M:%S %Y'))
    
    domainRefine, domain0, ns, lhs, ns_prev_t, domain1, newton_success, ns_prev_t_00 = refineRoutine(adaptivefields, nrefine, domain, nrefinezero,
      epsilon, mobility, Lambda, geom, istep, Ambient, Symplane2, Symaxis, Symplane1, rdirection,
      θdirection, zdirection, ns_prev_t, ns_prev_sol, domain1, domain_prev_t, timestepsize, lqddensity,
      lqdviscosity, airdensity, airviscosity, surftens_fluid, surftensinner, surftensouter, tol,
      int_degree, xy_order, plot_from, alpha, vareps, vis_profile, vprofile, vmin, max_iter0, max_iter1,
      double_well_type, BE_only, nu2inv, precon, make_plot, currentsimtime, errhist, fraction, make_energy_plot, energyInt1List, energyInt2List,
      energyExt1List, energyExt2List, energyExt3List, energyExt4List, energyMixtureList, energyInterfaceList,
      energyWallList, energyKineticList, energyTotalList, pickle_save, domain0, ns_prev_t_00, R0, L0, make_VTK, h0, A0, import_IC, Timelist, β)
    
    if not newton_success:
      # Set flag for successful timstep to False.
      lastTimestepSuccess = False
      # Start counting the timesteps upto nt_rep
      t_counter = True
      t_rep.append(0)
#      # Needed for next iteration...
#      import_IC = import_IC + 1
      
      # Halve the time step size.
      timestepsize = timestepsize / 2
      
      # Restore the saved namespaces
      ns_prev_t       = ns_prev_t_save
      ns_prev_sol     = ns_prev_sol_save
      domain_prev_t   = domain_prev_t_save
      domain1         = domain1save
      ns_prev_t_00    = ns_prev_t_00_save
      
    else:
      lastTimestepSuccess = True
      
      if istep > 0:
        Timelist = numpy.append(Timelist, currentsimtime + timestepsize)
        numpy.save("Time.npy", Timelist)
        
      currentsimtime = currentsimtime + timestepsize
      #If timestep repetition is activated, timestep size is increased after nt_rep timesteps
      # the second time the timestep is halved it is only done 2 times to keep original timestepping
      if t_counter:
        t_rep[-1]    +=1
        
        log.info('Time repetition:', t_rep)
        #Increase timestep size
        if len(t_rep) > 1:
          while len(t_rep) > (numpy.log(nt_rep)/numpy.log(2)) and t_rep[-1] >= 2:    
            timestepsize =  timestepsize*2
            t_rep        =  t_rep[:-1]
            t_rep[-1]    += 1  
          if len(t_rep) <= (numpy.log(nt_rep)/numpy.log(2)) and t_rep[-1] >= nt_rep:
            timestepsize =  timestepsize*2
            t_rep        =  t_rep[:-1]
        
        # Always nt_rep repetitions for the first halved timestep    
        if len(t_rep) == 1 and t_rep[-1] >= nt_rep:
          timestepsize = maxtimestepsize
          t_rep        = []
          t_counter    = False
          
    
    timestep_pce = time.perf_counter()
    log.debug('(SUM) timestep (+post-processing) (perf counter):',timestep_pce-timestep_pcb,'seconds on',
      time.strftime('%a %b %d %H:%M:%S %Y'))



########################################################################################################################
#======================================================================================================================#
########################################################################################################################



def refineRoutine(adaptivefields, nrefine, domain, nrefinezero, epsilon, mobility, Lambda, geom, istep, Ambient, Symplane2,
  Symaxis, Symplane1, rdirection, θdirection, zdirection, ns_prev_t, ns_prev_sol, domain1, domain_prev_t, timestepsize, lqddensity, lqdviscosity, airdensity,
  airviscosity, surftens_fluid, surftensinner, surftensouter, tol,
  int_degree, xy_order, plot_from, alpha, vareps, vis_profile, vprofile, vmin, max_iter0,
  max_iter1, double_well_type, BE_only, nu2inv, precon, make_plot, currentsimtime, errhist, fraction,
  make_energy_plot, energyInt1List, energyInt2List, energyExt1List, energyExt2List, energyExt3List, energyExt4List,
  energyMixtureList, energyInterfaceList, energyWallList, energyKineticList, energyTotalList, pickle_save, domain0, ns_prev_t_00, R0, L0, make_VTK, h0, A0, import_IC, Timelist, β):
  
  if nrefine==0:
    
    nrefine0_init_pcb = time.perf_counter()
    
    # No iteration over refinements needed
    irefine=0
    irefine2=0
    
    nrefine0_init_pce = time.perf_counter()
    log.debug('nrefine0 init (perf counter):',nrefine0_init_pce-nrefine0_init_pcb,'seconds on',
      time.strftime('%a %b %d %H:%M:%S %Y'))
    
    solve_routine_pcb = time.perf_counter()
    
    ns, lhs, newton_success, ns_prev_t_00 = solveEquations(Lambda, geom, istep, domain,
          Ambient, Symplane2, Symaxis, Symplane1, rdirection, θdirection, zdirection, irefine,
          irefine2, ns_prev_t, ns_prev_sol, domain1, domain_prev_t, timestepsize, lqddensity,
          lqdviscosity, airdensity, airviscosity, mobility, epsilon, surftens_fluid, 
          surftensinner, surftensouter, tol, int_degree, xy_order, nrefine, plot_from,
          alpha, vareps, vis_profile, vprofile, vmin, max_iter0, max_iter1, double_well_type,
          BE_only, nu2inv, precon, ns_prev_t_00, R0, L0, h0, A0, β)
    
    if not newton_success:
#      log.info('Do something.')
      domainRefine = domain
      return domainRefine, domain0, ns, lhs, ns_prev_t, domain1, newton_success, ns_prev_t_00
    
    solve_routine_pce = time.perf_counter()
    log.debug('(SUM) solve routine (perf counter):',solve_routine_pce-solve_routine_pcb,'seconds on',
      time.strftime('%a %b %d %H:%M:%S %Y'))
    
    solve_post_pcb = time.perf_counter()
    
    # Updating variables to return at end of function
    # ns_prev_sol is not used because nrefine=0, ns_prev_sol is updated in main
    ns_prev_t = ns(**lhs)
    domainRefine = domain
    
    irefine_pickle_pcb = time.perf_counter()
    
    #PICKLE
    if pickle_save:
      file_directory      = os.path.dirname(os.path.realpath('__file__'))
      
      ns_pickle_path      = os.path.join(file_directory, 'pickle_files/')
      ns_pickle_name      = 'ns_backup_{}'.format(istep)
      ns_pickle_name_full = os.path.join(ns_pickle_path, ns_pickle_name)
      ns_pickle_object    = open(ns_pickle_name_full,'wb')
      pickle.dump(ns(**lhs),ns_pickle_object)
      ns_pickle_object.close()
        
      ns00_pickle_path      = os.path.join(file_directory, 'pickle_files/')
      ns00_pickle_name      = 'ns00_backup_{}'.format(istep)
      ns00_pickle_name_full = os.path.join(ns00_pickle_path, ns00_pickle_name)
      ns00_pickle_object    = open(ns00_pickle_name_full,'wb')
      pickle.dump(ns_prev_t_00,ns00_pickle_object)
      ns00_pickle_object.close()
      
      domain_pickle_path      = os.path.join(file_directory, 'pickle_files/')
      domain_pickle_name      = 'domain_backup_{}'.format(istep)
      domain_pickle_name_full = os.path.join(domain_pickle_path, domain_pickle_name)
      domain_pickle_object    = open(domain_pickle_name_full,'wb')
      pickle.dump(domain,domain_pickle_object)
      domain_pickle_object.close()
    
    irefine_pickle_pce = time.perf_counter()
    log.debug('pickle (perf counter):',irefine_pickle_pce-irefine_pickle_pcb,'seconds on',
      time.strftime('%a %b %d %H:%M:%S %Y'))
    
    solve_post_pce = time.perf_counter()
    log.debug('solve post + pickle (perf counter):',solve_post_pce-solve_post_pcb,'seconds on',
      time.strftime('%a %b %d %H:%M:%S %Y'))
    
    if istep > plot_from-1:
      
      plot_pcb = time.perf_counter()
      if make_plot:
        makePlots(currentsimtime, timestepsize, domain, domain1, ns, lhs, istep, irefine, irefine2, nrefine)
    
    if make_VTK:
      makeVTK(currentsimtime, timestepsize, domainRefine, ns, lhs, istep, irefine, irefine2)
      if make_energy_plot:
        energyMixtureList, energyInterfaceList, energyWallList, energyKineticList, energyTotalList =\
          makeEnergyPlots(domainRefine, domain1, ns, lhs, istep, irefine, 
          irefine2, Ambient, Symplane2, Symaxis, Symplane1, energyInt1List, energyInt2List, 
          energyExt1List, energyExt2List, energyExt3List, energyExt4List, energyMixtureList, 
          energyInterfaceList, energyWallList, energyKineticList, energyTotalList, nrefine, import_IC, Timelist, timestepsize)
      
      plot_pce = time.perf_counter()
      log.debug('post-processing (perf counter):',plot_pce-plot_pcb,'seconds on',time.strftime('%a %b %d %H:%M:%S %Y'))
  
  else:
    # CASE: nrefine > 0
    with log.iter.plain('refinement', range(nrefine)) as refinements:
     for irefine in refinements:
      
      refine_pcb = time.perf_counter()
      
      with log.iter.plain('super', range(2)) as superrefinements:
       for irefine2 in superrefinements:
        
        sub_refine_pcb = time.perf_counter()
        
        irefine2_init_pcb = time.perf_counter()
        
        domainRefine = domain.refine(irefine2)
        
        if irefine >= nrefine - nrefinezero:
          epsilon1  = epsilon
          mobility1 = mobility
          lqddensity1   = lqddensity
          lqdviscosity1 = lqdviscosity
          airdensity1   = airdensity
          airviscosity1 = airviscosity
          
        else:
          epsilon1  = epsilon * ( 2 ** ( nrefine - irefine - nrefinezero ) )
          mobility1 = 10 * mobility * ( 8 ** ( nrefine - irefine - nrefinezero ) )  
          dens_mult_lqd = max(1, airdensity/lqddensity     * (nrefine - irefine - nrefinezero)/(nrefine - nrefinezero)) 
          visc_mult_lqd = max(1, airviscosity/lqdviscosity * (nrefine - irefine - nrefinezero)/(nrefine - nrefinezero))
          dens_mult_air = max(1, lqddensity/airdensity     * (nrefine - irefine - nrefinezero)/(nrefine - nrefinezero)) 
          visc_mult_air = max(1, lqdviscosity/airviscosity * (nrefine - irefine - nrefinezero)/(nrefine - nrefinezero))
          
          lqddensity1   = dens_mult_lqd * lqddensity
          lqdviscosity1 = visc_mult_lqd * lqdviscosity
          airdensity1   = dens_mult_air * airdensity
          airviscosity1 = visc_mult_air * airviscosity
          
        log.info('epsilon  =', epsilon1)
        log.info('mobility =', mobility1)
        log.info('dens lqd =', lqddensity1)
        log.info('visc lqd =', lqdviscosity1)
        log.info('dens air =', airdensity1)
        log.info('visc air =', airviscosity1)
    
        irefine2_init_pce = time.perf_counter()
        log.debug('irefine2 init (perf counter):',irefine2_init_pce-irefine2_init_pcb,'seconds on',
          time.strftime('%a %b %d %H:%M:%S %Y'))

        solve_routine_pcb = time.perf_counter()

        ns, lhs, newton_success, ns_prev_t_00 = solveEquations(Lambda, geom, istep, domainRefine,
          Ambient, Symplane2, Symaxis, Symplane1, rdirection, θdirection, zdirection, irefine,
          irefine2, ns_prev_t, ns_prev_sol, domain1, domain_prev_t, timestepsize, lqddensity1,
          lqdviscosity1, airdensity1, airviscosity1, mobility1, epsilon1, surftens_fluid, 
          surftensinner, surftensouter, tol, int_degree, xy_order, nrefine, plot_from,
          alpha, vareps, vis_profile, vprofile, vmin, max_iter0, max_iter1, double_well_type,
          BE_only, nu2inv, precon, ns_prev_t_00, R0, L0, h0, A0, β)
        
        if not newton_success:
#          log.info('Do something.')
          domainRefine = domain
          return domainRefine, domain0, ns, lhs, ns_prev_t, domain1, newton_success, ns_prev_t_00
        
        solve_routine_pce = time.perf_counter()
        log.debug('(SUM) solve routine (perf counter):',solve_routine_pce-solve_routine_pcb,'seconds on',
          time.strftime('%a %b %d %H:%M:%S %Y'))
        
        if istep > plot_from-1:
          
          plot_pcb = time.perf_counter()
          
          if make_plot:
            makePlots(currentsimtime, timestepsize, domainRefine, domain1, ns, lhs, istep, irefine, irefine2, nrefine)
          
          plot_pce = time.perf_counter()
          log.debug('post-processing (perf counter):',plot_pce-plot_pcb,'seconds on',
            time.strftime('%a %b %d %H:%M:%S %Y'))
        
        if make_VTK:
            makeVTK(currentsimtime, timestepsize, domainRefine, ns, lhs, istep, irefine, irefine2)
        
        
        solve_post_pcb = time.perf_counter()
        
        nscompare2 = function.Namespace()
        nscompare2 = ns(**lhs)
        
        solve_post_pce = time.perf_counter()
        log.debug('solve post (perf counter):',solve_post_pce-solve_post_pcb,'seconds on',
          time.strftime('%a %b %d %H:%M:%S %Y'))
        
        if irefine2 == 0:
          
          irefine2_is_0_post_pcb = time.perf_counter()
          
          nscompare1      = function.Namespace()
          nscompare1      = nscompare2
          ns_prev_sol     = ns(**lhs)
          domain_prev_t = domainRefine
          
          irefine2_is_0_post_pce = time.perf_counter()
          log.debug('irefine2 is 0 post step (+post-processing) (perf counter):',
            irefine2_is_0_post_pce-irefine2_is_0_post_pcb,'seconds on',time.strftime('%a %b %d %H:%M:%S %Y'))
          
          sub_refine_pce = time.perf_counter()
          log.debug('(SUM) sub-refine step (+post-processing) (perf counter):',sub_refine_pce-sub_refine_pcb,
            'seconds on',time.strftime('%a %b %d %H:%M:%S %Y'))
          
      with log.context('errors'):
          
          error_pcb = time.perf_counter()
          
          error_φ_plot_pcb = time.perf_counter()
          
          fluid = domainRefine[:,0:,:].boundary['bottom'].sample('bezier', 4)
          xφ, eφ, ev = fluid.eval([ns.x[:3], abs(nscompare2.φ - nscompare1.φ),
            abs(function.norm2(nscompare2.v)-function.norm2(nscompare1.v))])
          xφ2Dplot = xφ[:,(0,2)]
          
          with export.mplfigure('errors_φ_{}.png'.format(irefine,istep), figsize=(10.24,7.68)) as fig:
            ax = fig.add_subplot(111, aspect='equal')
            im = ax.tripcolor(xφ2Dplot[:,0], xφ2Dplot[:,1], fluid.tri, eφ, shading='gouraud', cmap='jet')
            ax.add_collection(collections.LineCollection(xφ2Dplot[fluid.hull], colors='k', linewidths=.1))
#              ax.tricontour(xφ[:,0], xφ[:,1], fluid.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5,
#                -0.375, -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='w',
#                linestyles='solid', alpha=1)
            ax.autoscale(enable=True, axis='both', tight=True)
            fig.colorbar(im)
          with export.mplfigure('errors_v_{}.png'.format(irefine,istep), figsize=(10.24,7.68)) as fig:
            ax = fig.add_subplot(111, aspect='equal')
            im = ax.tripcolor(xφ2Dplot[:,0], xφ2Dplot[:,1], fluid.tri, ev, shading='gouraud', cmap='jet')
            ax.add_collection(collections.LineCollection(xφ2Dplot[fluid.hull], colors='k', linewidths=.1))
#              ax.tricontour(xφ[:,0], xφ[:,1], fluid.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5,
#                -0.375, -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='w',
#                linestyles='solid', alpha=1)
            ax.autoscale(enable=True, axis='both', tight=True)
            fig.colorbar(im)
            
          refine = {}
          
          
          error_φ_plot_pce = time.perf_counter()
          log.debug('error φv plot (perf counter):',error_φ_plot_pce-error_φ_plot_pcb,'seconds on',
            time.strftime('%a %b %d %H:%M:%S %Y'))
          
          if istep > 0:
            for k in adaptivefields:
              
              error_pcb = time.perf_counter()
              
              if k == 'φ':
                errf = (nscompare2.φ - nscompare1.φ)**2 + function.sum((nscompare2.φ - nscompare1.φ).grad(ns.x)**2, 0)
              elif k == 'μ':
                errf = (nscompare2.μ - nscompare1.μ)**2 + function.sum((nscompare2.μ - nscompare1.μ).grad(ns.x)**2, 0)
              elif k == 'v':
                errf = function.sum((nscompare2.v - nscompare1.v)**2, 0)\
                  + function.sum((nscompare2.v - nscompare1.v).grad(ns.x)**2, (0,1))
              elif k == 'p':
                errf = (nscompare2.p - nscompare1.p)**2
              elif k == 'd':
                nscompare2.divu = 'v_k,k'
                errf = (nscompare2.divu)**2
              else:
                raise Exception
              dom    = domainRefine
              errors = dom.integrate_elementwise(errf * function.J(ns.x), degree=int_degree*2)
              supp   = _getsupp(getattr(ns, 'vbasis' if k == 'd' else k+'basis'), dom)

              N      = len(set(dom.transforms[ielem][:-1] for ielems in supp for ielem in ielems))
              log.info('total supporting elements for {}: {}'.format(k, N))
              
              error_pce = time.perf_counter()
              log.debug('compute errors (perf counter):',error_pce-error_pcb,'seconds on',
                time.strftime('%a %b %d %H:%M:%S %Y'))
              
              error_hist_plot_pcb = time.perf_counter()
              
              errhist[k].append((numpy.sqrt(errors.sum()),
                numpy.sqrt(sum(errors[ielems].sum() for ielems in supp if ielems))))
              err, bnd = zip(*errhist[k])
              with export.mplfigure('error_{}_hist_{}.png'.format(k, irefine, istep), figsize=(10.24,7.68)) as fig:
                ax = fig.add_subplot(111)
                ax.semilogy(err, 'bo', label='error')
                ax.semilogy(bnd, 'yo', label='bound')
                ax.semilogy(err, 'b-')
                ax.semilogy(bnd, 'y-')
                ax.grid()
                ax.legend()
              
              error_hist_plot_pce = time.perf_counter()
              log.debug('error hist plot (perf counter):',error_hist_plot_pce-error_hist_plot_pcb,'seconds on',
                time.strftime('%a %b %d %H:%M:%S %Y'))
              
              compute_errors_post_pcb = time.perf_counter()
              
              log.info("Total error:",sum(errors))
              
              refinedelems = set()
              for isupp in numpy.argsort([errors[ielems].sum() for ielems in supp])[::-1]:
                refinedelems |= set(supp[isupp])
                refined_error = sum(errors[ielem] for ielem in refinedelems)
                if refined_error > fraction * sum(errors):
                  break
              for trans in {dom.transforms[ielem][:-1] for ielem in refinedelems}:
                refine.setdefault(trans, []).append(k)
                
#              with export.mplfigure('error_bars.png', figsize=(10.24,7.68)) as fig:
#                ax = fig.add_subplot(111, aspect='equal')
#                test_values = [20, 34, 30, 35, 27]
#                test_x = numpy.arange(leng(test_values))
#                ax.bar(test_x, test_values)
              with export.mplfigure('error_bars_{}.png'.format(k), figsize=(10.24,7.68)) as fig:
                ax = fig.add_subplot(111, aspect='auto')
  #              test_values = [20, 34, 30, 35, 27]
                test_x = numpy.arange(len(errors))
                ax.bar(test_x, errors, width=1.0)
              errorsSorted = numpy.sort(errors)
  #            refinedelemsSize = numpy.arrange(len(refinedelems))
              errorsSize = len(errors)
              refinedelemsSize = len(refinedelems)
              log.info('errorsSize =', errorsSize)
  #            log.info('refinedElemsCount =', refinedElemsCount)
              log.info('refinedelemsSize =', refinedelemsSize)
              with export.mplfigure('sorted_error_bars_{}.png'.format(k), figsize=(10.24,7.68)) as fig:
                ax = fig.add_subplot(111, aspect='auto')
  #              test_values = [20, 34, 30, 35, 27]
                test_x = numpy.arange(len(errors))
                ax.bar(test_x, errorsSorted, width=1.0)
                ax.axvline(x=errorsSize-0.5-refinedelemsSize, ymin=0, ymax=1, color='r')
              with export.mplfigure('sorted_error_bars_logy_{}.png'.format(k), figsize=(10.24,7.68)) as fig:
                ax = fig.add_subplot(111, aspect='auto')
                ax.set_yscale('log')
  #              test_values = [20, 34, 30, 35, 27]
                test_x = numpy.arange(len(errors))
                ax.bar(test_x, errorsSorted, width=1.0)
                ax.axvline(x=errorsSize-0.5-refinedelemsSize, ymin=0, ymax=1, color='r')
              
              compute_errors_post_pce = time.perf_counter()
              log.debug('compute errors post (perf counter):',compute_errors_post_pce-compute_errors_post_pcb,
                'seconds on',time.strftime('%a %b %d %H:%M:%S %Y'))
            
            
            refinements_plot_pcb = time.perf_counter()
            
            log.info('refining {}/{} elements'.format(len(refine), len(domain)))
#             bezier = domain[:,0:,:].boundary['bottom'].sample('bezier', 2)
#             x = bezier.eval(geom)
#             x2Dplot = x[:,(0,2)]
#             phi = bezier.eval(nscompare1.φ)
#             geom2Dplot = geom[(0,2),]
#             with export.mplfigure('refinements_{}.png'.format(irefine,istep), figsize=(10.24,7.68)) as fig:
#               ax = fig.add_subplot(111, aspect='equal')
#               ax.add_collection(collections.LineCollection(x2Dplot[bezier.hull], colors='k', linewidths=.1))
# #                ax.tricontour(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5,
# #                  -0.375, -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='silver',
# #                  linestyles='dashed', alpha=1)
#               ax.tricontour(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5,
#                 -0.375, -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='gray',
#                 linestyles='dotted', alpha=1)
#               ax.autoscale(enable=True, axis='both', tight=True)
#               for trans in refine:
#                 center, = geom2Dplot.eval(_transforms=[trans], _points=numpy.array([[.5,.5]]))
#                 ax.annotate(''.join(refine[trans]), xy=center, horizontalalignment='center',
#                   verticalalignment='center')
            
            refinements_plot_pce = time.perf_counter()
            log.debug('refinements plot (perf counter):',refinements_plot_pce-refinements_plot_pcb,'seconds on',
              time.strftime('%a %b %d %H:%M:%S %Y'))
            
          else:
            
            error_pcb = time.perf_counter()
              
            errf = (nscompare2.φ - nscompare1.φ)**2 + function.sum((nscompare2.φ - nscompare1.φ).grad(ns.x)**2, 0)
            dom = domainRefine
            errors = dom.integrate_elementwise(errf * function.J(ns.x), degree=int_degree*2)
            supp = _getsupp(getattr(ns, 'φbasis'), dom)
            N = len(set(dom.transforms[ielem][:-1] for ielems in supp for ielem in ielems))
            log.info('total supporting elements for φ: {}'.format(N))
            error_pce = time.perf_counter()
            log.debug('compute errors (perf counter):',error_pce-error_pcb,'seconds on',
              time.strftime('%a %b %d %H:%M:%S %Y'))
            
            error_hist_plot_pcb = time.perf_counter()
            
            errhist['φ'].append((numpy.sqrt(errors.sum()),
              numpy.sqrt(sum(errors[ielems].sum() for ielems in supp if ielems))))
            err, bnd = zip(*errhist['φ'])
            with export.mplfigure('error_φ_hist_{}.png'.format(irefine, istep), figsize=(10.24,7.68)) as fig:
              ax = fig.add_subplot(111)
              ax.semilogy(err, 'bo', label='error')
              ax.semilogy(bnd, 'yo', label='bound')
              ax.semilogy(err, 'b-')
              ax.semilogy(bnd, 'y-')
              ax.grid()
              ax.legend()
              
            error_hist_plot_pce = time.perf_counter()
            log.debug('error hist plot (perf counter):',error_hist_plot_pce-error_hist_plot_pcb,'seconds on',
              time.strftime('%a %b %d %H:%M:%S %Y'))
            
            compute_errors_post_pcb = time.perf_counter()
            
            log.info("Total error:",sum(errors))
            
            refinedelems = set()
#            refinedElemsCount = 0
            for isupp in numpy.argsort([errors[ielems].sum() for ielems in supp])[::-1]:
#              refinedElemsCount += 1
              refinedelems |= set(supp[isupp])
              refined_error = sum(errors[ielem] for ielem in refinedelems)
              if refined_error > fraction * sum(errors):
                break
            for trans in {dom.transforms[ielem][:-1] for ielem in refinedelems}:
              refine.setdefault(trans, []).append('φ')
                
#            with export.mplfigure('error_bars.png', figsize=(10.24,7.68)) as fig:
#              ax = fig.add_subplot(111, aspect='equal')
#              test_values = [20, 34, 30, 35, 27]
#              test_x = numpy.arange(len(test_values))
#              ax.bar(test_x, test_values)
#            with export.mplfigure('error_bars.png', figsize=(10.24,7.68)) as fig:
#              ax = fig.add_subplot(111, aspect='equal')
##              test_values = [20, 34, 30, 35, 27]
#              test_x = numpy.arange(len(errors))
#              ax.bar(test_x, errors)
#            with export.mplfigure('error_bars.png', figsize=(10.24,7.68)) as fig:
#              ax = fig.add_subplot(111, aspect='auto')
#              test_values = [1000, 1002, 1001, 1003, 1005]
#              test_x = numpy.arange(len(test_values))
#              ax.bar(test_x, test_values)
            with export.mplfigure('error_bars_φ.png', figsize=(10.24,7.68)) as fig:
              ax = fig.add_subplot(111, aspect='auto')
#              test_values = [20, 34, 30, 35, 27]
              test_x = numpy.arange(len(errors))
              ax.bar(test_x, errors, width=1.0)
            errorsSorted = numpy.sort(errors)
#            refinedelemsSize = numpy.arrange(len(refinedelems))
            errorsSize = len(errors)
            refinedelemsSize = len(refinedelems)
            log.info('errorsSize =', errorsSize)
#            log.info('refinedElemsCount =', refinedElemsCount)
            log.info('refinedelemsSize =', refinedelemsSize)
            with export.mplfigure('sorted_error_bars_φ.png', figsize=(10.24,7.68)) as fig:
              ax = fig.add_subplot(111, aspect='auto')
#              test_values = [20, 34, 30, 35, 27]
              test_x = numpy.arange(len(errors))
              ax.bar(test_x, errorsSorted, width=1.0)
#              ax.axvline(x=errorsSize-0.5-refinedElemsCount, ymin=0, ymax=1, color='r')
              ax.axvline(x=errorsSize-0.5-refinedelemsSize, ymin=0, ymax=1, color='r')
            with export.mplfigure('sorted_error_bars_logy_φ.png', figsize=(10.24,7.68)) as fig:
              ax = fig.add_subplot(111, aspect='auto')
              ax.set_yscale('log')
#              test_values = [20, 34, 30, 35, 27]
              test_x = numpy.arange(errorsSize)
              ax.bar(test_x, errorsSorted, width=1.0)
#              ax.axvline(x=len(basis)-0.5, ymin=0, ymax=1, color='r')
#              ax.axvline(x=len(basis)-0.5-refinedElemsCount, ymin=0, ymax=1, color='r')
#              ax.axvline(x=errorsSize-0.5, ymin=0, ymax=1, color='r')
#              ax.axvline(x=errorsSize-0.5-refinedElemsCount, ymin=0, ymax=1, color='r')
              ax.axvline(x=errorsSize-0.5-refinedelemsSize, ymin=0, ymax=1, color='r')
            
            compute_errors_post_pce = time.perf_counter()
            log.debug('compute errors post (perf counter):',compute_errors_post_pce-compute_errors_post_pcb,
              'seconds on',time.strftime('%a %b %d %H:%M:%S %Y'))
            
            refinements_plot_pcb = time.perf_counter()
            log.info('refining {}/{} elements'.format(len(refine), len(domain)))
#             bezier = domain[:,0:,:].boundary['bottom'].sample('bezier', 2)
#             x = bezier.eval(geom)
#             x2Dplot = x[:,(0,2)]
#             geom2Dplot = geom[(0,2),]
#             phi = bezier.eval(nscompare1.φ)
#             with export.mplfigure('refinements_{}.png'.format(irefine,istep), figsize=(10.24,7.68)) as fig:
#               ax = fig.add_subplot(111, aspect='equal')
#               ax.add_collection(collections.LineCollection(x2Dplot[bezier.hull], colors='k', linewidths=.1))
# #                ax.tricontour(x[:,0], x[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5,
# #                  -0.375, -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='silver',
# #                linestyles='dashed', alpha=1)
#               ax.tricontour(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5,
#                 -0.375, -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='gray',
#                 linestyles='dotted', alpha=1)
#               ax.autoscale(enable=True, axis='both', tight=True)
#               for trans in refine:
#                 center, = geom2Dplot.eval(_transforms=[trans], _points=numpy.array([[.5,.5]]))
#                 ax.annotate(''.join(refine[trans]), xy=center, horizontalalignment='center',
#                   verticalalignment='center')
            
            refinements_plot_pce = time.perf_counter()
            log.debug('refinements plot (perf counter):',refinements_plot_pce-refinements_plot_pcb,'seconds on',
              time.strftime('%a %b %d %H:%M:%S %Y'))
            
          domain_refinement_pcb = time.perf_counter()
          
          domain = domain.refined_by(refine)
          
          domain_refinement_pce = time.perf_counter()
          log.debug('domain refinement (perf counter):',domain_refinement_pce-domain_refinement_pcb,'seconds on',
            time.strftime('%a %b %d %H:%M:%S %Y'))
          
          error_pce = time.perf_counter()
          log.debug('(SUM) error comp. (perf counter):',error_pce-error_pcb,'seconds on',
            time.strftime('%a %b %d %H:%M:%S %Y'))
          
          sub_refine_pce = time.perf_counter()
          log.debug('(SUM) sub-refine step (+post-processing) (perf counter):',sub_refine_pce-sub_refine_pcb,
            'seconds on',time.strftime('%a %b %d %H:%M:%S %Y'))
      
      if irefine < nrefine-1:
        # CASE: irefine < nrefine - 1
        
        irefine_less_post_pcb = time.perf_counter()
        
        #Updating values for next iteration
        ns_prev_sol   = ns(**lhs)
        domain_prev_t = domainRefine
        
        irefine_less_post_pce = time.perf_counter()
        log.debug('irefine less post (perf counter):',irefine_less_post_pce-irefine_less_post_pcb,'seconds on',
          time.strftime('%a %b %d %H:%M:%S %Y'))
        
      else:
        # CASE: irefine == nrefine - 1
        
        if istep > plot_from-1:
          
          plot_pcb = time.perf_counter()
          
          if make_energy_plot:
            energyInt1List, energyInt2List, energyExt1List, energyExt2List, energyExt3List, energyExt4List =\
              makeEnergyPlots(domainRefine, domain1, ns, lhs, istep, irefine, irefine2, Ambient, Symplane2, Symaxis, Symplane1,
              energyInt1List, energyInt2List, energyExt1List, energyExt2List, energyExt3List,
              energyExt4List, energyMixtureList, energyInterfaceList, energyWallList, energyKineticList,
              energyTotalList, nrefine, import_IC, Timelist, timestepsize)
          
          plot_pce = time.perf_counter()
          log.debug('post-processing (perf counter):',plot_pce-plot_pcb,'seconds on',
            time.strftime('%a %b %d %H:%M:%S %Y'))
        
        irefine_equal_post_pcb = time.perf_counter()
        
        # Updating variable to return at function end
        # ns_prev_sol will be updated in main
        domain1 = domainRefine
        ns_prev_t = ns(**lhs)
        
        irefine_pickle_pcb = time.perf_counter()
        
        if pickle_save:
          #PICKLE
          file_directory      = os.path.dirname(os.path.realpath('__file__'))
          
          ns_pickle_path      = os.path.join(file_directory, 'pickle_files/')
          ns_pickle_name      = 'ns_backup_{}'.format(istep)
          ns_pickle_name_full = os.path.join(ns_pickle_path, ns_pickle_name)
          ns_pickle_object = open(ns_pickle_name_full,'wb')
          pickle.dump(ns(**lhs),ns_pickle_object)
          ns_pickle_object.close()
          
          ns00_pickle_path      = os.path.join(file_directory, 'pickle_files/')
          ns00_pickle_name      = 'ns00_backup_{}'.format(istep)
          ns00_pickle_name_full = os.path.join(ns00_pickle_path, ns00_pickle_name)
          ns00_pickle_object    = open(ns00_pickle_name_full,'wb')
          pickle.dump(ns_prev_t_00,ns00_pickle_object)
          ns00_pickle_object.close()
          
          domain_pickle_path      = os.path.join(file_directory, 'pickle_files/')
          domain_pickle_name      = 'domain_backup_{}'.format(istep)
          domain_pickle_name_full = os.path.join(domain_pickle_path, domain_pickle_name)
          domain_pickle_object    = open(domain_pickle_name_full,'wb')
          pickle.dump(domainRefine,domain_pickle_object)
          domain_pickle_object.close()
        
        irefine_pickle_pce = time.perf_counter()
        log.debug('irefine pickle (perf counter):',irefine_pickle_pce-irefine_pickle_pcb,'seconds on',
          time.strftime('%a %b %d %H:%M:%S %Y'))
        
        irefine_equal_post_pce = time.perf_counter()
        log.debug('irefine equal post + pickle (perf counter):',irefine_equal_post_pce-irefine_equal_post_pcb,
          'seconds on',time.strftime('%a %b %d %H:%M:%S %Y'))
          
      refine_pce = time.perf_counter()
      log.debug('(SUM) refine step (+post-processing) (perf counter):',refine_pce-refine_pcb,'seconds on',
        time.strftime('%a %b %d %H:%M:%S %Y'))

  return domainRefine, domain0, ns, lhs, ns_prev_t, domain1, newton_success, ns_prev_t_00



########################################################################################################################
#======================================================================================================================#
########################################################################################################################



def createDomain(nelems, R0, L0):

#    domain, geom = mesh.rectilinear([numpy.linspace(0, 0.0003, 3*nelems+1), numpy.linspace(0, 0.0001, nelems+1)])
#    domain, geom = mesh.rectilinear([numpy.linspace(0, 0.0001, nelems+1), numpy.linspace(0, 0.0001, nelems+1)])
#    domain, geom = mesh.rectilinear([numpy.linspace(0, 0.0001, 2*nelems+1), numpy.linspace(0, 0.00005, nelems+1)]
  # Create mesh (r, θ, z)  
  domain, (r, θ, z) = mesh.rectilinear([numpy.linspace(0, 3*R0, nelems+1), [0, 2*numpy.pi], 
         numpy.linspace(0, L0, int(numpy.ceil((L0)/(3*R0))*nelems+1))], periodic=[1], norefine=[1])
  rdirection        = function.stack([function.cos(θ), function.sin(θ), 0])
  θdirection        = function.stack([-function.sin(θ), function.cos(θ), 0])
  zdirection        = function.stack([0, 0, 1])
  # Create geometry (x,y,z) 
  geom              = r*rdirection + z*zdirection
  axes =  function.asarray([[function.cos(θ), function.sin(θ), 0], [0, 0, 1]]) # r, z -> x, y, z
  Symaxis  = 'left'
  Ambient = 'right'
  Symplane2 = 'back'
  Symplane1 = 'front'

  return domain, geom, Ambient, Symplane2, Symaxis, Symplane1, rdirection, θdirection, zdirection



########################################################################################################################
#======================================================================================================================#
########################################################################################################################
  


def ellipse(point):
  # Work in quadrant 1.
  px = abs(point[0])
#  px = function.abs
  py = abs(point[1])
  
  # Initial guess for parameter t corresponding to shortest point on ellipse.
  t = math.pi / 4
  
  # Axes of ellipse.
  a = 0.00002
  b = 0.00001
  # Equilibrium values for above a and b
#  a = math.sqrt(0.0000000002)
#  b = math.sqrt(0.0000000002)
  
  # Approximation loop; total number of loops should be dependent on rate of reduction of error.
  for i in range(0, 20):
    
    # Parametrization of ellipse in point t.
    x = a * function.cos(t)
    y = b * function.sin(t)
    
    # Parametrization of evolute in point t.
    ex = (a*a - b*b) * function.cos(t)**3 / a
    ey = (b*b - a*a) * function.sin(t)**3 / b
    
    # Distance of coordinates of ellipse and evolute for t.
    rx = x - ex
    ry = y - ey
    
    # Distance of coordinates between point and evolute for t.
    qx = px - ex
    qy = py - ey
    
    # Distance between ellipse and evolute for t.
    r = function.sqrt(rx**2 + ry**2)
    
    # Distance between point and evolute for t.
    q = function.sqrt(qx**2 + qy**2)
    
    # Approximate distance between current guess a and new guess a'.
    delta_c = r * function.arcsin((rx*qy - ry*qx)/(r*q))
    
    # Approximate distance in t coordinate.
    delta_t = delta_c / function.sqrt(a*a + b*b - x*x - y*y)
    
    # Add increment delta_t to t.
    t += delta_t
    
    # Enforce t between 0 and pi/2.
    t = function.min(math.pi/2, function.max(0, t))
    
    # Recompute parametrization of ellipse for the new t.
    x = a * function.cos(t)
    y = b * function.sin(t)
    
    # Reintroduce different quadrants.
    xy = [function.abs(x)*function.sign(point[0]), function.abs(y)*function.sign(point[1])]
    
    # Compute distance between point and ellipse.
    distance = function.piecewise(px, [a], function.sign(py - b * function.sqrt(1-(px/a)**2)) *\
      function.sqrt((point[0] - xy[0])**2 + (point[1] - xy[1])**2), function.sqrt((point[0] - xy[0])**2 +\
      (point[1] - xy[1])**2) )
    
  return distance

def filament(coordinates, R0, L0):
    
  r  = (coordinates[0]**2 + coordinates[1]**2)**0.5
  z  = coordinates[2]
  zc = L0 - R0

  distance = function.piecewise(z, [zc], r, (r**2 + (z-zc)**2)**0.5)
   
  return distance

def neck(z_input, R0, L0, h0, A0):
  z = z_input  
  Rφ0 =  R0 + A0*function.cos( z/h0 * numpy.pi )
  
  return Rφ0

########################################################################################################################
#======================================================================================================================#
########################################################################################################################



def solveEquations(Lambda, geom, istep, domain, Ambient, Symplane2, Symaxis, Symplane1,
    rdirection, θdirection, zdirection, irefine, irefine2, ns_prev_t_input, ns_prev_sol,
    domain1, domain_prev_t, timestepsize, lqddensity, lqdviscosity, airdensity, 
    airviscosity, mobility, epsilon, surftens_fluid, surftensinner, surftensouter,
    tol, int_degree, xy_order, nrefine, plot_from, alpha, vareps, vis_profile, 
    vprofile, vmin, max_iter0, max_iter1, double_well_type, BE_only, nu2inv, precon,
    ns_prev_t_00, R0, L0, h0, A0, β):

  
  namespace_init_pcb = time.perf_counter()
  
 # Define the namespace.
 # ns is the namespace of this timestep, ns.varprev contains previous solution,
 # ns.var00 contains unrefined previous timestep
  ns           = function.Namespace()
  #ns_prev_t contains the solution of the previous timestep on the finest grid
  ns_prev_t    = function.Namespace()
  # Storing the geometry in the namespace.
  ns.x         = geom
  ns_prev_t.x  = geom
  # Storing the input parameter values in the namespace.
  ns.σ         = surftens_fluid * (1.5/numpy.sqrt(2))
  ns.ε         = epsilon
  ns.σ1        = surftensinner
  ns.σ2        = surftensouter
  ns.ρ1        = lqddensity
  ns.vis1      = lqdviscosity
  ns.ρ2        = airdensity
  ns.vis2      = airviscosity
  ns.α         = '(ρ2 - ρ1) / (ρ2 + ρ1)'
  ns.lambdavar = Lambda
  ns.nsnu2inv  = nu2inv
  
  namespace_init_pce = time.perf_counter()
  log.debug('namespace init (perf counter):',namespace_init_pce - namespace_init_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
#======================================================================================================================#
  
 # Definition of basis functions.
  basis_pcb = time.perf_counter()
  
  # Define only once for saving computational cost.
  suppbasis = domain.basis('th-std', degree=(xy_order,1,xy_order))
  ns.φbasis = ns_prev_t.φold2basis = suppbasis
  ns.μbasis = suppbasis
  ns.pbasis = domain.basis('th-std', degree=(xy_order-1,1,xy_order-1))
  
  vrbasis, vθbasis, vzbasis = function.chain([domain.basis('th-std', degree=(xy_order,1,xy_order)),
                domain.basis('th-std', degree=(xy_order,1,xy_order)),domain.basis('th-std', degree=(xy_order,1,xy_order))])
  ns.vbasis = ns_prev_t.vold2basis = vrbasis[:,numpy.newaxis] * rdirection + vθbasis[:,numpy.newaxis] * θdirection \
          + vzbasis[:,numpy.newaxis] * zdirection 

  
  # Output the number of state variables in the log file.
  log.user('degrees of freedom: φ={}, μ={}, v={}, p={}'.format(len(ns.φbasis), len(ns.μbasis), len(ns.vbasis), len(ns.pbasis)))
  
  basis_pce = time.perf_counter()
  log.debug('basis generation (perf counter):',basis_pce-basis_pcb,'seconds on',time.strftime('%a %b %d %H:%M:%S %Y'))
  
#======================================================================================================================#
  
  def_unknowns_pcb = time.perf_counter()
  
 # Name-space variables for unknowns.
  # Fluid velocity.
  ns.v_i = 'vbasis_ni ?vdofs_n'
  if istep > 0:
    ns.vold = ns_prev_t_input.v
  # Pressure.
  ns.p   = 'pbasis_n ?pdofs_n'
  # Phase-field.
  ns.φ   = 'φbasis_n ?φdofs_n'
  # Phase-field in previous time-step.
  if istep > 0:
    ns.φold = ns_prev_t_input.φ
    if not BE_only and irefine >= nrefine - 1:
      ns.pold = ns_prev_t_input.p
    else:
      #Previous timestep values on finest grid 
      ns_prev_t.φold    = ns_prev_t_input.φ
      ns_prev_t.vold    = ns_prev_t_input.v
      ns_prev_t.φold2   = 'φold2basis_n ?φdofs2_n'
      ns_prev_t.vold2_i = 'vold2basis_ni ?vdofs2_n'
  if istep > 0:
    if irefine == 0 and irefine2 == 0:
      # Previous unrefined timestep values
      ns.φPrevt00 = ns_prev_t_00.φ
      ns.μPrevt00 = ns_prev_t_00.μ
      ns.pPrevt00 = ns_prev_t_00.p
      ns.vPrevt00 = ns_prev_t_00.v
    else:
      # Values of the previous solution/iteration
      ns.φPrevSol = ns_prev_sol.φ
      ns.μPrevSol = ns_prev_sol.μ
      ns.pPrevSol = ns_prev_sol.p
      ns.vPrevSol = ns_prev_sol.v
  # Chemical potential.
  ns.μ   = 'μbasis_n ?μdofs_n'
  if istep > 0:
    ns.μold = ns_prev_t_input.μ
  if istep > 0:
    ns.μavg = '(μ + μold) / 2'
  else:
    ns.μavg = 'μ'
  ns.timestep = timestepsize
  ns.l = '?lm'
  
  def_unknowns_pce = time.perf_counter()
  log.debug('def. unknowns (perf counter):',def_unknowns_pce - def_unknowns_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
  def_equations_pcb = time.perf_counter()
  
  # Equation-related name-space variables.
  left, mid, right = function.partition(ns.φ, -1, 1)
  if istep>0 and irefine >= nrefine - 1:
    lefto, mido, righto = function.partition(ns.φold, -1, 1)
    
  # Truncated double-well potential.
  if double_well_type == 0:
    ns.ψ = '.25 φ^4 - .5 φ^2 + .25'
  elif double_well_type == 1:
    dwa = -0.125
    dwm = 1.1
    ns.ψ = left*( dwa * ( function.ln((dwm+ns.φ)/(dwm-1)) - (1+ns.φ)/(dwm-1) ) ) + mid*0.25*(ns.φ**2-1)**2 +\
      right*( dwa * ( function.ln((dwm-ns.φ)/(dwm-1)) - (1-ns.φ)/(dwm-1) ) )
  else:
    log.warning('double_well_type =',double_well_type,'not supported.')
    return
  
  
  
  if vis_profile == 0:
    ns.vis    = 'vis1 (1 + φ) / 2 + vis2 (1 - φ) / 2'
  elif vis_profile == 1:
    ns.vis    = function.exp( function.ln(ns.vis1) * ( 1 + ns.φ) / 2 + function.ln(ns.vis2) * (1 - ns.φ) / 2 )
  else:
    log.warning('vis_profile =',vis_profile,'not supported.')
    return
  
  def_equations_pce = time.perf_counter()
  log.debug('def. equations (perf counter):',def_equations_pce - def_equations_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
    
#======================================================================================================================#
  
  neumann_bc_def_pcb = time.perf_counter()
  
  # Neumann boundary condition values.
  ns.μnorm     = '0'
  ns.φnorm     = '0'
  
  neumann_bc_def_pce = time.perf_counter()
  log.debug('Neumann BC def. (perf counter):',neumann_bc_def_pce - neumann_bc_def_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
#======================================================================================================================#
  
  dirichlet_bc_def_pcb = time.perf_counter()
  
  # Dirichlet boundary condition values.

  
  dirichlet_bc_def_pce = time.perf_counter()
  log.debug('Dirichlet BC def. (perf counter):',dirichlet_bc_def_pce - dirichlet_bc_def_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
#======================================================================================================================#
  
  def_equations_2_pcb = time.perf_counter()
  
  def_equations_2_01_pcb = time.perf_counter()
  
  if istep>0:
    ns.ψold = ns_prev_t_input.ψ
    
  def_equations_2_01_pce = time.perf_counter()
  log.debug('(sub) def_equations_2_01 (perf counter):',def_equations_2_01_pce - def_equations_2_01_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
  def_equations_2_02_pcb = time.perf_counter()
  
  ns.m = mobility * ( (0.5*(1-vareps)*(1+function.exp(-1*(2*vareps*(ns.φ*ns.φ-1))/(1-vareps))))*left +\
    (1-vareps*ns.φ*ns.φ)*mid + (0.5*(1-vareps)*(1+function.exp(-1*(2*vareps*(ns.φ*ns.φ-1))/(1-vareps))))*right )
  if istep>0 and irefine >= nrefine - 1:
    ns.mold = mobility * ( (0.5*(1-vareps)*(1+function.exp(-1*(2*vareps*(ns.φold*ns.φold-1))/(1-vareps))))*lefto +\
      (1-vareps*ns.φold*ns.φold)*mido +\
      (0.5*(1-vareps)*(1+function.exp(-1*(2*vareps*(ns.φold*ns.φold-1))/(1-vareps))))*righto )
  
  def_equations_2_02_pce = time.perf_counter()
  log.debug('(sub) def_equations_2_02 (perf counter):',def_equations_2_02_pce - def_equations_2_02_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
  def_equations_2_03_pcb = time.perf_counter()
  
  
  # Derivative of truncated double-well potential to φ.
  if not BE_only:
    if irefine >= nrefine - 1:
      if double_well_type == 0:
        ns.dψ = 'φ^3 - φ'
      else:
        ns.dψ = left*( dwa * ( -1/(dwm-1) + 1/(dwm+ns.φ) ) ) + mid*( ns.φ**3 - ns.φ ) + right*( dwa * ( 1/(dwm-1) -\
          1/(dwm-ns.φ) ) )
    
    
  def_equations_2_03_pce = time.perf_counter()
  log.debug('(sub) def_equations_2_03 (perf counter):',def_equations_2_03_pce - def_equations_2_03_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
  def_equations_2_04_pcb = time.perf_counter()
    
  def_equations_2_04_pce = time.perf_counter()
  log.debug('(sub) def_equations_2_04 (perf counter):',def_equations_2_04_pce - def_equations_2_04_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
  def_equations_2_05_pcb = time.perf_counter()
  
  if lqddensity == airdensity:
    ns.ρ = 'ρ1'
  else:
    if lqddensity > airdensity:
      ns.densε = 'ρ2 / ( ρ1 - ρ2 )'
    else:
      ns.densε = 'ρ1 / ( ρ2 - ρ1 )'
    dens_ll, dens_lm, dens_m, dens_mr, dens_r = function.partition(ns.φ, -1-2*ns.densε, -1-ns.densε,
      1+ns.densε, 1+2*ns.densε)
    if lqddensity > airdensity:
      ns.ρ = dens_ll * ( ns.ρ2 / 4 )\
        + dens_lm * ( ns.ρ2 / 4 + ns.ρ2 / ( 4 * ns.densε * ns.densε ) * ( 1 + 2 * ns.densε + ns.φ ) *\
        ( 1 + 2 * ns.densε + ns.φ ) )\
        + dens_m * ( ns.ρ1 * (1 + ns.φ) / 2 + ns.ρ2 * (1 - ns.φ) / 2 )\
        + dens_mr * ( ns.ρ1 + 3 * ns.ρ2 / 4 - ns.ρ2 / ( 4 * ns.densε * ns.densε ) * ( 1 + 2 * ns.densε - ns.φ ) *\
        ( 1 + 2 * ns.densε - ns.φ ) )\
        + dens_r * ( ns.ρ1 + 3 * ns.ρ2 / 4 )
    else:
      ns.ρ = dens_ll * ( ns.ρ2 + 3 * ns.ρ1 / 4 )\
        + dens_lm * ( ns.ρ2 + 3 * ns.ρ1 / 4 - ns.ρ1 / ( 4 * ns.densε * ns.densε ) * ( 1 + 2 * ns.densε + ns.φ ) *\
        ( 1 + 2 * ns.densε + ns.φ ) )\
        + dens_m * ( ns.ρ1 * (1 + ns.φ) / 2 + ns.ρ2 * (1 - ns.φ) / 2 )\
        + dens_mr * ( ns.ρ1 / 4 + ns.ρ1 / ( 4 * ns.densε * ns.densε ) * ( 1 + 2 * ns.densε - ns.φ ) *\
        ( 1 + 2 * ns.densε - ns.φ ) )\
        + dens_r * ( ns.ρ2 / 4 )
  
  def_equations_2_05_pce = time.perf_counter()
  log.debug('(sub) def_equations_2_05 (perf counter):',def_equations_2_05_pce - def_equations_2_05_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
  def_equations_2_06_pcb = time.perf_counter()
  
  if istep > 0:
    ns.ρold = ns_prev_t_input.ρ
    
  def_equations_2_06_pce = time.perf_counter()
  log.debug('(sub) def_equations_2_06 (perf counter):',def_equations_2_06_pce - def_equations_2_06_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
  def_equations_2_07_pcb = time.perf_counter()
  
  ns.τ_ij = 'vis ( v_i,j + v_j,i + lambdavar δ_ij v_k,k )'
  
  def_equations_2_07_pce = time.perf_counter()
  log.debug('(sub) def_equations_2_07 (perf counter):',def_equations_2_07_pce - def_equations_2_07_pce,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
  def_equations_2_08_pcb = time.perf_counter()
  
#  ns.ζ_ij = '- (ε σ) φ_,i φ_,j + .5 ε σ φ_,k φ_,k δ_ij + (σ / ε) ψ δ_ij - μ φ δ_ij'
  ns.ζ_ij = '- (ε σ) φ_,i φ_,j + .5 ε σ φ_,k φ_,k δ_ij + (σ / ε) ψ δ_ij'
  
  def_equations_2_08_pce = time.perf_counter()
  log.debug('(sub) def_equations_2_08 (perf counter):',def_equations_2_08_pce - def_equations_2_08_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
  def_equations_2_09_pcb = time.perf_counter()
  
  ns.gradφ_i = 'φ_,i'
  
  def_equations_2_09_pce = time.perf_counter()
  log.debug('(sub) def_equations_2_09 (perf counter):',def_equations_2_09_pce - def_equations_2_09_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
  def_equations_2_10_pcb = time.perf_counter()
  
  if istep > 0:
    ns.τold = ns_prev_t_input.τ
    ns.ζold = ns_prev_t_input.ζ
    
  def_equations_2_10_pce = time.perf_counter()
  log.debug('(sub) def_equations_2_10 (perf counter):',def_equations_2_10_pce - def_equations_2_10_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
  def_equations_2_11_pcb = time.perf_counter()
  
  ns.dσSF   = '.75 (σ1 - σ2) (1 - φ^2)'
  
  def_equations_2_11_pce = time.perf_counter()
  log.debug('(sub) def_equations_2_11 (perf counter):',def_equations_2_11_pce - def_equations_2_11_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
  def_equations_2_pce = time.perf_counter()
  log.debug('def. equations 2 (perf counter):',def_equations_2_pce - def_equations_2_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
#======================================================================================================================#
  
  if istep > 0:
    
    if not BE_only and irefine >= nrefine - 1:
#    if irefine >= nrefine:
      
      compute_residual_pcb = time.perf_counter()
      
      log.info('Crank--Nicolson method:')
      
      combinedDomain = domain & domain1
      #   Eq. 01 + Eq. 02
      ns.Dρv_k = 'ρ v_k - ρold vold_k'
      vresidual = combinedDomain.integral('vbasis_nk d:x Dρv_k / timestep' @ ns, degree=int_degree)
      
      vresidual += alpha*0.5*domain.integral('- vbasis_ni,j ρ v_i v_j d:x' @ ns, degree=int_degree)
      vresidual += (1-alpha)*0.5*combinedDomain.integral('- vbasis_ni,j ρold vold_i vold_j d:x' @ ns, degree=int_degree)
      
      vresidual += alpha*0.5*domain.integral('v_i,j ρ vbasis_ni v_j d:x' @ ns, degree=int_degree)
      vresidual += (1-alpha)*0.5*combinedDomain.integral('vold_i,j ρold vbasis_ni vold_j d:x' @ ns, degree=int_degree)
      
      vresidual += alpha*0.5*domain.integral('vbasis_ni v_i v_j ρ_,j d:x' @ ns, degree=int_degree)
      vresidual += (1-alpha)*0.5*combinedDomain.integral('vbasis_ni vold_i vold_j ρold_,j d:x' @ ns, degree=int_degree)
      
      vresidual += alpha*domain.integral('vbasis_ni,j τ_ij d:x' @ ns, degree=int_degree)
      vresidual += (1-alpha)*combinedDomain.integral('vbasis_ni,j τold_ij d:x' @ ns, degree=int_degree)
      
      vresidual += alpha*domain.integral('vbasis_ni,j ζ_ij d:x' @ ns, degree=int_degree)
      vresidual += (1-alpha)*combinedDomain.integral('vbasis_ni,j ζold_ij d:x' @ ns, degree=int_degree)
      
      vresidual += alpha*domain.integral('- vbasis_ni,j ( ρ1 - ρ2 ) m v_i μ_,j d:x / 2' @ ns, degree=int_degree)
      vresidual += (1-alpha)*combinedDomain.integral('- vbasis_ni,j ( ρ1 - ρ2 ) mold vold_i μold_,j d:x / 2' @ ns,
        degree=int_degree)
      
      vresidual += domain.integral('- vbasis_ni,i p d:x' @ ns, degree=int_degree)
      
      #vresidual += (1-alpha)*0.5*(domain.boundary[Ambient]&domain1.boundary[Ambient]).integral('''vbasis_ni ρold vold_i
      #  vold_j n_j d:x''' @ ns, degree=int_degree) Term zou 0 moeten zijn
      
      
      
      μresidual = combinedDomain.integral('μbasis_n d:x ( φ - φold ) / timestep' @ ns, degree=int_degree)
      
      μresidual += alpha*domain.integral('μbasis_n φ_,k v_k d:x' @ ns, degree=int_degree)
      μresidual += (1-alpha)*combinedDomain.integral('μbasis_n φold_,k vold_k d:x' @ ns, degree=int_degree)
      
      μresidual += alpha*domain.integral('μbasis_n,k m μ_,k d:x' @ ns, degree=int_degree)
      μresidual += (1-alpha)*combinedDomain.integral('μbasis_n,k mold μold_,k d:x' @ ns, degree=int_degree)
      
      
      
      φresidual = domain.integral('φbasis_n μ d:x' @ ns, degree=int_degree)
  
      φresidual += domain.integral('- φbasis_n,k (σ ε) φ_,k d:x' @ ns, degree=int_degree)
      
      φresidual += domain.integral('- φbasis_n (σ / ε) dψ d:x' @ ns, degree=int_degree)
           
      
      
#      residual += domain.integral('pbasis_n v_k,k d:x' @ ns, degree=int_degree)
      presidual = domain.integral('pbasis_n ( v_k,k + l ) d:x' @ ns, degree=int_degree)
      
      
      
      lresidual = domain.integral('p d:x' @ ns, degree=int_degree)
      
      
      
     # Integral (Neumann) conditions.
      # μresidual  = domain.boundary[Symaxis].integral('''
      #   μbasis_n m μnorm d:x''' @ ns, degree=int_degree)
      #μresidual += domain.boundary[Symplane2].integral('''
      #  μbasis_n m μnorm d:x''' @ ns, degree=int_degree)
      #μresidual += domain.boundary[Ambient].integral('''
      #  μbasis_n m μnorm d:x''' @ ns, degree=int_degree)
      #μresidual += domain.boundary[Symplane1].integral('''
      #  μbasis_n m μnorm d:x''' @ ns, degree=int_degree)
      
      #φresidual += domain.boundary[Ambient].integral('''
      # φbasis_n ε σ φnorm d:x''' @ ns, degree=int_degree)
      # φresidual += domain.boundary[Symaxis].integral('''
      #   φbasis_n ε σ φnorm d:x''' @ ns, degree=int_degree)
      #φresidual += domain.boundary[Symplane2].integral('''
      #  φbasis_n ε σ φnorm d:x''' @ ns, degree=int_degree)
      #φresidual += domain.boundary[Symplane1].integral('''
      #   φbasis_n ε σ φnorm d:x''' @ ns, degree=int_degree)
      
      compute_residual_pce = time.perf_counter()
      log.debug('compute residual (perf counter):',compute_residual_pce - compute_residual_pcb,'seconds on',
        time.strftime('%a %b %d %H:%M:%S %Y'))
      
#======================================================================================================================#
      
    else:
      
      project_old_sol_pcb = time.perf_counter()
      
      log.info('Backward Euler method:')
      
      combinedDomain       = domain & domain1
      ns_prev_t.voldold2_i = 'vold2_i - vold_i'
      sqr = combinedDomain.integral('''( (φold2 - φold)^2 + voldold2_k voldold2_k ) d:x '''@ ns_prev_t, degree=int_degree)
      lhs_prev_t = solver.optimize(['vdofs2', 'φdofs2'], sqr)
      ns_prev_t  = ns_prev_t(**lhs_prev_t)
      ns.φold3   = ns_prev_t.φold2
      ns.vold3   = ns_prev_t.vold2
      ns.ρold3   = 'ρ1 (1 + φold3) / 2 + ρ2 (1 - φold3) / 2'
      ns.dψ      = 'φold3^3 - 3 φold3 + 2 φ'
      
      project_old_sol_pce = time.perf_counter()
      log.debug('project old sol. (perf counter):',project_old_sol_pce - project_old_sol_pcb,'seconds on',
        time.strftime('%a %b %d %H:%M:%S %Y'))
    
      compute_residual_pcb = time.perf_counter()
      
      ns.Dρv_k = 'ρ v_k - ρold3 vold3_k'
      vresidual = domain.integral('vbasis_nk d:x Dρv_k / timestep' @ ns, degree=int_degree)
  
      vresidual += 0.5*domain.integral('- vbasis_ni,j ρ v_i v_j d:x' @ ns, degree=int_degree)
      
      vresidual += 0.5*domain.integral('v_i,j ρ vbasis_ni v_j d:x' @ ns, degree=int_degree)
  
      vresidual += 0.5*domain.integral('vbasis_ni v_i v_j ρ_,j d:x' @ ns, degree=int_degree)
      
      vresidual += domain.integral('vbasis_ni,j τ_ij d:x' @ ns ,degree=int_degree)
      
      vresidual += domain.integral('vbasis_ni,j ζ_ij d:x' @ ns ,degree=int_degree)
      
      vresidual += domain.integral('- vbasis_ni,j ( ρ1 - ρ2 ) m v_i μ_,j d:x / 2' @ ns, degree=int_degree)
      
      vresidual += domain.integral('- vbasis_ni,i p d:x' @ ns ,degree=int_degree)
      
      
      
      μresidual = domain.integral('μbasis_n d:x ( φ - φold3 ) / timestep' @ ns, degree=int_degree)
      
      μresidual += domain.integral('μbasis_n φ_,k v_k d:x' @ ns, degree=int_degree)
      
      μresidual += domain.integral('μbasis_n,k m μ_,k d:x' @ ns, degree=int_degree)
      
      
      
      φresidual = domain.integral('φbasis_n μ d:x' @ ns, degree=int_degree)
  
      φresidual += domain.integral('- φbasis_n,k (σ ε) φ_,k d:x' @ ns, degree=int_degree)
      
      φresidual += domain.integral('- φbasis_n (σ / ε) dψ d:x' @ ns, degree=int_degree)      
      
      
#      residual += domain.integral('pbasis_n v_k,k d:x' @ ns, degree=int_degree)
      presidual = domain.integral('pbasis_n ( v_k,k + l ) d:x' @ ns, degree=int_degree)
      lresidual = domain.integral('p d:x' @ ns, degree=int_degree)
      
      
      
#      # Integral (Neumann) conditions.
      # μresidual  = domain.boundary[Symaxis].integral('''
      #   μbasis_n m μnorm d:x''' @ ns, degree=int_degree)
      #μresidual += domain.boundary[Symplane2].integral('''
      #  μbasis_n m μnorm d:x''' @ ns, degree=int_degree)
      #μresidual += domain.boundary[Ambient].integral('''
      #  μbasis_n m μnorm d:x''' @ ns, degree=int_degree)
      #μresidual += domain.boundary[Symplane1].integral('''
      #  μbasis_n m μnorm d:x''' @ ns, degree=int_degree)
      
      #φresidual += domain.boundary[Ambient].integral('''
      #  φbasis_n ε σ φnorm d:x''' @ ns, degree=int_degree)
      # φresidual += domain.boundary[Symaxis].integral('''
      #   φbasis_n ε σ φnorm d:x''' @ ns, degree=int_degree)
      #φresidual += domain.boundary[Symplane2].integral('''
      #  φbasis_n ε σ φnorm d:x''' @ ns, degree=int_degree)
      #φresidual += domain.boundary[Symplane1].integral('''
      #   φbasis_n ε σ φnorm d:x''' @ ns, degree=int_degree)
          
      compute_residual_pce = time.perf_counter()
      log.debug('compute residual (perf counter):',compute_residual_pce - compute_residual_pcb,'seconds on',
        time.strftime('%a %b %d %H:%M:%S %Y'))
      
#======================================================================================================================#
  
  dirichlet_sqr_pcb = time.perf_counter()
  
  # Dirichlet conditions.
  ns.vn = 'v_k n_k'
  sqr =  domain.boundary[Symplane1].integral('vn vn' @ ns,
    degree=int_degree)
#  sqr =  domain.boundary[Symaxis].integral('vn vn d:x' @ ns,
#    degree=int_degree)
  sqr += domain.boundary[Ambient].integral('v_k v_k d:x' @ ns,
    degree=int_degree)
  sqr += domain.boundary[Symplane2].integral('vn vn' @ ns,
    degree=int_degree)
  cons = solver.optimize(['vdofs'], sqr, droptol=1e-12)
  
  dirichlet_sqr_pce = time.perf_counter()
  log.debug('Dirichlet sqr (perf counter):',dirichlet_sqr_pce - dirichlet_sqr_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  
#======================================================================================================================#
  
  
 # Initial condition values.
  
  if istep == 0:
    
    newton_success = True
    
    init_cond_pcb = time.perf_counter()
    
    # Initial pressure.
    ns.pinit = '0'
    # Initial φ; φ has a tanh solution in the equilibrium state.
    #ns.φinit = function.tanh( (R0 - filament([ns.x[0],ns.x[1],ns.x[2]], R0, L0)) / (ns.ε * function.sqrt(2)) )
    #ns.φinit = - function.tanh(ellipse([ns.x[0]-0.00005,ns.x[1]-0.00005])/ (ns.ε * function.sqrt(2) ))
    #ns.φinit = - function.tanh(ellipse([ns.x[0]-0.00005,ns.x[1]])/ (ns.ε * function.sqrt(2) ))
    #ns.φinit = - function.tanh(((ns.x[0]**2+ns.x[1]**2)**0.5 -5e-5) / (ns.ε * function.sqrt(2)))
    ns.r   = (ns.x[0]**2 + ns.x[1]**2)**0.5
    ns.Rφ0 = neck(ns.x[2], R0, L0, h0, A0)
    ns.φinit = function.tanh( (ns.Rφ0 - ns.r) / (ns.ε * function.sqrt(2)) )
    #ns.φinit ='0' 
    # Initial chemical potential.
    ns.μinit = '0'
  
    init_cond_pce = time.perf_counter()
    log.debug('init cond (perf counter):',init_cond_pce - init_cond_pcb,'seconds on',
      time.strftime('%a %b %d %H:%M:%S %Y'))
    
    init_cond_lhs0_istep0_pcb = time.perf_counter()
    
    # Initial conditions.
    ns.vzero = numpy.zeros(3)
    ns.vinit_i = 'v_i - vzero_i'
    sqr = domain.integral('''( vinit_k vinit_k + (p - pinit)^2 + (φ - φinit)^2
      + (μ - μinit)^2 ) d:x''' @ ns, degree=int_degree)
    lhs0 = solver.optimize(['φdofs', 'μdofs', 'vdofs', 'pdofs'], sqr)
    lhs = lhs0
    
    init_cond_lhs0_istep0_pce = time.perf_counter()
    log.debug('init. cond. lhs0 istep0 (perf counter):',init_cond_lhs0_istep0_pce - init_cond_lhs0_istep0_pcb,
      'seconds on',time.strftime('%a %b %d %H:%M:%S %Y'))
  
#======================================================================================================================#
  
  if istep > 0:
    
    init_cond_lhs0_istep_pcb = time.perf_counter()
    
    # Using previous solutions as initial guess
    if irefine == 0 and irefine2 == 0:
      ns.vPrevt200_i = 'v_i - vPrevt00_i'
      sqr = domain.integral('''( (φ - φPrevt00)^2 + (p - pPrevt00)^2
        + (μ - μPrevt00)^2 + vPrevt200_k vPrevt200_k )''' @ ns, degree = int_degree)
    else:
      oldnew = domain & domain_prev_t
      ns.vPrevSol2_i = 'v_i - vPrevSol_i'
      sqr = oldnew.integral('''( (φ - φPrevSol)^2 + (p - pPrevSol)^2
        + (μ - μPrevSol)^2 + vPrevSol2_k vPrevSol2_k) d:x '''@ ns, degree=int_degree)
      
    lhs0 = solver.optimize(['φdofs', 'μdofs', 'vdofs', 'pdofs'], sqr)
    
    nschp = ns(**lhs0)
    bezier = domain[:,0:,:].boundary['bottom'].sample('bezier', 2)
    uniform = domain[:,0:,:].boundary['bottom'].sample('uniform', 1)
    x, v, phi, mu, p = bezier.eval([nschp.x[:3], function.norm2(nschp.v),nschp.φ, nschp.μ, nschp.p])
    x2Dplot = x[:,(0,2)]
    xy, uv = uniform.eval([nschp.x[:3], nschp.v[:3]])
    xy2Dplot = xy[:,(0,2)]
    uv2Dplot = uv[:,(0,2)]
    with export.mplfigure('phase_projection.png', figsize=(10.24,7.68)) as fig:
      plottitle = 'Phase projection'
      ax = fig.add_subplot(111, aspect='equal',title=plottitle)
      ax.autoscale(enable=True, axis='both', tight=True)
      im = ax.tripcolor(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, phi, shading='gouraud',cmap='coolwarm')
      ax.add_collection(collections.LineCollection(x2Dplot[bezier.hull], colors='k', linewidth=0.5, alpha=0.2))
      ax.tricontour(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5, -0.375,
        -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='w', linestyles='solid', alpha=1)
      fig.colorbar(im)
    with export.mplfigure('velocity_projection.png', figsize=(10.24,7.68)) as fig:
      plottitle = 'Velocity projection'
      ax = fig.add_subplot(111, aspect='equal',title=plottitle)
      ax.autoscale(enable=True, axis='both', tight=True)
      im = ax.tripcolor(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, v, shading='gouraud',cmap='coolwarm')
      ax.add_collection(collections.LineCollection(x2Dplot[bezier.hull], colors='k', linewidth=0.5, alpha=0.2))
      ax.tricontour(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5, -0.375,
        -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='w', linestyles='solid', alpha=1)
      ax.quiver(xy2Dplot[:,0], xy2Dplot[:,1], uv2Dplot[:,0], uv2Dplot[:,1], angles='xy', scale_units='xy')
      fig.colorbar(im)
    with export.mplfigure('chem_pot_projection.png', figsize=(10.24,7.68)) as fig:
      plottitle = 'Chemical potential projection'
      ax = fig.add_subplot(111, aspect='equal',title=plottitle)
      ax.autoscale(enable=True, axis='both', tight=True)
      im = ax.tripcolor(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, mu, shading='gouraud',cmap='coolwarm')
      ax.tricontour(x2Dplot[:,0],x2Dplot[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5, -0.375,
        -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='w', linestyles='solid', alpha=1)
      fig.colorbar(im)
    with export.mplfigure('pressure_projection.png', figsize=(10.24,7.68)) as fig:
      plottitle = 'Pressure projection'
      ax = fig.add_subplot(111, aspect='equal',title=plottitle)
      ax.autoscale(enable=True, axis='both', tight=True)
      im = ax.tripcolor(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, p, shading='gouraud',cmap='jet')
      ax.tricontour(x2Dplot[:,0],x2Dplot[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5, -0.375,
        -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='w', linestyles='solid', alpha=1)
      fig.colorbar(im)
    
    init_cond_lhs0_istep_pce = time.perf_counter()
    log.debug('init. cond. lhs0 istep (perf counter):',init_cond_lhs0_istep_pce - init_cond_lhs0_istep_pcb,'seconds on',
      time.strftime('%a %b %d %H:%M:%S %Y'))
    
    newton_solve_pcb = time.perf_counter()
    
    newton = solver.newton(['φdofs', 'μdofs', 'vdofs', 'pdofs', 'lm'],
      residual=[φresidual, μresidual, vresidual, presidual, lresidual],
      arguments=lhs0, constrain=cons,
      linprecon=precon(len(ns.φbasis)+len(ns.μbasis), numpy.isnan(cons['vdofs']).sum()+len(ns.pbasis)+1))
    
    newton_success = False

    try:
      perf_counter_begin = time.perf_counter()
      for iiter, (lhs, info) in log.iter.plain('iter', enumerate(newton)):
        perf_counter_end = time.perf_counter()
        log.info('residual: {:.2e}'.format(info.resnorm))
        log.debug('perf counter: {:.2e}'.format(perf_counter_end-perf_counter_begin))
        perf_counter_begin = perf_counter_end
        if info.resnorm < tol:
          newton_success = True
          break
        if irefine2 == 0:
          max_iter = max_iter0
        else:
          max_iter = max_iter1
        if iiter >= max_iter:
          log.warning('Maximum number of iterations reached. Time step size is being halved.')
          break
    except solver.SolverError as e:
      log.warning(e)
      log.warning('Time step size is being halved.')
    
    newton_solve_pce = time.perf_counter()
    log.debug('Newton solve (perf counter):',newton_solve_pce - newton_solve_pcb,'seconds on',
      time.strftime('%a %b %d %H:%M:%S %Y'))
    
  namespace_write_pcb = time.perf_counter()
  
  nsch      = ns(**lhs)
  bezier    = domain[:,0:,:].boundary['bottom'].sample('bezier', 4)
  phi       = bezier.eval(nsch.φ)
  phase_min = numpy.min(phi)
  
  if newton_success and phase_min < -1 - β:
   newton_success = False
   log.info('Phase field minimum  =', phase_min)
   log.warning('Phase field minimum too low. Time step size is being halved.')

  
  namespace_write_pce = time.perf_counter()
  log.debug('namespace write (perf counter):',namespace_write_pce - namespace_write_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))

  # Updating ns_prev_t_00 for this timestep
  if newton_success and irefine == 0 and irefine2 == 0:
    ns_prev_t_00 = ns(**lhs)
  
  return ns, lhs, newton_success, ns_prev_t_00



########################################################################################################################
#======================================================================================================================#
########################################################################################################################



def makePlots(currentsimtime, timestepsize, domain, domain1, ns, lhs, index2, index, index3, nrefine):
  
#  timeintext = str(import_time + timestep*(index2-import_IC))
  timeintext = str(currentsimtime + timestepsize)
  ns.divu = 'v_k,k'
  nsch = ns(**lhs)
  bezier = domain[:,0:,:].boundary['bottom'].sample('bezier', 5)
  combinedDomain = domain & domain1
  combinedBezier = combinedDomain[:,0:,:].boundary['bottom'].sample('bezier', 5)
  bezier2 = domain.refine(2)[:,0:,:].boundary['bottom'].sample('bezier', 2)
  uniform = domain[:,0:,:].boundary['bottom'].sample('uniform', 1)
  x, v, phi, mu, p, divu = bezier.eval([nsch.x[:3], function.norm2(nsch.v),nsch.φ, nsch.μ, nsch.p, nsch.divu])
  x2Dplot = x[:,(0,2)]
  xavg, muavg = combinedBezier.eval([nsch.x[:3], nsch.μavg])
  xavg2Dplot = xavg[:,(0,2)]
  xy2, uv2, phi2, pres2, mu2, divu2 = bezier2.eval([nsch.x, nsch.v, nsch.φ, nsch.p, nsch.μ, nsch.divu])
  xy22Dplot = xy2[:,(0,2)]
  uv22Dplot = uv2[:,(0,2)]
  xy, uv = uniform.eval([nsch.x[:3], nsch.v[:3]])
  xy2Dplot = xy[:,(0,2)]
  uv2Dplot = uv[:,(0,2)]
  
  with export.mplfigure('phase_{}_{}.png'.format(index3,index,index2), figsize=(10.24,7.68)) as fig:
    plottitle = 'Phase (t = '+timeintext+' s)'
    ax = fig.add_subplot(111, aspect='equal',title=plottitle)
    ax.autoscale(enable=True, axis='both', tight=True)
    im = ax.tripcolor(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, phi, shading='gouraud',cmap='coolwarm')
    ax.add_collection(collections.LineCollection(x2Dplot[bezier.hull], colors='k', linewidth=0.5, alpha=0.2))
    ax.tricontour(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5, -0.375,
      -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='w', linestyles='solid', alpha=1)
    fig.colorbar(im)
  
  with export.mplfigure('phase_noElem_{}_{}.png'.format(index3,index,index2), figsize=(10.24,7.68)) as fig:
    plottitle = 'Phase (no elem.) (t = '+timeintext+' s)'
    ax = fig.add_subplot(111, aspect='equal',title=plottitle)
    ax.autoscale(enable=True, axis='both', tight=True)
    im = ax.tripcolor(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, phi, shading='gouraud',cmap='coolwarm')
    ax.tricontour(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5, -0.375,
      -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='w', linestyles='solid', alpha=1)
    fig.colorbar(im)
  
  with export.mplfigure('velocity_{}_{}.png'.format(index3,index,index2), figsize=(10.24,7.68)) as fig:
    plottitle = 'Velocity (t = '+timeintext+' s)'
    ax = fig.add_subplot(111, aspect='equal',title=plottitle)
    ax.autoscale(enable=True, axis='both', tight=True)
    im = ax.tripcolor(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, v, shading='gouraud',cmap='jet')
    ax.add_collection(collections.LineCollection(x2Dplot[bezier.hull], colors='k', linewidth=0.5, alpha=0.2))
    ax.tricontour(x2Dplot[:,0],x2Dplot[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5, -0.375,
      -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='w', linestyles='solid', alpha=1)
    ax.quiver(xy2Dplot[:,0], xy2Dplot[:,1], uv2Dplot[:,0], uv2Dplot[:,1], angles='xy', scale_units='xy')
    fig.colorbar(im)
  
  with export.mplfigure('velocity_noElem_{}_{}.png'.format(index3,index,index2), figsize=(10.24,7.68)) as fig:
    plottitle = 'Velocity (no elem.) (t = '+timeintext+' s)'
    ax = fig.add_subplot(111, aspect='equal',title=plottitle)
    ax.autoscale(enable=True, axis='both', tight=True)
    im = ax.tripcolor(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, v, shading='gouraud',cmap='jet')
    ax.tricontour(x2Dplot[:,0],x2Dplot[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5, -0.375,
      -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='w', linestyles='solid', alpha=1)
    ax.quiver(xy2Dplot[:,0], xy2Dplot[:,1], uv2Dplot[:,0], uv2Dplot[:,1], angles='xy', scale_units='xy')
    fig.colorbar(im)
  
  with export.mplfigure('velocity_noElem_noArrow_{}_{}.png'.format(index3,index,index2), figsize=(10.24,7.68)) as fig:
    plottitle = 'Velocity (no elem. and no vectors) (t = '+timeintext+' s)'
    ax = fig.add_subplot(111, aspect='equal',title=plottitle)
    ax.autoscale(enable=True, axis='both', tight=True)
    im = ax.tripcolor(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, v, shading='gouraud',cmap='jet')
    ax.tricontour(x2Dplot[:,0],x2Dplot[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5, -0.375,
      -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='w', linestyles='solid', alpha=1)
    fig.colorbar(im)
  
  with export.mplfigure('chem_pot_{}_{}.png'.format(index3,index,index2), figsize=(10.24,7.68)) as fig:
    plottitle = 'Chemical potential (t = '+timeintext+' s)'
    ax = fig.add_subplot(111, aspect='equal',title=plottitle)
    ax.autoscale(enable=True, axis='both', tight=True)
    im = ax.tripcolor(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, mu, shading='gouraud',cmap='coolwarm')
    ax.tricontour(x2Dplot[:,0],x2Dplot[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5, -0.375,
      -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='w', linestyles='solid', alpha=1)
    fig.colorbar(im)
  
  with export.mplfigure('chem_pot_avg_{}_{}.png'.format(index3,index,index2), figsize=(10.24,7.68)) as fig:
    plottitle = 'Averaged chemical potential (t = '+timeintext+' s)'
    ax = fig.add_subplot(111, aspect='equal',title=plottitle)
    ax.autoscale(enable=True, axis='both', tight=True)
    im = ax.tripcolor(xavg2Dplot[:,0], xavg2Dplot[:,1], combinedBezier.tri, muavg, shading='gouraud',cmap='coolwarm')
    ax.tricontour(x2Dplot[:,0],x2Dplot[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5, -0.375,
      -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='w', linestyles='solid', alpha=1)
    fig.colorbar(im)

  with export.mplfigure('pressure_{}_{}.png'.format(index3,index,index2), figsize=(10.24,7.68)) as fig:
    plottitle = 'Pressure (t = '+timeintext+' s)'
    ax = fig.add_subplot(111, aspect='equal',title=plottitle)
    ax.autoscale(enable=True, axis='both', tight=True)
    im = ax.tripcolor(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, p, shading='gouraud',cmap='jet')
    ax.tricontour(x2Dplot[:,0],x2Dplot[:,1], bezier.tri, phi, linewidths=0.5, levels=[-1, -0.875, -0.75, -0.625, -0.5, -0.375,
      -0.25, -0.125, 0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1], colors='w', linestyles='solid', alpha=1)
    fig.colorbar(im)
    
  with export.mplfigure('div_u_{}_{}.png'.format(index3,index,index2), figsize=(10.24,7.68)) as fig:
    plottitle = 'Divergence of u (t = '+timeintext+' s)'
    ax = fig.add_subplot(111, aspect='equal',title=plottitle)
    ax.autoscale(enable=True, axis='both', tight=True)
    im = ax.tripcolor(x2Dplot[:,0], x2Dplot[:,1], bezier.tri, divu, shading='gouraud',cmap='jet')
    fig.colorbar(im)


########################################################################################################################
#======================================================================================================================#
########################################################################################################################

def makeVTK(currentsimtime, timestepsize, domain, ns, lhs, index2, index, index3):

  timeintext = str(currentsimtime + timestepsize)
  ns.divu = 'v_k,k'
  nsch = ns(**lhs)
  bezier2 = domain.refine(2)[:,0:,:].boundary['bottom'].sample('bezier', 2)
  uniform = domain[:,0:,:].boundary['bottom'].sample('uniform', 1)
  xy2, uv2, phi2, pres2, mu2, divu2 = bezier2.eval([nsch.x, nsch.v, nsch.φ, nsch.p, nsch.μ, nsch.divu])
  xy22Dplot = xy2[:,(0,2)]
  uv22Dplot = uv2[:,(0,2)]
  uv2theta  = uv2[:,  (1)]

  export.vtk('VTK_file_{}_{}_{}'.format(index3,index,index2), bezier2.tri, xy22Dplot, v=uv22Dplot, phi=phi2, p=pres2, mu=mu2,
    divu=divu2, vtheta=uv2theta)

########################################################################################################################
#======================================================================================================================#
########################################################################################################################



def makeEnergyPlots(domain, domain1, ns, lhs, index2, index, index3, Ambient, Symplane2, Symaxis, Symplane1, eInt1, eInt2,
  eExt1, eExt2, eExt3, eExt4, eMix, eInt, eWal, eKin, eTot, nrefine, import_IC, Timelist, timestepsize):
  
  energy_compute_pcb = time.perf_counter()
  
  nsch = ns(**lhs)
  # energyInt1 = domain.sample('gauss', degree=4).integrate('- τ_ij v_i,j d:x' @ nsch)
  # energyInt2 = domain.sample('gauss', degree=4).integrate('- m μ_,i μ_,i d:x' @ nsch)
  # if index2>0:
  #   if index >= nrefine - 1:
  #     energyExt1 = (domain.boundary[Symaxis]&domain1.boundary[Symaxis]).sample('gauss', degree=4).integrate('σ ε φ_,i n_i\
  #       (φ - φold) d:x / timestep' @ nsch)
  #     energyExt1 += (domain.boundary[Ambient]&domain1.boundary[Ambient]).sample('gauss', degree=4).integrate('σ ε φ_,i\
  #       n_i (φ - φold) d:x / timestep' @ nsch)
  #     energyExt1 += (domain.boundary[Symplane2]&domain1.boundary[Symplane2]).sample('gauss', degree=4).integrate('σ ε\
  #       φ_,i n_i (φ - φold) d:x / timestep' @ nsch)
  #     energyExt1 += (domain.boundary[Symplane1]&domain1.boundary[Symplane1]).sample('gauss', degree=4).integrate('σ ε\
  #       φ_,i n_i (φ - φold) d:x / timestep' @ nsch)
  #   else:
  #     energyExt1 = domain.boundary[Symaxis].sample('gauss', degree=4).integrate('''σ ε φ_,i n_i (φ - φold3) d:x
  #       / timestep''' @ nsch)
  #     energyExt1 += domain.boundary[Ambient].sample('gauss', degree=4).integrate('''σ ε φ_,i n_i (φ - φold3) d:x
  #       / timestep''' @ nsch)
  #     energyExt1 += domain.boundary[Symplane2].sample('gauss', degree=4).integrate('''σ ε φ_,i n_i (φ - φold3) d:x
  #       / timestep''' @ nsch)
  #     energyExt1 += domain.boundary[Symplane1].sample('gauss', degree=4).integrate('''σ ε φ_,i n_i (φ - φold3) d:x
  #       / timestep''' @ nsch)
  # else:
  #   energyExt1 = 0
  # energyExt2 = domain.boundary[Symaxis].sample('gauss', degree=4).integrate('- ( ρ v_i v_i v_j n_j ) d:x / 2' @ nsch)
  # energyExt2 += domain.boundary[Ambient].sample('gauss', degree=4).integrate('- ( ρ v_i v_i v_j n_j ) d:x / 2' @ nsch)
  # energyExt2 += domain.boundary[Symplane2].sample('gauss', degree=4).integrate('- ( ρ v_i v_i v_j n_j ) d:x / 2' @ nsch)
  # energyExt2 += domain.boundary[Symplane1].sample('gauss', degree=4).integrate('- ( ρ v_i v_i v_j n_j ) d:x / 2' @ nsch)
  # energyExt3 = domain.boundary[Symaxis].sample('gauss', degree=4).integrate('τ_ij v_j n_i d:x' @ nsch)
  # energyExt3 += domain.boundary[Ambient].sample('gauss', degree=4).integrate('τ_ij v_j n_i d:x' @ nsch)
  # energyExt3 += domain.boundary[Symplane2].sample('gauss', degree=4).integrate('τ_ij v_j n_i d:x' @ nsch)
  # energyExt3 += domain.boundary[Symplane1].sample('gauss', degree=4).integrate('τ_ij v_j n_i d:x' @ nsch)
  # energyExt4 = domain.boundary[Symaxis].sample('gauss', degree=4).integrate('μ m μ_,i n_i d:x' @ nsch)
  # energyExt4 += domain.boundary[Ambient].sample('gauss', degree=4).integrate('μ m μ_,i n_i d:x' @ nsch)
  # energyExt4 += domain.boundary[Symplane2].sample('gauss', degree=4).integrate('μ m μ_,i n_i d:x' @ nsch)
  # energyExt4 += domain.boundary[Symplane1].sample('gauss', degree=4).integrate('μ m μ_,i n_i d:x' @ nsch)
  if index2>0:
    Timelist        = numpy.append(Timelist, Timelist[-1]+timestepsize)
  energyMixture   = domain.sample('gauss',degree=4).integrate('σ ψ d:x / ε' @ nsch)
  energyInterface = domain.sample('gauss',degree=4).integrate('0.5 ε σ φ_,j φ_,j d:x' @ nsch)
  energyWall      = domain.boundary[Symplane2].sample('gauss',degree=4).integrate('dσSF d:x' @ nsch)
  energyWall      += domain.boundary[Symplane1].sample('gauss',degree=4).integrate('dσSF d:x' @ nsch)
  energyKinetic   = domain.sample('gauss',degree=4).integrate('0.5 ρ v_k v_k d:x' @ nsch)
  energyTotal     = energyMixture + energyInterface + energyWall + energyKinetic
  
  # eInt1.append(energyInt1)
  # eInt2.append(energyInt2)
  # eExt1.append(energyExt1)
  # eExt2.append(energyExt2)
  # eExt3.append(energyExt3)
  # eExt4.append(energyExt4)
  eMix.append(energyMixture)
  eInt.append(energyInterface)
  eWal.append(energyWall)
  eKin.append(energyKinetic)
  eTot.append(energyTotal)
  
  # log.user('Energy Int 1: ', energyInt1)
  # log.user('Energy Int 2: ', energyInt2)
  # log.user('Energy Ext 1: ', energyExt1)
  # log.user('Energy Ext 2: ', energyExt2)
  # log.user('Energy Ext 3: ', energyExt3)
  # log.user('Energy Ext 4: ', energyExt4)
  
  log.user('Mixture energy:   ', energyMixture)
  log.user('Interface energy: ', energyInterface)
  log.user('Wall energy:      ', energyWall)
  log.user('Kinetic energy:   ', energyKinetic)
  log.user('Total energy:     ', energyTotal)
  
  energy_compute_pce = time.perf_counter()
  log.debug('energy compute (perf counter):',energy_compute_pce - energy_compute_pcb,'seconds on',
    time.strftime('%a %b %d %H:%M:%S %Y'))
  

  with export.mplfigure('Mixture_energy_{}_{}.png'.format(index3,index,index2), figsize=(10.24,7.68)) as fig:
    ax = fig.add_subplot(111)
    ax.plot(Timelist, eMix, 'bo', label='Mixture energy')
    ax.plot(Timelist, eMix, 'b-')
    ax.grid()
    ax.legend()
  with export.mplfigure('Interface_energy_{}_{}.png'.format(index3,index,index2), figsize=(10.24,7.68)) as fig:
    ax = fig.add_subplot(111)
    ax.plot(Timelist, eInt, 'bo', label='Interface energy')
    ax.plot(Timelist, eInt, 'b-')
    ax.grid()
    ax.legend()
  with export.mplfigure('Wall_energy_{}_{}.png'.format(index3,index,index2), figsize=(10.24,7.68)) as fig:
    ax = fig.add_subplot(111)
    ax.plot(Timelist, eWal, 'bo', label='Wall energy')
    ax.plot(Timelist, eWal, 'b-')
    ax.grid()
    ax.legend()
  with export.mplfigure('Kinetic_energy_{}_{}.png'.format(index3,index,index2), figsize=(10.24,7.68)) as fig:
    ax = fig.add_subplot(111)
    ax.plot(Timelist, eKin, 'bo', label='Kinetic energy')
    ax.plot(Timelist, eKin, 'b-')
    ax.grid()
    ax.legend()
  with export.mplfigure('Total_energy_{}_{}.png'.format(index3,index,index2), figsize=(10.24,7.68)) as fig:
    ax = fig.add_subplot(111)
    ax.plot(Timelist, eTot, 'bo', label='Total energy')
    ax.plot(Timelist, eTot, 'b-')
    ax.grid()
    ax.legend()
              
  # with export.mplfigure('energy.png'.format(index2), figsize=(10.24,7.68)) as fig:
  #   ax = fig.add_subplot(1, 1, 1)
  #   line1, = ax.plot(range(0,index2+1-import_IC-1), eInt1, 'xkcd:blue', marker=11, label='eInt1')
  #   line2, = ax.plot(range(0,index2+1-import_IC-1), eInt2, 'xkcd:green', marker=11, label='eInt2')
  #   line3, = ax.plot(range(0,index2+1-import_IC-1), eExt1, 'xkcd:wheat', marker=10, label='eExt1')
  #   line4, = ax.plot(range(0,index2+1-import_IC-1), eExt2, 'xkcd:copper', marker=10, label='eExt2')
  #   line5, = ax.plot(range(0,index2+1-import_IC-1), eExt3, 'xkcd:dark green', marker=10, label='eExt3')
  #   line6, = ax.plot(range(0,index2+1-import_IC-1), eExt4, 'xkcd:orange', marker=10, label='eExt4')
  #   ax.set_title('Energy derivative in system.')
  #   ax.grid(True)
  #   ax.set_xlabel('timestep')
  #   ax.set_ylabel('energy derivative')
  #   ax.legend(loc=3)
  
  return eMix, eInt, eWal, eKin, eTot
  
  
# Run the main.
cli.run( main )

# [1] Yue, Pengtao, and James J. Feng. "Wall energy relaxation in the
#     Cahn–Hilliard model for moving contact lines." Physics of Fluids
#     23.1 (2011): 012106.
